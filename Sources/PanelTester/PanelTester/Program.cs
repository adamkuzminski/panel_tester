﻿using PanelTester.UI;
using System;
using System.Windows.Forms;

namespace PanelTester
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            if (Properties.Settings.Default.AppVersion != Application.ProductVersion)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.AppVersion = Application.ProductVersion;
                Properties.Settings.Default.Save();
            }

            AppEngine.GetInstance().Init();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}