﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PanelTester.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DatabasePath {
            get {
                return ((string)(this["DatabasePath"]));
            }
            set {
                this["DatabasePath"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("USB001")]
        public string PrinterDeviceName {
            get {
                return ((string)(this["PrinterDeviceName"]));
            }
            set {
                this["PrinterDeviceName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("25")]
        public int PrinterDarkness {
            get {
                return ((int)(this["PrinterDarkness"]));
            }
            set {
                this["PrinterDarkness"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("120")]
        public int BarcodeHorzOffset {
            get {
                return ((int)(this["BarcodeHorzOffset"]));
            }
            set {
                this["BarcodeHorzOffset"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("9")]
        public int BarcodeThickBarWidth {
            get {
                return ((int)(this["BarcodeThickBarWidth"]));
            }
            set {
                this["BarcodeThickBarWidth"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("3")]
        public int BarcodeNarrowBarWidth {
            get {
                return ((int)(this["BarcodeNarrowBarWidth"]));
            }
            set {
                this["BarcodeNarrowBarWidth"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("opc.tcp://192.168.2.91:4840")]
        public string PLCAddress {
            get {
                return ((string)(this["PLCAddress"]));
            }
            set {
                this["PLCAddress"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("ns=4;s=|var|DC2007W V TS 0.8S 1131 NTL.Application.GVL")]
        public string PLCNodeId {
            get {
                return ((string)(this["PLCNodeId"]));
            }
            set {
                this["PLCNodeId"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool ShowVirtualKeyboard {
            get {
                return ((bool)(this["ShowVirtualKeyboard"]));
            }
            set {
                this["ShowVirtualKeyboard"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1.4.3")]
        public string AppVersion {
            get {
                return ((string)(this["AppVersion"]));
            }
            set {
                this["AppVersion"] = value;
            }
        }
    }
}
