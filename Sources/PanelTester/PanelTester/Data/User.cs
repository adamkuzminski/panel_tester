﻿using System.ComponentModel;

namespace PanelTester.Data
{
    public class User : BaseEntity
    {
        [DisplayName("Nazwa")]
        public string Name { get; set; }

        [Browsable(false)]
        public string Password { get; set; }

        [DisplayName("Rola")]
        public Role Role { get; set; }
    }
}