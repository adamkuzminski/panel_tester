﻿using PanelTester.Common;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace PanelTester.Data
{
    public class SocketPin : BasePin
    {
        public SocketPin()
        {
        }

        public SocketPin(int number, SocketType socketType, SocketPinType pinType, string nodeId) : this(number, socketType, pinType, nodeId, null, false)
        {
        }

        public SocketPin(int number, SocketType socketType, SocketPinType pinType, string nodeId, int connectedPin) : this(number, socketType, pinType, nodeId, new int[] { connectedPin }, false)
        {
        }

        public SocketPin(int number, SocketType socketType, SocketPinType pinType, string nodeId, int[] connectedPins, bool readOnly = false)
        {
            Number = number;
            SocketType = socketType;
            PinType = pinType;
            NodeId = nodeId;
            ConnectedPins = connectedPins;
            ReadOnly = readOnly;
        }

        public SocketPin(int number, SocketType socketType, SocketPinType pinType, bool readOnly) : this(number, socketType, pinType, "", null, readOnly)
        {
        }

        [Browsable(false)]
        public int[] ConnectedPins { get; set; }

        [Browsable(false)]
        public string NodeId { get; set; }

        [DisplayName("Typ pinu")]
        public SocketPinType PinType { get; set; }

        [Browsable(false)]
        public bool ReadOnly { get; set; }

        [DisplayName("Typ złącza")]
        public SocketType SocketType { get; set; }

        public override string GetLabelPin()
        {
            if (ConnectedPins == null)
                return Number.ToString();
            else
                return String.Format("{0},{1}", Number, string.Join(",", ConnectedPins));
        }

        public override int GetParentType()
        {
            return (int)SocketType;
        }

        public override int GetRangeMax()
        {
            switch (PinType)
            {
                case SocketPinType.In10VDC:
                case SocketPinType.Out10VDC:
                    return 10;

                case SocketPinType.InCurrent:
                    return 20;

                default:
                    return 0;
            }
        }

        public override int GetRangeMin()
        {
            switch (PinType)
            {
                case SocketPinType.In10VDC:
                case SocketPinType.Out10VDC:
                default:
                    return 0;

                case SocketPinType.InCurrent:
                    return 4;
            }
        }

        public override bool IsInteger()
        {
            return PinType == SocketPinType.InCurrent;
        }

        public override bool IsLogic()
        {
            return
                PinType.In(
                    SocketPinType.Out230VACL1,
                    SocketPinType.Out230VACL2,
                    SocketPinType.Out230VACL3,
                    SocketPinType.In230VAC,
                    SocketPinType.Out24VDC,
                    SocketPinType.In24VDC
                    );
        }

        public override bool IsSocket()
        {
            return true;
        }

        public override bool IsVariable()
        {
            return
                !ReadOnly &&
                PinType.In(
                    SocketPinType.Out230VACL1,
                    SocketPinType.Out230VACL2,
                    SocketPinType.Out230VACL3,
                    SocketPinType.In230VAC,
                    SocketPinType.Out24VDC,
                    SocketPinType.Out10VDC,
                    SocketPinType.In24VDC,
                    SocketPinType.In10VDC,
                    SocketPinType.InCurrent
                    );
        }

        protected override string GetSymbol()
        {
            return typeof(SocketPinType).GetMember(PinType.ToString()).FirstOrDefault().GetCustomAttribute<PinAttribute>().Label;
        }

        protected override bool IsInUse()
        {
            if (PinType == SocketPinType.NotUsed)
                return false;
            else if ((PinType == SocketPinType.Resistance) || (PinType == SocketPinType.OutN) || (PinType == SocketPinType.OutPE))
                return true;
            else
                return inUse;
        }
    }
}