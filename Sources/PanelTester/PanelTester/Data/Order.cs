﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace PanelTester.Data
{
    public class Order : BaseEntity
    {
        public Order() : base()
        {
        }

        [DisplayName("Zlecenie")]
        public string OrderNumber { get; set; }

        [DisplayName("Model panelu")]
        public string Model { get; set; }

        [DisplayName("Numer dokumentacji modelu")]
        public string ModelDocNumber { get; set; }

        [DisplayName("Nazwa produktu")]
        public string ProductName { get; set; }

        [Browsable(false)]
        public string TestCaseId { get; set; }

        [DisplayName("Scenariusz")]
        public string TestCaseName { get; set; }

        [DisplayName("Wersja")]
        public int TestCaseVersion { get; set; }

        [Browsable(false)]
        public string BoardId { get; set; }

        [DisplayName("Płyta główna")]
        public string BoardName { get; set; }

        [DisplayName("Typ płyty")]
        public BoardType BoardType { get; set; }

        [DisplayName("Deklarowana ilość paneli")]
        public int DeclaredQuantity { get; set; }

        [DisplayName("Data rozpoczęcia")]
        public DateTime StartDate { get; set; }

        [DisplayName("Data zakończenia")]
        public DateTime EndDate { get; set; }
    }
}