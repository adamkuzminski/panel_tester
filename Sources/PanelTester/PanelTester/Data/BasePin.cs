﻿using System.ComponentModel;

namespace PanelTester.Data
{
    public abstract class BasePin
    {
        protected bool inUse;

        [DisplayName("Etykieta")]
        public string Caption { get; set; }

        [DisplayName("W użyciu")]
        public bool InUse
        {
            get
            {
                return IsInUse();
            }
            set => inUse = value;
        }

        [DisplayName("Numer")]
        public int Number { get; set; }

        public string GetLabel()
        {
            if ((Caption == null) || (Caption.Length == 0))
                return string.Format("{0}. {1}", GetLabelPin(), GetSymbol());
            else
                return string.Format("{0}. {1}", GetLabelPin(), Caption);
        }

        public abstract string GetLabelPin();

        public abstract int GetParentType();

        public abstract int GetRangeMax();

        public abstract int GetRangeMin();

        //If pin value is integer
        public abstract bool IsInteger();

        //If pin allows only 0/1 values
        public abstract bool IsLogic();

        public abstract bool IsSocket();
        //If pin is editable in a step
        public abstract bool IsVariable();

        protected abstract string GetSymbol();

        protected virtual bool IsInUse()
        {
            return inUse;
        }
    }
}