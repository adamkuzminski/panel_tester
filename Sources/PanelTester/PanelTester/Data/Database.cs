﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PanelTester.Data
{
    public class Database
    {
        private const string KTestCaseDirName = "TestCases";
        private const string KTestResultDirName = "TestResults";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<User> UserList;

        public List<Board> BoardList;

        public String DatabasePath { get; set; }

        public List<Order> OrderList { get; set; }

        public List<TestCase> TestCaseList { get; set; }

        public static Database GetInstance()
        {
            return AppEngine.GetInstance().Database;
        }

        public string CreateTestCaseFileName(string id, string name)
        {
            string dir = Path.Combine(DatabasePath, KTestCaseDirName, String.Format(DateTime.Now.ToString("yyyyMMddHHmmss_{0}_{1}"), CleanFileName(name), Convert.ToString(id)));
            Directory.CreateDirectory(dir);
            return Path.Combine(dir, "testCase.dat");
        }

        private string CleanFileName(string fileName)
        {
            return Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
        }

        public bool DeleteOrder(Order order)
        {
            try
            {
                string fileName = Path.Combine(DatabasePath, KTestResultDirName, order.OrderNumber + ".dat");
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                    return true;
                }
            }
            catch (Exception e)
            {
                log.Error("DeleteOrder", e);
            }

            return false;
        }

        public bool DeleteOrderTestResult(Order order)
        {
            try
            {
                string fileName = Path.Combine(DatabasePath, KTestResultDirName, order.OrderNumber + ".res");
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                    return true;
                }
            }
            catch (Exception e)
            {
                log.Error("DeleteOrderTestResult", e);
            }

            return false;
        }

        public bool DeleteTestCase(TestCase testCase)
        {
            TestCaseList.Remove(testCase);
            Directory.Delete(Path.GetDirectoryName(testCase.FileName), true);
            return true;
        }

        public T FindById<T>(string id)
        {
            foreach (Object list in GetDataLists())
            {
                if ((list != null) && list.GetType().GetGenericArguments().Single().Equals(typeof(T)))
                {
                    List<T> refList = list as List<T>;
                    foreach (T t in refList)
                    {
                        if ((t is BaseEntity) && ((t as BaseEntity).Id == id))
                            return t;
                    }
                }
            }
            return default(T);
        }

        public TestCase FindTestCaseByModel(string model)
        {
            if ((model == null) || (model.Length == 0))
                return null;

            TestCase result = null;
            int version = -1;
            foreach (TestCase testCase in TestCaseList)
            {
                if (model.Equals(testCase.Model, StringComparison.CurrentCultureIgnoreCase) && (testCase.Version > version))
                {
                    result = testCase;
                    version = testCase.Version;
                }
            }

            return result;
        }

        public bool Load(String path)
        {
            if (!Directory.Exists(path))
            {
                DirectoryInfo dirInfo = Directory.CreateDirectory(path);
                if (!dirInfo.Exists)
                {
                    log.Error("Cannot create directory: " + path);
                    return false;
                }
            }
            DatabasePath = path;
            Directory.CreateDirectory(Path.Combine(path, KTestCaseDirName));
            Directory.CreateDirectory(Path.Combine(path, KTestResultDirName));

            LoadData(ref UserList);
            if (UserList.Count == 0)
            {
                UserList.Add(new User
                {
                    Id = "Superadmin",
                    Name = "Superadmin",
                    Password = "1qaz!QAZ",
                    Role = Role.Superadmin
                });
                UserList.Add(new User
                {
                    Id = "Admin",
                    Name = "Admin",
                    Password = "Backer123*",
                    Role = Role.Admin
                });
                SaveData(UserList);
            }

            LoadData(ref BoardList);

            LoadOrderData();
            LoadTestCaseData();

            return true;
        }

        public bool LoadData<T>(ref List<T> list)
        {
            list = new List<T>();
            try
            {
                string fileName = Path.Combine(DatabasePath, typeof(T).Name + ".dat");
                foreach (String line in File.ReadAllLines(fileName))
                {
                    try
                    {
                        T t = JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(Convert.FromBase64String(line)));
                        if (t != null)
                            list.Add(t);
                    }
                    catch (Exception e)
                    {
                        log.Error("LoadData.Line " + typeof(T).Name, e);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("LoadData " + typeof(T).Name, e);
                return false;
            }

            return true;
        }

        public bool LoadOrderData()
        {
            OrderList = new List<Order>();
            try
            {
                string path = Path.Combine(DatabasePath, KTestResultDirName);
                foreach (string fileName in Directory.EnumerateFiles(path, "*.dat"))
                {
                    try
                    {
                        Order order = JsonConvert.DeserializeObject<Order>(Encoding.UTF8.GetString(Convert.FromBase64String(File.ReadAllText(fileName))));
                        if (order != null)
                            OrderList.Add(order);
                    }
                    catch (Exception e)
                    {
                        log.Error("LoadOrderData " + fileName, e);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("LoadOrderData", e);
            }

            return true;
        }

        public OrderTestResult LoadOrderTestResult(Order order)
        {
            try
            {
                string fileName = Path.Combine(DatabasePath, KTestResultDirName, order.OrderNumber + ".res");
                if (File.Exists(fileName))
                    return JsonConvert.DeserializeObject<OrderTestResult>(Encoding.UTF8.GetString(Convert.FromBase64String(File.ReadAllText(fileName))));
                else
                    return null;
            }
            catch (Exception e)
            {
                log.Error("LoadOrderTestResult", e);
            }

            return null;
        }

        public TestCase LoadTestCase(string fileName)
        {
            TestCase testCase = JsonConvert.DeserializeObject<TestCase>(Encoding.UTF8.GetString(Convert.FromBase64String(File.ReadAllText(fileName))));
            if (testCase == null)
                return null;
            testCase.FileName = fileName;
            return testCase;
        }

        public bool LoadTestCaseData()
        {
            TestCaseList = new List<TestCase>();
            try
            {
                string path = Path.Combine(DatabasePath, KTestCaseDirName);
                Directory.CreateDirectory(path);
                foreach (string dir in Directory.EnumerateDirectories(path))
                {
                    string fileName = Path.Combine(dir, "testCase.dat");
                    try
                    {
                        TestCase testCase = LoadTestCase(fileName);
                        if (testCase != null)
                            TestCaseList.Add(testCase);
                    }
                    catch (Exception e)
                    {
                        if (!File.Exists(fileName))
                            Directory.Delete(dir, true);
                        log.Error("LoadTestCaseData " + dir, e);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("LoadTestCaseData", e);
            }

            return true;
        }

        public bool Rollback<T>(T record)
        {
            foreach (Object list in GetDataLists())
            {
                if ((list != null) && list.GetType().GetGenericArguments().Single().Equals(typeof(T)))
                {
                    List<T> refList = list as List<T>;
                    LoadData(ref refList);
                }
            }

            return false;
        }

        public bool SaveData<T>(List<T> list)
        {
            if (list == null)
                return false;

            try
            {
                string fileName = Path.Combine(DatabasePath, typeof(T).Name + ".dat");
                using (StreamWriter file = new StreamWriter(fileName))
                {
                    foreach (T t in list)
                    {
                        try
                        {
                            string line = Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(t)));
                            if (line.Length > 0)
                                file.WriteLine(line);
                        }
                        catch (Exception e)
                        {
                            log.Error("SaveData.Line " + typeof(T).Name, e);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("SaveData " + typeof(T).Name, e);
                return false;
            }

            return true;
        }

        public bool SaveOrder(Order order)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(Path.Combine(DatabasePath, KTestResultDirName, order.OrderNumber + ".dat")))
                {
                    string line = Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(order)));
                    if (line.Length > 0)
                        file.WriteLine(line);
                }

                return true;
            }
            catch (Exception e)
            {
                log.Error("SaveOrder", e);
                return false;
            }
        }

        public bool SaveOrderTestResult(OrderTestResult orderTestResult)
        {
            try
            {
                string dir = Path.Combine(DatabasePath, KTestResultDirName);
                string fileName = Path.Combine(dir, orderTestResult.OrderNumber + ".res");

                using (StreamWriter file = new StreamWriter(fileName))
                {
                    string line = Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(orderTestResult)));
                    if (line.Length > 0)
                    {
                        file.WriteLine(line);
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("LoadOrderTestResult", e);
            }

            return false;
        }

        public bool SaveTestCase(TestCase testCase)
        {
            try
            {
                if (testCase == null)
                    return false;

                using (StreamWriter file = new StreamWriter(testCase.FileName))
                {
                    string line = Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(testCase)));
                    if (line.Length > 0)
                        file.WriteLine(line);
                }

                return true;
            }
            catch (Exception e)
            {
                log.Error("SaveTestCase " + Convert.ToString(testCase.Id), e);
                return false;
            }
        }

        private Object[] GetDataLists()
        {
            return new Object[]
            {
                UserList,
                BoardList,
                TestCaseList,
                OrderList
            };
        }
    }
}