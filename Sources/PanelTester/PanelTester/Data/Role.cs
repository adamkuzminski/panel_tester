﻿namespace PanelTester.Data
{
    public enum Role
    {
        Operator,
        Engineer,
        Admin,
        Superadmin
    }
}