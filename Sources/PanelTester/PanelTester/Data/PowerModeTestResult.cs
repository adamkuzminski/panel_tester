﻿using PanelTester.Common;
using System;
using System.Collections.Generic;

namespace PanelTester.Data
{
    public class PowerModeTestResult
    {
        public PowerModeTestResult()
        {
            ItemList = new List<PowerModeItemTestResult>();
        }

        public DateTime EndDate { get; set; }

        public List<string> ErrorImageList { get; set; }

        public List<string> WrongInputList { get; set; }

        public List<string> InfoList { get; set; }

        public List<PowerModeItemTestResult> ItemList { get; set; }

        public TestResult Result { get; set; }

        public DateTime StartDate { get; set; }

        public void AddErrorImage(string errorImage, bool first = false)
        {
            errorImage = Utils.GetString(errorImage);
            if (errorImage.Length > 0)
            {
                if (ErrorImageList == null)
                    ErrorImageList = new List<string>();
                if (first)
                    ErrorImageList.Insert(0, errorImage);
                else
                    ErrorImageList.Add(errorImage);
            }
        }

        public void AddWrongInput(string input)
        {
            if (WrongInputList == null)
                WrongInputList = new List<string>();
            WrongInputList.Add(input);
        }

        public void AddInfoText(string text, bool first = false)
        {
            text = Utils.GetString(text);
            if (text.Length > 0)
            {
                if (InfoList == null)
                    InfoList = new List<string>();
                if (first)
                    InfoList.Insert(0, text);
                else
                    InfoList.Add(text);
            }
        }
        public TestStatus GetTestStatus()
        {
            if (StartDate == default(DateTime))
                return TestStatus.NotTested;
            else if (EndDate == default(DateTime))
                return TestStatus.Testing;
            else
                return TestStatus.Tested;
        }
    }
}