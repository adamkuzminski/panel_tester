﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

namespace PanelTester.Data
{
    public class TestCase : BaseEntity
    {
        private string fileName;

        private Board board;

        public TestCase() : base()
        {
            IsActive = false;
            Version = 1;

            SocketList = new Socket[(int)SocketType.Count];
            for (int i = 0; i < SocketList.Length; i++)
                SocketList[i] = new Socket((SocketType)i);

            RegistrySetList = new BoardRegistrySet[(int)BoardRegistryType.Count];
            for (int i = 0; i < RegistrySetList.Length; i++)
                RegistrySetList[i] = new BoardRegistrySet((BoardRegistryType)i);

            PowerModeList = new List<PowerMode>();

            StepList = new List<TestCaseStep>();
        }

        [DisplayName("Nazwa")]
        public string Name { get; set; }

        [DisplayName("Model panelu")]
        public string Model { get; set; }

        [DisplayName("Numer dokumentacji modelu")]
        public string ModelDocNumber { get; set; }

        [DisplayName("Nazwa produktu")]
        public string ProductName { get; set; }

        [DisplayName("Wersja scenariusza")]
        public int Version { get; set; }

        [Browsable(false)]
        public string BoardId { get; set; }

        [DisplayName("Płyta główna")]
        public string BoardName
        {
            get
            {
                return GetBoard()?.Name;
            }
        }

        [DisplayName("Komentarz")]
        public string Comment { get; set; }

        [Browsable(false)]
        public string FileName
        {
            get
            {
                if (fileName == null)
                    fileName = Database.GetInstance().CreateTestCaseFileName(Id, Name);
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        [DisplayName("Aktywny")]
        public bool IsActive { get; set; }

        [Browsable(false)]
        public List<PowerMode> PowerModeList { get; set; }

        [Browsable(false)]
        public BoardRegistrySet[] RegistrySetList { get; set; }

        [DisplayName("Rezystancja")]
        public int Resistance { get; set; }

        [Browsable(false)]
        public Socket[] SocketList { get; set; }

        [Browsable(false)]
        public List<TestCaseStep> StepList { get; set; }

        public TestCase Copy(bool copySteps)
        {
            Board board = GetBoard();
            if (board == null)
                return null;

            TestCase testCase = new TestCase();
            testCase.Comment = Comment;
            testCase.IsActive = false;
            testCase.Model = Model;
            testCase.ModelDocNumber = ModelDocNumber;
            testCase.Name = Name;
            testCase.ProductName = ProductName;
            testCase.Resistance = Resistance;

            testCase.SetBoard(board);

            testCase.PowerModeList.AddRange(PowerModeList);
            string destImagePath = testCase.GetErrorImagePath();
            Directory.CreateDirectory(destImagePath);
            
            for (int i = 0; i < RegistrySetList.Length; i++)
                testCase.RegistrySetList[i] = RegistrySetList[i].Copy();

            for (int i = 0; i < SocketList.Length; i++)
                for (int j = 0; j < SocketList[i].PinList.Length; j++)
                {
                    testCase.SocketList[i].PinList[j].Caption = SocketList[i].PinList[j].Caption;
                    testCase.SocketList[i].PinList[j].InUse = SocketList[i].PinList[j].InUse;
                }

            if (copySteps)
            {
                string srcImagePath = GetErrorImagePath();
                foreach (TestCaseStep testCaseStep in StepList)
                {
                    TestCaseStep clone = testCaseStep.Clone(true);
                    try
                    {
                        if ((clone.ErrorImage != null) && (clone.ErrorImage.Length > 0))
                        {
                            string fileName = Path.GetFileName(clone.ErrorImage);
                            File.Copy(Path.Combine(srcImagePath, fileName), Path.Combine(destImagePath, fileName), true);
                        }
                    }
                    catch (Exception e)
                    {
                    }
                    testCase.StepList.Add(clone);
                }
            }

            testCase.Version = Version + 1;
            Database database = Database.GetInstance();
            foreach (TestCase tc in database.TestCaseList)
                if (tc.Model.Equals(Model) && (tc.Version >= testCase.Version))
                {
                    testCase.Version = tc.Version + 1;
                }

            
            return testCase;
        }

        public Board GetBoard()
        {
            if ((board == null) || (board.Id != BoardId))
                board = Database.GetInstance().FindById<Board>(BoardId);
            return board;
        }

        public string GetErrorImagePath()
        {
            if (FileName == null)
                return "";
            else
                return Path.Combine(Path.GetDirectoryName(FileName), "ErrorImages");
        }

        public override bool IsLocked()
        {
            Database database = Database.GetInstance();
            foreach (Order order in database.OrderList)
            {
                if ((order.TestCaseId == Id) && (order.TestCaseVersion == Version))
                    return true;
            }
            return false;
        }

        public void SetBoard(Board board)
        {
            BoardId = board.Id;
            this.board = board;

            RegistrySetList = new BoardRegistrySet[board.RegistrySetList.Length];
            for (int i = 0; i < board.RegistrySetList.Length; i++)
            {
                RegistrySetList[i] = board.RegistrySetList[i].Copy();
                foreach (BoardRegistry boardRegistry in RegistrySetList[i].RegistryList)
                    boardRegistry.InUse = true;
            }
        }
    }
}