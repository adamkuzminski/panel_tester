﻿namespace PanelTester.Data
{
    public enum TestResult
    {
        Unknown = 0,
        Error,
        OK
    }
}