﻿namespace PanelTester.Data
{
    public class TestCaseStepInputItem
    {
        private double maxValue;

        private double minValue;

        public TestCaseStepInputItem(PowerMode powerMode)
        {
            PowerMode = powerMode;
        }

        public string ErrorImage { get; set; }

        public string ErrorMessage { get; set; }

        public bool InUse { get; set; }

        public bool IsFatalError { get; set; }

        public double MaxValue
        {
            get => maxValue; set
            {
                maxValue = value;
                if (maxValue < minValue)
                    minValue = maxValue;
            }
        }

        public double MinValue
        {
            get => minValue; set
            {
                minValue = value;
                if (minValue > maxValue)
                    maxValue = minValue;
            }
        }

        public PowerMode PowerMode { get; set; }

        public TestCaseStepInputItem Clone(bool copyValues)
        {
            TestCaseStepInputItem inputItem = new TestCaseStepInputItem(PowerMode);
            inputItem.ErrorImage = copyValues ? ErrorImage : "";
            inputItem.ErrorMessage = copyValues ? ErrorMessage : "";
            inputItem.InUse = copyValues ? InUse : false;
            inputItem.IsFatalError = copyValues ? IsFatalError : true;
            inputItem.maxValue = copyValues ? maxValue : 0;
            inputItem.minValue = copyValues ? minValue : 0;
            return inputItem;
        }

        public void Update(TestCaseStepInputItem refItem)
        {
            ErrorImage = refItem.ErrorImage;
            ErrorMessage = refItem.ErrorMessage;
            InUse = refItem.InUse;
            IsFatalError = refItem.IsFatalError;
            MaxValue = refItem.MaxValue;
            MinValue = refItem.MinValue;
        }
    }
}