﻿namespace PanelTester.Data
{
    public enum SocketType
    {
        Unknown = -1,
        AOut,
        AIn,
        DOut,
        DIn,
        Count
    }
}