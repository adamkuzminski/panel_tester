﻿namespace PanelTester.Data
{
    public enum BoardRegistryType
    {
        Unknown = -1,
        AOut,
        AIn,
        DOut,
        DIn,
        Count
    }
}