﻿namespace PanelTester.Data
{
    public enum BoardType
    {
        Unknown = 0,
        GP,
        WPIO,
        HZIO,
        MAXIO,
        LIN_Exp
    }
}