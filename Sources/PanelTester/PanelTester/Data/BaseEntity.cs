﻿using System;
using System.ComponentModel;

namespace PanelTester.Data
{
    public class BaseEntity
    {
        [Browsable(false)]
        public string Id { get; set; }

        [DisplayName("Autor")]
        public string Author { get; set; }

        [DisplayName("Identyfikator autora")]
        public string AuthorUserId { get; set; }

        [DisplayName("Data wprowadzenia")]
        public DateTime CreateTime { get; set; }

        public BaseEntity()
        {
            Id = Guid.NewGuid().ToString("N");

            User user = AppEngine.GetInstance().User;
            if (user != null)
            {
                Author = user.Name;
                AuthorUserId = user.Id;
            }
            CreateTime = DateTime.Now;
        }

        public virtual bool IsLocked()
        {
            return false;
        }
    }
}