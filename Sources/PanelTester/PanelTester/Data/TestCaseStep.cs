﻿using System.Collections.Generic;

namespace PanelTester.Data
{
    public class TestCaseStep
    {
        public List<TestCaseStepInput> InputList;

        public List<TestCaseStepOutput> OutputList;

        public TestCaseStep()
        {
            Delay = 10000;
            InputList = new List<TestCaseStepInput>();
            OutputList = new List<TestCaseStepOutput>();
        }

        public int Delay { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorImage { get; set; }

        public TestCaseStep Clone(bool copyValues)
        {
            TestCaseStep step = new TestCaseStep();
            step.Delay = Delay;
            step.ErrorMessage = ErrorMessage;
            step.ErrorImage = ErrorImage;

            foreach (TestCaseStepInput input in InputList)
                step.InputList.Add(input.Clone(copyValues));
            foreach (TestCaseStepOutput output in OutputList)
                step.OutputList.Add(output.Clone(copyValues));
            return step;
        }

        public void Update(TestCaseStep refStep)
        {
            Delay = refStep.Delay;
            ErrorImage = refStep.ErrorImage;
            ErrorMessage = refStep.ErrorMessage;

            List<TestCaseStepInput> refInputList = refStep.InputList;
            foreach (TestCaseStepInput input in InputList)
                input.Update(refInputList);
            List<TestCaseStepOutput> refOutputList = refStep.OutputList;
            foreach (TestCaseStepOutput output in OutputList)
                output.Update(refOutputList);
        }
    }
}