﻿namespace PanelTester.Data
{
    public enum TestStatus
    {
        NotTested = 0,
        Testing,
        Tested
    }
}