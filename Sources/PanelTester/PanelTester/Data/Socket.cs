﻿using System.Collections.Generic;

namespace PanelTester.Data
{
    public class Socket
    {
        public Socket(SocketType socketType)
        {
            Type = socketType;
            PinList = GetSocketPins(socketType);
        }

        public SocketPin[] PinList { get; set; }

        public SocketType Type { get; set; }

        public static SocketPin GetSocketPinByType(SocketType socketType, SocketPinType socketPinType)
        {
            foreach (SocketPin socketPin in GetSocketPins(socketType))
                if (socketPin.PinType == socketPinType)
                    return socketPin;
            return null;
        }

        public static SocketPin GetSocketPinByType(SocketPinType socketPinType)
        {
            for (int i = 0; i < (int)SocketType.Count; i++)
            {
                SocketPin socketPin = GetSocketPinByType((SocketType)i, socketPinType);
                if (socketPin != null)
                    return socketPin;
            }
            return null;
        }

        public static List<SocketPin> GetSocketPinListByType(SocketPinType socketPinType)
        {
            List<SocketPin> list = new List<SocketPin>();
            for (int i = 0; i < (int)SocketType.Count; i++)
            {
                foreach (SocketPin socketPin in GetSocketPins((SocketType)i))
                    if (socketPin.PinType == socketPinType)
                        list.Add(socketPin);
            }
            return list;
        }

        public static List<SocketPin> GetSocketPinListByType(Socket[] socketList, SocketPinType socketPinType)
        {
            List<SocketPin> result = new List<SocketPin>();
            foreach (Socket socket in socketList)
                foreach (SocketPin pin in socket.PinList)
                    if (pin.InUse && (pin.PinType == socketPinType))
                        result.Add(pin);
            return result;
        }

        public static SocketPin[] GetSocketPins(SocketType socketType)
        {
            switch (socketType)
            {
                case SocketType.AOut:
                    return new SocketPin[]
                    {
                        new SocketPin(1, socketType, SocketPinType.Out230VACL1Power, "Zal_L1_Zasil"),
                        new SocketPin(2, socketType, SocketPinType.Out230VACL1, "Zal_L1", new int[] {3, 4 }),
                        new SocketPin(3, socketType, SocketPinType.Out230VACL1, true),
                        new SocketPin(4, socketType, SocketPinType.Out230VACL1, true),
                        new SocketPin(5, socketType, SocketPinType.Out230VACL2Power, "Zal_L2_Zasil"),
                        new SocketPin(6, socketType, SocketPinType.Out230VACL2, "Zal_L2", new int[] {7, 8 }),
                        new SocketPin(7, socketType, SocketPinType.Out230VACL2, true),
                        new SocketPin(8, socketType, SocketPinType.Out230VACL2, true),
                        new SocketPin(9, socketType, SocketPinType.Out230VACL3Power, "Zal_L3_Zasil"),
                        new SocketPin(10, socketType, SocketPinType.Out230VACL3, "Zal_L3", new int[] {11, 12 }),
                        new SocketPin(11, socketType, SocketPinType.Out230VACL3, true),
                        new SocketPin(12, socketType, SocketPinType.Out230VACL3, true),
                        new SocketPin(13, socketType, SocketPinType.OutN, "", new int[] {14, 15, 16 }),
                        new SocketPin(14, socketType, SocketPinType.OutN, true),
                        new SocketPin(15, socketType, SocketPinType.OutN, true),
                        new SocketPin(16, socketType, SocketPinType.OutN, true),
                        new SocketPin(17, socketType, SocketPinType.OutPE, "", new int[] {18, 19, 20 }),
                        new SocketPin(18, socketType, SocketPinType.OutPE, true),
                        new SocketPin(19, socketType, SocketPinType.OutPE, true),
                        new SocketPin(20, socketType, SocketPinType.OutPE, true),
                        new SocketPin(21, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(22, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(23, socketType, SocketPinType.Resistance, true),
                        new SocketPin(24, socketType, SocketPinType.Resistance, true)
                    };

                case SocketType.AIn:
                    return new SocketPin[]
                    {
                        new SocketPin(1, socketType, SocketPinType.In230VAC, "Odczyt_Wej1_230V", 2),
                        new SocketPin(2, socketType, SocketPinType.InN, "Zal_Odczyt_Ciaglosci_N_KQ110", null, true),
                        new SocketPin(3, socketType, SocketPinType.In230VAC, "Odczyt_Wej2_230V", 4),
                        new SocketPin(4, socketType, SocketPinType.InN, "Zal_Odczyt_Ciaglosci_N_KQ111", null, true),
                        new SocketPin(5, socketType, SocketPinType.In230VAC, "Odczyt_Wej3_230V", 6),
                        new SocketPin(6, socketType, SocketPinType.InN, "Zal_Odczyt_Ciaglosci_N_KQ112", null, true),
                        new SocketPin(7, socketType, SocketPinType.In230VAC, "Odczyt_Wej4_230V", 8),
                        new SocketPin(8, socketType, SocketPinType.InN, "Zal_Odczyt_Ciaglosci_N_KQ113", null, true),
                        new SocketPin(9, socketType, SocketPinType.In230VAC, "Odczyt_Wej5_230V", 10),
                        new SocketPin(10, socketType, SocketPinType.InN, "Zal_Odczyt_Ciaglosci_N_KQ114", null, true),
                        new SocketPin(11, socketType, SocketPinType.In230VAC, "Odczyt_Wej6_230V", 12),
                        new SocketPin(12, socketType, SocketPinType.InN, "Zal_Odczyt_Ciaglosci_N_KQ115", null, true),
                        new SocketPin(13, socketType, SocketPinType.In230VAC, "Odczyt_Wej7_230V_wspolne_N"),
                        new SocketPin(14, socketType, SocketPinType.NotUsed, true),//, "Zal_Odczyt_Ciaglosci_N_KQ116", null, true),
                        new SocketPin(15, socketType, SocketPinType.In230VAC, "Odczyt_Wej8_230V_wspolne_N"),
                        new SocketPin(16, socketType, SocketPinType.NotUsed, true), //InN, "Zal_Odczyt_Ciaglosci_N_KQ117", null, true),
                        new SocketPin(17, socketType, SocketPinType.In230VAC, "Odczyt_Wej9_230V_wspolne_N"),
                        new SocketPin(18, socketType, SocketPinType.In230VAC, "Odczyt_Wej10_230V_wspolne_N"),
                        new SocketPin(19, socketType, SocketPinType.In230VAC, "Odczyt_Wej11_230V_wspolne_N"),
                        new SocketPin(20, socketType, SocketPinType.In230VAC, "Odczyt_Wej12_230V_wspolne_N"),
                        new SocketPin(21, socketType, SocketPinType.NotUsed, true),// InN, "Zal_Odczyt_Ciaglosci_N_KQ118_KQ121", null, true),
                        new SocketPin(22, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(23, socketType, SocketPinType.Resistance, true),
                        new SocketPin(24, socketType, SocketPinType.Resistance, true)
                    };

                case SocketType.DOut:
                    return new SocketPin[]
                    {
                        new SocketPin(1, socketType, SocketPinType.Out24VDC, "Zal_24V_1", 13),
                        new SocketPin(2, socketType, SocketPinType.Out24VDC, "Zal_24V_2", 14),
                        new SocketPin(3, socketType, SocketPinType.Out24VDC, "Zal_24V_3", 15),
                        new SocketPin(4, socketType, SocketPinType.Out24VDC, "Zal_24V_4", 16),
                        new SocketPin(5, socketType, SocketPinType.Out24VDC, "Zal_24V_5", 17),
                        new SocketPin(6, socketType, SocketPinType.Out24VDC, "Zal_24V_6", 18),
                        new SocketPin(7, socketType, SocketPinType.Out24VDC, "Zal_24V_7", 19),
                        new SocketPin(8, socketType, SocketPinType.Out24VDC, "Zal_24V_8", 20),
                        new SocketPin(9, socketType, SocketPinType.Out24VDC, "Zal_24V_9", 21),
                        new SocketPin(10, socketType, SocketPinType.Out24VDC, "Zal_24V_10", 22),
                        new SocketPin(11, socketType, SocketPinType.Out24VDC, "Zal_24V_11", 23),
                        new SocketPin(12, socketType, SocketPinType.Out24VDC, "Zal_24V_12", 24),
                        new SocketPin(13, socketType, SocketPinType.OutGND, true),
                        new SocketPin(14, socketType, SocketPinType.OutGND, true),
                        new SocketPin(15, socketType, SocketPinType.OutGND, true),
                        new SocketPin(16, socketType, SocketPinType.OutGND, true),
                        new SocketPin(17, socketType, SocketPinType.OutGND, true),
                        new SocketPin(18, socketType, SocketPinType.OutGND, true),
                        new SocketPin(19, socketType, SocketPinType.OutGND, true),
                        new SocketPin(20, socketType, SocketPinType.OutGND, true),
                        new SocketPin(21, socketType, SocketPinType.OutGND, true),
                        new SocketPin(22, socketType, SocketPinType.OutGND, true),
                        new SocketPin(23, socketType, SocketPinType.OutGND, true),
                        new SocketPin(24, socketType, SocketPinType.OutGND, true),
                        new SocketPin(25, socketType, SocketPinType.Out10VDC, "Wyslij_War_0_10V_1", 26),
                        new SocketPin(26, socketType, SocketPinType.OutGND, true),
                        new SocketPin(27, socketType, SocketPinType.Out10VDC, "Wyslij_War_0_10V_2", 28),
                        new SocketPin(28, socketType, SocketPinType.OutGND, true),
                        new SocketPin(29, socketType, SocketPinType.Out10VDC, "Wyslij_War_0_10V_3", 30),
                        new SocketPin(30, socketType, SocketPinType.OutGND, true),
                        new SocketPin(31, socketType, SocketPinType.Out10VDC, "Wyslij_War_0_10V_4", 32),
                        new SocketPin(32, socketType, SocketPinType.OutGND, true),
                        new SocketPin(33, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(34, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(35, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(36, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(37, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(38, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(39, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(40, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(41, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(42, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(43, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(44, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(45, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(46, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(47, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(48, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(49, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(50, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(51, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(52, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(53, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(54, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(55, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(56, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(57, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(58, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(59, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(60, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(61, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(62, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(63, socketType, SocketPinType.Resistance, true),
                        new SocketPin(64, socketType, SocketPinType.Resistance, true)
                    };

                case SocketType.DIn:
                    return new SocketPin[]
                    {
                        new SocketPin(1, socketType, SocketPinType.In24VDC, "Odczyt_Wej1_24V", 2),
                        new SocketPin(2, socketType, SocketPinType.InGND, true),
                        new SocketPin(3, socketType, SocketPinType.In24VDC, "Odczyt_Wej2_24V", 4),
                        new SocketPin(4, socketType, SocketPinType.InGND, true),
                        new SocketPin(5, socketType, SocketPinType.In24VDC, "Odczyt_Wej3_24V", 6),
                        new SocketPin(6, socketType, SocketPinType.InGND, true),
                        new SocketPin(7, socketType, SocketPinType.In24VDC, "Odczyt_Wej4_24V", 8),
                        new SocketPin(8, socketType, SocketPinType.InGND, true),
                        new SocketPin(9, socketType, SocketPinType.In24VDC, "Odczyt_Wej5_24V", 10),
                        new SocketPin(10, socketType, SocketPinType.InGND, true),
                        new SocketPin(11, socketType, SocketPinType.In24VDC, "Odczyt_Wej6_24V", 12),
                        new SocketPin(12, socketType, SocketPinType.InGND, true),
                        new SocketPin(13, socketType, SocketPinType.In24VDC, "Odczyt_Wej7_24V", 14),
                        new SocketPin(14, socketType, SocketPinType.InGND, true),
                        new SocketPin(15, socketType, SocketPinType.In24VDC, "Odczyt_Wej8_24V", 16),
                        new SocketPin(16, socketType, SocketPinType.InGND, true),
                        new SocketPin(17, socketType, SocketPinType.In24VDC, "Odczyt_Wej9_24V", 18),
                        new SocketPin(18, socketType, SocketPinType.InGND, true),
                        new SocketPin(19, socketType, SocketPinType.In24VDC, "Odczyt_Wej10_24V", 20),
                        new SocketPin(20, socketType, SocketPinType.InGND, true),
                        new SocketPin(21, socketType, SocketPinType.In24VDC, "Odczyt_Wej11_24V", 22),
                        new SocketPin(22, socketType, SocketPinType.InGND, true),
                        new SocketPin(23, socketType, SocketPinType.In24VDC, "Odczyt_Wej12_24V", 24),
                        new SocketPin(24, socketType, SocketPinType.InGND, true),
                        new SocketPin(25, socketType, SocketPinType.In24VDC, "Odczyt_Wej13_24V_wspolne_GND", 33),
                        new SocketPin(26, socketType, SocketPinType.In24VDC, "Odczyt_Wej14_24V_wspolne_GND", 33),
                        new SocketPin(27, socketType, SocketPinType.In24VDC, "Odczyt_Wej15_24V_wspolne_GND", 33),
                        new SocketPin(28, socketType, SocketPinType.In24VDC, "Odczyt_Wej16_24V_wspolne_GND", 33),
                        new SocketPin(29, socketType, SocketPinType.In24VDC, "Odczyt_Wej17_24V_wspolne_GND", 33),
                        new SocketPin(30, socketType, SocketPinType.In24VDC, "Odczyt_Wej18_24V_wspolne_GND", 33),
                        new SocketPin(31, socketType, SocketPinType.In24VDC, "Odczyt_Wej19_24V_wspolne_GND", 33),
                        new SocketPin(32, socketType, SocketPinType.In24VDC, "Odczyt_Wej20_24V_wspolne_GND", 33),
                        new SocketPin(33, socketType, SocketPinType.InGND, true),
                        new SocketPin(34, socketType, SocketPinType.In10VDC, "Odczyt_War_0_10V_1", 35),
                        new SocketPin(35, socketType, SocketPinType.InGND, true),
                        new SocketPin(36, socketType, SocketPinType.In10VDC, "Odczyt_War_0_10V_2", 37),
                        new SocketPin(37, socketType, SocketPinType.InGND, true),
                        new SocketPin(38, socketType, SocketPinType.In10VDC, "Odczyt_War_0_10V_3", 39),
                        new SocketPin(39, socketType, SocketPinType.InGND, true),
                        new SocketPin(40, socketType, SocketPinType.In10VDC, "Odczyt_War_0_10V_4", 41),
                        new SocketPin(41, socketType, SocketPinType.InGND, true),
                        new SocketPin(42, socketType, SocketPinType.InCurrent, true),// 43),
                        new SocketPin(43, socketType, SocketPinType.InCurrentGND, true),
                        new SocketPin(44, socketType, SocketPinType.InCurrent, true),// 45),
                        new SocketPin(45, socketType, SocketPinType.InCurrentGND, true),
                        new SocketPin(46, socketType, SocketPinType.InCurrent, true),// 47),
                        new SocketPin(47, socketType, SocketPinType.InCurrentGND, true),
                        new SocketPin(48, socketType, SocketPinType.InCurrent, true),//49),
                        new SocketPin(49, socketType, SocketPinType.InCurrentGND, true),
                        new SocketPin(50, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(51, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(52, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(53, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(54, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(55, socketType, SocketPinType.InPE, "Zal_Odczyt_Ciaglosci_PE_1"),
                        new SocketPin(56, socketType, SocketPinType.InPE, "Zal_Odczyt_Ciaglosci_PE_2"),
                        new SocketPin(57, socketType, SocketPinType.InPE, "Zal_Odczyt_Ciaglosci_PE_3"),
                        new SocketPin(58, socketType, SocketPinType.InPE, "Zal_Odczyt_Ciaglosci_PE_4"),
                        new SocketPin(59, socketType, SocketPinType.InPE, "Zal_Odczyt_Ciaglosci_PE_5"),
                        new SocketPin(60, socketType, SocketPinType.InPE, "Zal_Odczyt_Ciaglosci_PE_6"),
                        new SocketPin(61, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(62, socketType, SocketPinType.NotUsed, true),
                        new SocketPin(63, socketType, SocketPinType.Resistance, true),
                        new SocketPin(64, socketType, SocketPinType.Resistance, true)
                    };
            }
            return null;
        }

        public SocketPin GetFirstPinByType(SocketPinType socketPinType)
        {
            foreach (SocketPin socketPin in PinList)
                if (socketPin.PinType == socketPinType)
                    return socketPin;
            return null;
        }

        public SocketPin GetFirstPinInUseByType(SocketPinType socketPinType)
        {
            foreach (SocketPin socketPin in PinList)
                if ((socketPin.PinType == socketPinType) && socketPin.InUse)
                    return socketPin;
            return null;
        }
    }
}