﻿using System;
using System.Collections.Generic;

namespace PanelTester.Data
{
    public class StepTestResult
    {
        public StepTestResult()
        {
        }

        public StepTestResult(List<PowerMode> powerModes)
        {
            PowerModeTestResultList = new PowerModeTestResult[powerModes.Count];
            for (int i = 0; i < powerModes.Count; i++)
                PowerModeTestResultList[i] = new PowerModeTestResult();
        }

        public DateTime EndDate { get; set; }

        public TestResult PowerModeResult0
        {
            get
            {
                return GetPowerModeResult(0);
            }
            set
            {
                SetPowerModeResult(0, value);
            }
        }

        public TestResult PowerModeResult1
        {
            get
            {
                return GetPowerModeResult(1);
            }
            set
            {
                SetPowerModeResult(1, value);
            }
        }

        public TestResult PowerModeResult2
        {
            get
            {
                return GetPowerModeResult(2);
            }
            set
            {
                SetPowerModeResult(2, value);
            }
        }

        public TestResult PowerModeResult3
        {
            get
            {
                return GetPowerModeResult(3);
            }
            set
            {
                SetPowerModeResult(3, value);
            }
        }

        public TestResult PowerModeResult4
        {
            get
            {
                return GetPowerModeResult(4);
            }
            set
            {
                SetPowerModeResult(4, value);
            }
        }

        public TestResult PowerModeResult5
        {
            get
            {
                return GetPowerModeResult(5);
            }
            set
            {
                SetPowerModeResult(5, value);
            }
        }

        public TestResult PowerModeResult6
        {
            get
            {
                return GetPowerModeResult(6);
            }
            set
            {
                SetPowerModeResult(6, value);
            }
        }

        public PowerModeTestResult[] PowerModeTestResultList { get; set; }

        public TestResult Result { get; set; }

        public DateTime StartDate { get; set; }

        public int StepIndex { get; set; }

        public void CalculateResult()
        {
            Result = TestResult.Unknown;
            foreach (PowerModeTestResult powerModeTestResult in PowerModeTestResultList)
                switch (powerModeTestResult.Result)
                {
                    case TestResult.OK:
                        if (Result != TestResult.Error)
                            Result = TestResult.OK;
                        break;

                    case TestResult.Error:
                        Result = TestResult.Error;
                        break;
                }
        }

        public TestResult GetPowerModeResult(int index)
        {
            if (PowerModeTestResultList.Length > index)
                return PowerModeTestResultList[index].Result;
            else
                return TestResult.Unknown;
        }

        public TestStatus GetTestStatus()
        {
            if (StartDate == default(DateTime))
                return TestStatus.NotTested;
            else if (EndDate == default(DateTime))
                return TestStatus.Testing;
            else
                return TestStatus.Tested;
        }

        public void SetPowerModeResult(int index, TestResult value)
        {
            if ((PowerModeTestResultList != null) && (PowerModeTestResultList.Length > index))
                PowerModeTestResultList[index].Result = value;
        }
    }
}