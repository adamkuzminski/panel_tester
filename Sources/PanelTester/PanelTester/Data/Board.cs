﻿using System.ComponentModel;

namespace PanelTester.Data
{
    public class Board : BaseEntity
    {
        public Board() : base()
        {
            AddressIP = "192.168.2.77";
            IsActive = true;
            InstallTimeout = 70;
            UnitID = 1;
            RegistrySetList = new BoardRegistrySet[(int)BoardRegistryType.Count];
            for (int i = 0; i < RegistrySetList.Length; i++)
                RegistrySetList[i] = new BoardRegistrySet((BoardRegistryType)i);
        }

        [DisplayName("Nazwa")]
        public string Name { get; set; }

        [DisplayName("Adres IP")]
        public string AddressIP { get; set; }

        [DisplayName("Unit ID")]
        public byte UnitID { get; set; }

        [DisplayName("Typ")]
        public BoardType Type { get; set; }

        [DisplayName("Wartość rejestru")]
        public byte RegistryValue { get; set; }

        [DisplayName("Czas instalacji")]
        public int InstallTimeout { get; set; }

        [DisplayName("Komentarz")]
        public string Comment { get; set; }

        [DisplayName("Aktywna")]
        public bool IsActive { get; set; }

        [Browsable(false)]
        public BoardRegistrySet[] RegistrySetList;

        public override bool IsLocked()
        {
            Database database = Database.GetInstance();
            foreach (TestCase testCase in database.TestCaseList)
            {
                if (testCase.BoardId == Id)
                    return true;
            }

            return false;
        }

        public Board Copy()
        {
            Board board = new Board();
            board.AddressIP = AddressIP;
            board.Comment = Comment;
            board.IsActive = false;
            board.Name = Name + " - kopia";
            board.UnitID = UnitID;
            board.Type = Type;
            board.RegistryValue = RegistryValue;
            board.InstallTimeout = InstallTimeout;

            for (int i = 0; i < RegistrySetList.Length; i++)
                board.RegistrySetList[i] = RegistrySetList[i].Copy();

            return board;
        }
    }
}