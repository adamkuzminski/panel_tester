﻿using System;

namespace PanelTester.Data
{
    public class PowerModeItemTestResult
    {
        public string ErrorImage { get; set; }

        public string Info { get; set; }

        public int InputIndex { get; set; }

        public bool Result { get; set; }

        public Object Value { get; set; }
    }
}