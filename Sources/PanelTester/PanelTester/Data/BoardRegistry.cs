﻿using PanelTester.Common;
using System;
using System.ComponentModel;

namespace PanelTester.Data
{
    public class BoardRegistry : BasePin
    {
        public BoardRegistry()
        {
        }

        public BoardRegistry(int number, BoardRegistryType type)
        {
            Number = number;
            Type = type;
        }

        [DisplayName("Symbol")]
        public string Symbol { get; set; }

        [DisplayName("Typ")]
        public BoardRegistryType Type { get; set; }

        public BoardRegistry Copy()
        {
            BoardRegistry boardRegistry = new BoardRegistry();
            boardRegistry.Caption = Caption;
            boardRegistry.InUse = InUse;
            boardRegistry.Number = Number;
            boardRegistry.Type = Type;
            boardRegistry.Symbol = Symbol;
            return boardRegistry;
        }

        public override string GetLabelPin()
        {
            return Number.ToString();
        }

        public override int GetParentType()
        {
            return (int)Type;
        }

        public override int GetRangeMax()
        {
            return ushort.MaxValue;
        }

        public override int GetRangeMin()
        {
            return 0;
        }

        public override bool IsInteger()
        {
            return true;
        }

        public override bool IsLogic()
        {
            return Type.In(BoardRegistryType.DIn, BoardRegistryType.DOut);
        }

        public override bool IsSocket()
        {
            return false;
        }

        public override bool IsVariable()
        {
            return true;
        }

        public void SetSymbol(int offset)
        {
            Symbol = String.Format("{0}{1} ({2})", Type.ToString(), Number, Number - 1 + offset);
        }

        protected override string GetSymbol()
        {
            return Symbol;
        }
    }
}