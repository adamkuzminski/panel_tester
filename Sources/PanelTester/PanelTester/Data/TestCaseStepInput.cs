﻿using System.Collections.Generic;
using System.ComponentModel;

namespace PanelTester.Data
{
    public class TestCaseStepInput
    {
        public TestCaseStepInputItem[] ItemList;

        public TestCaseStepInput()
        {
        }

        public TestCaseStepInput(BasePin basePin, List<PowerMode> powerModes)
        {
            Number = basePin.Number;
            Label = basePin.GetLabel();
            RangeMax = basePin.GetRangeMax();
            RangeMin = basePin.GetRangeMin();
            Integer = basePin.IsInteger();
            Logic = basePin.IsLogic();
            Socket = basePin.IsSocket();
            ParentType = basePin.GetParentType();

            ItemList = new TestCaseStepInputItem[powerModes.Count];
            for (int i = 0; i < powerModes.Count; i++)
                ItemList[i] = new TestCaseStepInputItem(powerModes[i]);
        }

        public bool Integer { get; set; }

        public bool InUse0
        {
            get
            {
                return GetInUse(0);
            }
            set
            {
                SetInUse(0, value);
            }
        }

        public bool InUse1
        {
            get
            {
                return GetInUse(1);
            }
            set
            {
                SetInUse(1, value);
            }
        }

        public bool InUse2
        {
            get
            {
                return GetInUse(2);
            }
            set
            {
                SetInUse(2, value);
            }
        }

        public bool InUse3
        {
            get
            {
                return GetInUse(3);
            }
            set
            {
                SetInUse(3, value);
            }
        }

        public bool InUse4
        {
            get
            {
                return GetInUse(4);
            }
            set
            {
                SetInUse(4, value);
            }
        }

        public bool InUse5
        {
            get
            {
                return GetInUse(5);
            }
            set
            {
                SetInUse(5, value);
            }
        }

        public bool InUse6
        {
            get
            {
                return GetInUse(6);
            }
            set
            {
                SetInUse(6, value);
            }
        }

        [DisplayName("Wejście")]
        public string Label { get; set; }

        public bool Logic { get; set; }

        public int Number { get; set; }

        public int ParentType { get; set; }
        public int RangeMax { get; set; }
        public int RangeMin { get; set; }
        public bool Socket { get; set; }
        public bool Variable { get; set; }

        public TestCaseStepInput Clone(bool copyValues)
        {
            TestCaseStepInput input = new TestCaseStepInput();
            input.Number = Number;
            input.Label = Label;
            input.ParentType = ParentType;
            input.RangeMax = RangeMax;
            input.RangeMin = RangeMin;
            input.Integer = Integer;
            input.Logic = Logic;
            input.Socket = Socket;
            input.ItemList = new TestCaseStepInputItem[ItemList.Length];
            for (int i = 0; i < ItemList.Length; i++)
                input.ItemList[i] = ItemList[i].Clone(copyValues);
            return input;
        }

        public bool GetInUse(int index)
        {
            if (ItemList.Length > index)
                return ItemList[index].InUse;
            else
                return false;
        }

        public void SetInUse(int index, bool value)
        {
            if (ItemList.Length > index)
                ItemList[index].InUse = value;
        }

        public bool Update(List<TestCaseStepInput> refInputList)
        {
            foreach (TestCaseStepInput refInput in refInputList)
                if ((refInput.Number == Number) && (refInput.Socket == Socket) && (refInput.ParentType == ParentType))
                {
                    foreach (TestCaseStepInputItem item in ItemList)
                        foreach (TestCaseStepInputItem refItem in refInput.ItemList)
                            if (refItem.PowerMode == item.PowerMode)
                                item.Update(refItem);
                    return true;
                }

            return false;
        }
    }
}