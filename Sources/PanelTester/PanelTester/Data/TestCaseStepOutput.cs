﻿using System.Collections.Generic;
using System.ComponentModel;

namespace PanelTester.Data
{
    public class TestCaseStepOutput
    {
        public TestCaseStepOutput()
        {
        }

        public TestCaseStepOutput(BasePin basePin)
        {
            InUse = false;
            Number = basePin.Number;
            Value = basePin.GetRangeMax();
            Label = basePin.GetLabel();
            RangeMax = basePin.GetRangeMax();
            RangeMin = basePin.GetRangeMin();
            Integer = basePin.IsInteger();
            Logic = basePin.IsLogic();
            Socket = basePin.IsSocket();
            ParentType = basePin.GetParentType();
        }

        public int Number { get; set; }

        public bool Integer { get; set; }

        [DisplayName("W użyciu")]
        public bool InUse { get; set; }

        [DisplayName("Wyjście")]
        public string Label { get; set; }

        public bool Logic { get; set; }

        public int ParentType { get; set; }

        public int RangeMax { get; set; }

        public int RangeMin { get; set; }

        public bool Socket { get; set; }

        public double Value { get; set; }

        public TestCaseStepOutput Clone(bool copyValues)
        {
            TestCaseStepOutput output = new TestCaseStepOutput();
            output.InUse = copyValues ? InUse : true;
            output.Number = Number;
            output.Value = copyValues ? Value : RangeMax;
            output.Label = Label;
            output.ParentType = ParentType;
            output.RangeMax = RangeMax;
            output.RangeMin = RangeMin;
            output.Integer = Integer;
            output.Logic = Logic;
            output.Socket = Socket;
            return output;
        }

        public bool Update(List<TestCaseStepOutput> refOutputList)
        {
            foreach (TestCaseStepOutput refOutput in refOutputList)
                if ((refOutput.Number == Number) && (refOutput.Socket == Socket) && (refOutput.ParentType == ParentType))
                {
                    InUse = refOutput.InUse;
                    Value = refOutput.Value;
                    return true;
                }

            return false;
        }
    }
}