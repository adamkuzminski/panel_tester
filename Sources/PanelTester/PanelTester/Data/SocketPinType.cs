﻿using PanelTester.Common;

namespace PanelTester.Data
{
    public enum SocketPinType
    {
        [Pin("Zasilanie 230VAC L1")]
        Out230VACL1Power,

        [Pin("230VAC L1")]
        Out230VACL1,

        [Pin("Zasilanie 230VAC L2")]
        Out230VACL2Power,

        [Pin("230VAC L2")]
        Out230VACL2,

        [Pin("Zasilanie 230VAC L3")]
        Out230VACL3Power,

        [Pin("230VAC L3")]
        Out230VACL3,

        [Pin("N")]
        OutN,

        [Pin("PE")]
        OutPE,

        [Pin("230VAC")]
        In230VAC,

        [Pin("N + Ciągłość")]
        InN,

        [Pin("24VDC")]
        Out24VDC,

        [Pin("GND")]
        OutGND,

        [Pin("0-10VDC")]
        Out10VDC,

        [Pin("24VDC")]
        In24VDC,

        [Pin("GND")]
        InGND,

        [Pin("0-10VDC")]
        In10VDC,

        [Pin("PE + Ciągłość")]
        InPE,

        [Pin("Prądowe 4-20mA")]
        InCurrent,

        [Pin("Prądowe GND")]
        InCurrentGND,

        [Pin("Rezystancja")]
        Resistance,

        [Pin("-")]
        NotUsed
    }
}