﻿using System.ComponentModel;

namespace PanelTester.Data
{
    public enum PowerMode
    {
        [Description("L1, L2, L3"), DefaultValue(0b0111)]
        All = 0,

        [Description("L1, L2"), DefaultValue(0b0011)]
        L1_L2,

        [Description("L2, L3"), DefaultValue(0b0110)]
        L2_L3,

        [Description("L1, L3"), DefaultValue(0b0101)]
        L1_L3,

        [Description("L1"), DefaultValue(0b0001)]
        L1,

        [Description("L2"), DefaultValue(0b0010)]
        L2,

        [Description("L3"), DefaultValue(0b0100)]
        L3
    }
}