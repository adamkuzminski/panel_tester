﻿using System.Collections.Generic;
using System.ComponentModel;

namespace PanelTester.Data
{
    public class OrderTestResult : BaseEntity
    {
        public OrderTestResult() : base()
        {
        }

        public OrderTestResult(Order order) : base()
        {
            Id = order.Id;
            OrderNumber = order.OrderNumber;
            PanelTestResultList = new List<PanelTestResult>();
        }

        [DisplayName("Zlecenie")]
        public string OrderNumber { get; set; }

        [Browsable(false)]
        public List<PanelTestResult> PanelTestResultList { get; set; }

        public List<string[]> GetCSVData()
        {
            List<string[]> result = new List<string[]>();
            foreach (PanelTestResult panelTestResult in PanelTestResultList)
            {
                result.Add(panelTestResult.GetCSVData(OrderNumber));
            }
            return result;
        }

        public string[] GetCSVHeader()
        {
            if ((PanelTestResultList == null) || (PanelTestResultList.Count == 0))
                return null;
            else
                return PanelTestResultList[0].GetCSVHeader("Zlecenie");
        }
    }
}