﻿using PanelTester.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PanelTester.Data
{
    public class PanelTestResult
    {
        [DisplayName("Data rozpoczęcia testu")]
        public DateTime StartDate { get; set; }

        [DisplayName("Numer panelu")]
        public int PanelNumber { get; set; }

        [DisplayName("Wynik testu")]
        public bool Result { get; set; }

        [DisplayName("Komunikat")]
        public string Message { get; set; }

        [DisplayName("Błędy")]
        public string Errors { get; set; }

        [DisplayName("Próba")]
        public int Try { get; set; }

        [DisplayName("Identyfikator testera")]
        public string UserId { get; set; }

        [DisplayName("Nazwa testera")]
        public string UserName { get; set; }

        [DisplayName("Data zakończenia testu")]
        public DateTime EndDate { get; set; }

        public List<StepTestResult> StepTestResultList { get; set; }

        public string[] GetCSVData(string extraColumn)
        {
            return new string[]
            {
                extraColumn??"",
                PanelNumber.ToString(),
                Utils.BooleanToInt(Result).ToString(),
                StartDate.ToString("yyyy-MM-dd HH:mm"),
                (EndDate == default(DateTime))? "" : EndDate.ToString("yyyy-MM-dd HH:mm"),
                UserName??"",
                UserId??"",
                Try.ToString(),
                Message??"",
                Errors??""
            };
        }

        public string[] GetCSVHeader(string extraColumn)
        {
            return new string[] {
                extraColumn,
                "Numer panelu",
                "Wynik testu",
                "Data rozpoczęcia testu",
                "Data zakończenia testu",
                "Nazwa testera",
                "Identyfikator testera",
                "Próba",
                "Komunikat",
                "Błędy"
            };
        }
    }
}