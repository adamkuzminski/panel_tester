﻿using System.Collections.Generic;

namespace PanelTester.Data
{
    public class BoardRegistrySet
    {
        public BoardRegistrySet(BoardRegistryType boardRegistryType)
        {
            Type = boardRegistryType;
            RegistryList = new List<BoardRegistry>();
        }

        public int Offset { get; set; }

        public List<BoardRegistry> RegistryList { get; set; }

        public BoardRegistryType Type { get; set; }

        public BoardRegistrySet Copy()
        {
            BoardRegistrySet registrySet = new BoardRegistrySet(Type);
            registrySet.Offset = Offset;
            registrySet.RegistryList = new List<BoardRegistry>();
            foreach (BoardRegistry boardRegistry in RegistryList)
                registrySet.RegistryList.Add(boardRegistry.Copy());
            return registrySet;
        }
    }
}