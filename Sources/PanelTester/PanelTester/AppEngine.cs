﻿#if DEBUG
#define SKIP_LOGIN
#endif

using PanelTester.Data;
using PanelTester.UI;
using System.IO;
using System.Windows.Forms;

namespace PanelTester
{
    public class AppEngine
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static AppEngine instance;
        private User user; //Logged user

        public static AppEngine GetInstance()
        {
            if (instance == null)
                instance = new AppEngine();
            return instance;
        }

        public void CheckLogin()
        {
            if (!IsLogged())
            {
                Login();

                if (!IsLogged())
                    Application.Exit();
            }
        }

        public Database Database { get; private set; }

        public User User
        {
            get
            {
                return user;
            }
            set
            {
                log.Info("User: " + ((value == null) ? "Null" : value.Id));
                user = value;
                foreach (Form form in Application.OpenForms)
                {
                    if (form is BaseForm)
                        (form as BaseForm).OnUserChanged(user);
                }
            }
        }

        public bool Init()
        {
            string databasePath = Properties.Settings.Default.DatabasePath.Trim();
            if (databasePath.Length == 0)
            {
                databasePath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Data");
                Properties.Settings.Default.DatabasePath = databasePath;
                Properties.Settings.Default.Save();
            }

            log.Info("Init " + databasePath);

            Database = new Database();
            Database.Load(databasePath);

#if SKIP_LOGIN
            if (Database.UserList.Count > 0)
                User = Database.UserList[0];
#endif

            return true;
        }

        public bool IsLogged()
        {
#if SKIP_LOGIN
            return true;
#else
            return ((user != null));// && !Utils.IsTimeoutExceeded(user.LastActivityTime, Properties.Settings.Default.UserActivityTimeout));
#endif
        }

        public bool Login()
        {
            LoginDialog loginDialog = new LoginDialog();
            return (loginDialog.ShowDialog() == DialogResult.OK);
        }
    }
}