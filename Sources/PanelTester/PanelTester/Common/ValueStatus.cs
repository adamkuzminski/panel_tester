﻿namespace PanelTester.Data
{
    public enum ValueStatus
    {
        Unknown = 0,
        Ready,
        NotReady
    }
}