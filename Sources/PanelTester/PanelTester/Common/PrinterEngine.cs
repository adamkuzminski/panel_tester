﻿using com.citizen.sdk.LabelPrint;
using System;
using System.Windows.Forms;

namespace PanelTester.Common
{
    public class PrinterEngine : IDisposable
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private LabelPrinter printer;

        public PrinterEngine()
        {
            printer = new LabelPrinter();
            printer.SetMeasurementUnit(LabelConst.CLS_UNIT_MILLI);
        }

        public bool Connect(bool showMessage = true)
        {
            try
            {
                int ret = printer.Connect(LabelConst.CLS_PORT_USB, Properties.Settings.Default.PrinterDeviceName);
                if (ret != LabelConst.CLS_SUCCESS)
                {
                    if (showMessage)
                        MessageBox.Show("Błąd połączenia z drukarką: " + ret.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    log.ErrorFormat("PrintLabel. Connect error {0}. Device name: {1}", ret, Properties.Settings.Default.PrinterDeviceName);
                    return false;
                }

                // PrinterCheck
                string errorMessage = "";
                if (LabelConst.CLS_SUCCESS == printer.PrinterCheck())
                {
                    do
                    {
                        // CommandInterpreterInAction Property
                        if (printer.GetCommandInterpreterInAction() == 1)
                        {
                            // Command interpreter in action
                            errorMessage = "Command interpreter in action";
                            break;
                        }
                        // PaperError Property
                        if (printer.GetPaperError() == 1)
                        {
                            // Paper error
                            errorMessage = "Błąd papieru";
                            break;
                        }
                        // RibbonEnd Property
                        if (printer.GetRibbonEnd() == 1)
                        {
                            // Ribbon end
                            errorMessage = "Koniec papieru";
                            break;
                        }
                        // BatchProcessing Property
                        if (printer.GetBatchProcessing() == 1)
                        {
                            // Batch processing
                            errorMessage = "Batch processing";
                            break;
                        }
                        // Printing Property
                        if (printer.GetPrinting() == 1)
                        {
                            // Printing
                            errorMessage = "W trakcie drukowania";
                            break;
                        }
                        // Pause Property
                        if (printer.GetPause() == 1)
                        {
                            // Pause
                            errorMessage = "Drukowanie wstrzymane (Pause)";
                            break;
                        }
                        // WaitingForPeeling Property
                        if (printer.GetWaitingForPeeling() == 1)
                        {
                            // Waitiong for peeling
                            errorMessage = "Waiting for peeling";
                            break;
                        }
                    }
                    while (false);
                }
                else
                {
                    // Fail
                    errorMessage = "Nie można odczytać statusu drukarki";
                }
                if (errorMessage.Length > 0)
                {
                    log.Error("Connect: " + errorMessage);
                    if (showMessage)
                        MessageBox.Show("Wystąpił błąd drukarki: " + errorMessage + "\n\nSprawdź drukarkę i spróbuj ponownie", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
            }
            catch (Exception e)
            {
                log.Error("Connect", e);
                return false;
            }

            return true;
        }

        public void Disconnect()
        {
            printer.Disconnect();
        }

        public void Dispose()
        {
            if (printer != null)
                printer.Disconnect();
        }

        public bool PrintLabel(string articleNumber, string revision, string productName, string employeeId)
        {
            bool result = false;
            try
            {
                result = Connect();
                if (!result)
                    return false;

                printer.SetPrintDarkness(Properties.Settings.Default.PrinterDarkness);
                printer.SetMeasurementUnit(LabelConst.CLS_UNIT_MILLI);

                LabelDesign design = new LabelDesign();

                string supplier = "205077";
                //string magicText = "511109";

                int x = 10;
                int y = 250;
                int vertMargin = 40;
                design.DrawTextPtrFont(
                    string.Format("Art no: {0}  Rev: {1}", articleNumber, revision.PadLeft(2, '0').Substring(0, 2)),
                    LabelConst.CLS_LOCALE_OTHER,
                    LabelConst.CLS_PRT_FNT_TRIUMVIRATE,
                    LabelConst.CLS_RT_NORMAL,
                    1,
                    1,
                    LabelConst.CLS_PRT_FNT_SIZE_8,
                    x,
                    y);

                y -= vertMargin;
                design.DrawTextPtrFont(
                    string.Format("Type: {0}", (productName.Length > 30) ? productName.Substring(0, 30) : productName),
                    LabelConst.CLS_LOCALE_OTHER,
                    LabelConst.CLS_PRT_FNT_TRIUMVIRATE,
                    LabelConst.CLS_RT_NORMAL,
                    1,
                    1,
                    LabelConst.CLS_PRT_FNT_SIZE_8,
                    x,
                    y);
                y -= vertMargin;
                design.DrawTextPtrFont(
                    string.Format("Supplier No: {0}", supplier),
                    LabelConst.CLS_LOCALE_OTHER,
                    LabelConst.CLS_PRT_FNT_TRIUMVIRATE,
                    LabelConst.CLS_RT_NORMAL,
                    1,
                    1,
                    LabelConst.CLS_PRT_FNT_SIZE_8,
                    x,
                    y);
                y -= vertMargin;

                x = 500;
                y = 260;
                vertMargin = 35;
                design.DrawTextPtrFont(
                    "Final inspection",
                    LabelConst.CLS_LOCALE_OTHER,
                    LabelConst.CLS_PRT_FNT_TRIUMVIRATE,
                    LabelConst.CLS_RT_NORMAL,
                    1,
                    1,
                    LabelConst.CLS_PRT_FNT_SIZE_8,
                    x,
                    y);
                y -= vertMargin;
                design.DrawTextPtrFont(
                    string.Format("Date: {0}", DateTime.Now.ToString("yyyy-MM-dd")),
                    LabelConst.CLS_LOCALE_OTHER,
                    LabelConst.CLS_PRT_FNT_TRIUMVIRATE,
                    LabelConst.CLS_RT_NORMAL,
                    1,
                    1,
                    LabelConst.CLS_PRT_FNT_SIZE_8,
                    x,
                    y);
                y -= vertMargin;
                design.DrawTextPtrFont(
                    "Checked by",
                    LabelConst.CLS_LOCALE_OTHER,
                    LabelConst.CLS_PRT_FNT_TRIUMVIRATE,
                    LabelConst.CLS_RT_NORMAL,
                    1,
                    1,
                    LabelConst.CLS_PRT_FNT_SIZE_8,
                    x,
                    y);
                y -= vertMargin;
                design.DrawTextPtrFont(
                    string.Format("Employee no: {0}", employeeId),
                    LabelConst.CLS_LOCALE_OTHER,
                    LabelConst.CLS_PRT_FNT_TRIUMVIRATE,
                    LabelConst.CLS_RT_NORMAL,
                    1,
                    1,
                    LabelConst.CLS_PRT_FNT_SIZE_8,
                    x,
                    y);
                y -= vertMargin;

                design.DrawLine(5, 150, 795, 150, 5);

                design.DrawBarCode(
                    string.Format("{0}{1}{2}", articleNumber.PadLeft(6, '0').Substring(0, 6), revision.PadLeft(2, '0').Substring(0, 2), supplier),//.PadLeft(4, '0').Substring(0, 4)),
                    LabelConst.CLS_BCS_CODE39,
                    LabelConst.CLS_RT_NORMAL,
                    Properties.Settings.Default.BarcodeThickBarWidth,
                    Properties.Settings.Default.BarcodeNarrowBarWidth,
                    100,
                    Properties.Settings.Default.BarcodeHorzOffset,
                    10,
                    LabelConst.CLS_BCS_TEXT_SHOW);

                /*design.DrawTextPtrFont(
                    magicText,
                    LabelConst.CLS_LOCALE_OTHER,
                    LabelConst.CLS_PRT_FNT_TRIUMVIRATE,
                    LabelConst.CLS_RT_NORMAL,
                    1,
                    1,
                    LabelConst.CLS_PRT_FNT_SIZE_6,
                    700,
                    10);
                */
                printer.Print(design, 1);
            }
            catch (Exception e)
            {
                log.Error("PrintLabel", e);
            }

            Disconnect();

            return result;
        }

        public CitizenPrinterInfo[] SearchPrinter(out int result)
        {
            return printer.SearchCitizenPrinter(LabelConst.CLS_PORT_USB, 10, out result);
        }
    }
}