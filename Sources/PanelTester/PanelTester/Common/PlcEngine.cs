﻿using OpcUaHelper;
using System;

namespace PanelTester.Common
{
    public class PlcEngine : IDisposable
    {
        public static string GetResistanceValue1 = "Wtyczka_1_OK";
        public static string GetResistanceValue2 = "Wtyczka_2_OK";
        public static string GetResistanceValue3 = "Wtyczka_3_OK";
        public static string GetResistanceValue4 = "Wtyczka_4_OK";
        public static string GetStartValue = "Test_Zal";
        public static string GetStopValue = "Stop";
        public static string GetSafetyValue = "Obw_Bezp_NieOK";
        public static string SetResistanceValue1 = "Wartosc_Wtyk_1";
        public static string SetResistanceValue2 = "Wartosc_Wtyk_2";
        public static string SetResistanceValue3 = "Wartosc_Wtyk_3";
        public static string SetResistanceValue4 = "Wartosc_Wtyk_4";
        public static string SetTempValue1 = "Ustaw_Temp_Grzalka_1";
        public static string SetTempValue2 = "Ustaw_Temp_Grzalka_2";
        public static string SetTempValue3 = "Ustaw_Temp_Grzalka_3";
        public static string SetTempValue4 = "Ustaw_Temp_Grzalka_4";
        public static string SetTempValue5 = "Ustaw_Temp_Grzalka_5";
        public static string SetTempValue6 = "Ustaw_Temp_Grzalka_6";

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PlcEngine()
        {
            OpcClient = new OpcUaClient();
        }

        public OpcUaClient OpcClient { get; set; }

        public static string GetNodeId(string varName)
        {
            return String.Format("{0}.{1}", Properties.Settings.Default.PLCNodeId, varName);
        }

        public bool Connect()
        {
            try
            {
                if (OpcClient.Connected)
                    OpcClient.Disconnect();
                OpcClient.ConnectServer(Properties.Settings.Default.PLCAddress);
                return OpcClient.Connected;
            }
            catch (Exception e)
            {
                log.Error("Connect", e);
                return false;
            }
        }

        public void Dispose()
        {
            if (OpcClient.Connected)
                OpcClient.Disconnect();
        }

        public double ReadAnalogDouble(string varName, double range)
        {
            return IntToDoubleValue(ReadAnalogInt(varName), range);
        }

        public UInt16 ReadAnalogInt(string varName)
        {
            try
            {
                UInt16 value = OpcClient.ReadNode<UInt16>(GetNodeId(varName));
                log.DebugFormat("ReadAnalog: {0} -> {1}", varName, value);
                return value;
            }
            catch (Exception e)
            {
                log.Error("ReadAnalog: " + varName, e);
                return UInt16.MaxValue;
            }
        }

        public bool ReadDigital(string varName)
        {
            try
            {
                bool value = OpcClient.ReadNode<bool>(GetNodeId(varName));
                log.DebugFormat("ReadDigital: {0} -> {1}", varName, value);
                return value;
            }
            catch (Exception e)
            {
                log.Error("ReadDigital: " + varName, e);
                return false;
            }
        }

        public bool WriteAnalogDouble(string varName, double value, double range)
        {
            return WriteAnalogInt(varName, DoubleToIntValue(value, range));
        }

        public bool WriteAnalogInt(string varName, UInt16 value)
        {
            try
            {
                log.DebugFormat("WriteAnalog: {0} -> {1}", varName, value);
                return OpcClient.WriteNode(GetNodeId(varName), value);
            }
            catch (Exception e)
            {
                log.Error("WriteAnalog: " + varName, e);
                return false;
            }
        }

        public bool WriteDigital(string varName, bool value)
        {
            try
            {
                log.DebugFormat("WriteDigital: {0} -> {1}", varName, value);
                return OpcClient.WriteNode(GetNodeId(varName), value);
            }
            catch (Exception e)
            {
                log.Error("WriteDigital: " + varName, e);
                return false;
            }
        }

        private UInt16 DoubleToIntValue(double value, double range)
        {
            return (UInt16)Math.Min(UInt16.MaxValue, Math.Round(value * UInt16.MaxValue / range));
        }

        private double IntToDoubleValue(UInt16 value, double range)
        {
            return value * range / UInt16.MaxValue;
        }
    }
}