﻿using Modbus.Device;
using PanelTester.Data;
using System;
using System.Net.Sockets;

namespace PanelTester.Common
{
    public class ModbusEngine : IDisposable
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string boardAddressIP;
        private BoardRegistrySet[] boardRegistrySetList;
        private byte boardUnitID;
        private ModbusIpMaster modbus;
        private TcpClient tcpClient;

        private ushort[] analogIn;
        private ushort[] analogOut;
        private bool[] digitalIn;
        private bool[] digitalOut;

        public ModbusEngine(Board board)
        {
            boardRegistrySetList = board.RegistrySetList;
            boardUnitID = board.UnitID;
            boardAddressIP = board.AddressIP;
        }

        public ModbusEngine(TestCase testCase)
        {
            boardRegistrySetList = testCase.RegistrySetList;
            Board board = testCase.GetBoard();
            boardUnitID = board.UnitID;
            boardAddressIP = board.AddressIP;
        }

        public string LastErrorMessage
        {
            get; internal set;
        }

        public bool Connect()
        {
            try
            {
                tcpClient = new TcpClient(boardAddressIP, 502);
                if (!tcpClient.Connected)
                {
                    LastErrorMessage = "Cannot connect to IP address";
                    return false;
                }

                modbus = ModbusIpMaster.CreateIp(tcpClient);

                ResetOutputCache();
                
                return true;
            }
            catch (Exception e)
            {
                LastErrorMessage = e.Message;
                log.Error("ModbusEngine.Connect", e);
                return false;
            }
        }

        public void Dispose()
        {
            if (modbus != null)
                modbus.Dispose();
        }

        public void ResetOutputCache()
        {
            analogOut = new ushort[boardRegistrySetList[(int)BoardRegistryType.AOut].RegistryList.Count];
            digitalOut = new bool[boardRegistrySetList[(int)BoardRegistryType.DOut].RegistryList.Count];
        }

        public bool InitBoard(ushort value)
        {
            try
            {
                log.DebugFormat("InitBoard: {0}", value);
                modbus.WriteSingleRegister(boardUnitID, 0, value);
                return true;
            }
            catch (Exception e)
            {
                log.Error(String.Format("InitBoard: {0}", value), e);
                return false;
            }
        }

        public bool Read()
        {
            try
            {
                log.Debug("ReadInputs");
                analogIn = modbus.ReadInputRegisters(boardUnitID, (ushort)boardRegistrySetList[(int)BoardRegistryType.AIn].Offset, (ushort)boardRegistrySetList[(int)BoardRegistryType.AIn].RegistryList.Count);
                digitalIn = modbus.ReadInputs(boardUnitID, (ushort)boardRegistrySetList[(int)BoardRegistryType.DIn].Offset, (ushort)boardRegistrySetList[(int)BoardRegistryType.DIn].RegistryList.Count);
                //bool[] coils = modbus.ReadCoils(boardUnitID, (ushort)boardRegistrySetList[(int)BoardRegistryType.DOut].Offset, (ushort)boardRegistrySetList[(int)BoardRegistryType.DOut].RegistryList.Count);
                return true;
            }
            catch (Exception e)
            {
                log.Error("ReadInputs", e);
                return false;
            }
        }

        public bool Write()
        {
            try
            {
                log.Debug("WriteOutputs");
                modbus.WriteMultipleRegisters(boardUnitID, (ushort)boardRegistrySetList[(int)BoardRegistryType.AOut].Offset, analogOut);
                modbus.WriteMultipleCoils(boardUnitID, (ushort)boardRegistrySetList[(int)BoardRegistryType.DOut].Offset, digitalOut);
                return true;
            }
            catch (Exception e)
            {
                log.Error("WriteOutputs", e);
                return false;
            }
        }

        public UInt16 ReadAnalog(int registryIndex, BoardRegistryType registryType)
        {
            try
            {
                UInt16 value;
                BoardRegistrySet registrySet = boardRegistrySetList[(int)registryType];
                if (registryType == BoardRegistryType.AIn)
                    value = /*analogIn[registryIndex];// */modbus.ReadInputRegisters(boardUnitID, (ushort)(registrySet.Offset + registryIndex), 1)[0];
                else
                    value = modbus.ReadHoldingRegisters(boardUnitID, (ushort)(registrySet.Offset + registryIndex), 1)[0];
                log.DebugFormat("ReadAnalog: {0}, {1} -> {2}", registryIndex, registryType, value);
                return value;
            }
            catch (Exception e)
            {
                log.Error(String.Format("ReadAnalog: {0}, {1}", registryIndex, registryType), e);
                return UInt16.MaxValue;
            }
        }

        public bool ReadDigital(int registryIndex, BoardRegistryType registryType)
        {
            try
            {
                bool value;
                BoardRegistrySet registrySet = boardRegistrySetList[(int)registryType];
                if (registryType == BoardRegistryType.DIn)
                    value = /*digitalIn[registryIndex];// */modbus.ReadInputs(boardUnitID, (ushort)(registrySet.Offset + registryIndex), 1)[0];
                else
                    value = modbus.ReadCoils(boardUnitID, (ushort)(registrySet.Offset + registryIndex), 1)[0];
                log.DebugFormat("ReadDigital: {0}, {1} -> {2}", registryIndex, registryType, value);
                return value;
            }
            catch (Exception e)
            {
                log.Error(String.Format("ReadDigital: {0}, {1}", registryIndex, registryType), e);
                return false;
            }
        }

        public bool ResetAnalog(int count, int offset)
        {
            try
            {
                log.DebugFormat("ResetAnalog: {0}, {1}", count, offset);
                ushort[] data = new ushort[count];
                modbus.WriteMultipleRegisters(boardUnitID, (ushort)offset, data);
                return true;
            }
            catch (Exception e)
            {
                log.Error("ResetAnalog", e);
                return false;
            }
        }

        public bool ResetDigital(int count, int offset)
        {
            try
            {
                log.DebugFormat("ResetDigital: {0}, {1}", count, offset);
                bool[] data = new bool[count];
                modbus.WriteMultipleCoils(boardUnitID, (ushort)offset, data);
                return true;
            }
            catch (Exception e)
            {
                log.Error("ResetDigital", e);
                return false;
            }
        }

        public void WriteAnalog(int registerIndex, UInt16 value)
        {
            try
            {
                log.DebugFormat("WriteAnalog: {0} -> {1}", registerIndex, value);
                modbus.WriteSingleRegister(boardUnitID, (ushort)(boardRegistrySetList[(int)BoardRegistryType.AOut].Offset + registerIndex), value);
                analogOut[registerIndex] = value;
            }
            catch (Exception e)
            {
                log.Error("WriteAnalog: " + registerIndex, e);
            }
        }

        public void WriteDigital(int registerIndex, bool value)
        {
            try
            {
                log.DebugFormat("WriteDigital: {0} -> {1}", registerIndex, value);
                modbus.WriteSingleCoil(boardUnitID, (ushort)(boardRegistrySetList[(int)BoardRegistryType.DOut].Offset + registerIndex), value);
                digitalOut[registerIndex] = value;
            }
            catch (Exception e)
            {
                log.Error("WriteDigital: " + registerIndex, e);
            }
        }
    }
}