﻿using System;

namespace PanelTester.Common
{
    [AttributeUsage(AttributeTargets.All)]
    public class PinAttribute : Attribute
    {
        private string label;

        public PinAttribute(string label)
        {
            this.label = label;
        }

        public virtual string Label
        {
            get { return label; }
        }
    }
}