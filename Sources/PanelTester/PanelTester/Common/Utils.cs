﻿using System;
using System.Linq;

namespace PanelTester.Common
{
    public static class Utils
    {
        public static object BooleanToInt(bool value)
        {
            return value ? 1 : 0;
        }

        public static string GetString(string value)
        {
            if (value == null)
                return "";
            else
                return value.Trim();
        }

        public static bool In<T>(this T item, params T[] list)
        {
            return list.Contains(item);
        }

        public static bool IsTimeoutExceeded(long ticks, long timeout)
        {
            return (DateTime.Now.Ticks - ticks >= timeout);
        }

        public static void ShowKeyboard()
        {
            try
            {
                //if (Properties.Settings.Default.ShowVirtualKeyboard)
                //    System.Diagnostics.Process.Start("osk.exe");
            }
            catch
            {
            }
        }

        public static bool StringToBoolean(string value)
        {
            if (value == null)
                return false;

            bool boolResult;
            if (Boolean.TryParse(value, out boolResult))
                return boolResult;

            int intResult;
            if (Int32.TryParse(value, out intResult) && (intResult > 0))
                return true;

            return false;
        }
    }
}