﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanelTester.Common
{
    public class Consts
    {
        public static Color NotTestedColor = Color.White;
        public static Color TestingColor = Color.DeepSkyBlue;
        public static Color ErrorColor = Color.Red;
        public static Color PositiveColor = Color.FromArgb(0x99, 0xff, 0x33);
        public static Color WarningColor = Color.LightSalmon;
        public static Color InfoColor = Color.LightYellow;

        public static Color BackReadOnlyColor = Color.FromArgb(0xf8, 0xf8, 0xf8);

        public static Color AlternativeRowColor = Color.AliceBlue;
        public static Color DisabledRowColor = Color.LightGray;
        public static Color NotUsedRowColor = Color.WhiteSmoke;
        public static Color StandardRowColor = Color.White;

        public static Color AdminBackColor = Color.LightGray;
        public static Color SuperadminBackColor = Color.White;
        public static Color EngineerBackColor = Color.Gold;
        public static Color OperatorBackColor = Color.DeepSkyBlue;
    }
}
