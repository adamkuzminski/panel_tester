﻿using System;

namespace PanelTester.UI
{
    public partial class BaseRegistryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        protected System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            this.dataGridView = new PanelTester.UI.InheritedDataGridView();
            this.toolStrip = new PanelTester.UI.InheritedToolStrip();
            this.backToolStripButton = new PanelTester.UI.InheritedToolStripButton();
            this.addToolStripButton = new PanelTester.UI.InheritedToolStripButton();
            this.copyToolStripButton = new PanelTester.UI.InheritedToolStripButton();
            this.editToolStripButton = new PanelTester.UI.InheritedToolStripButton();
            this.previewToolStripButton = new PanelTester.UI.InheritedToolStripButton();
            this.deleteToolStripButton = new PanelTester.UI.InheritedToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 58);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(1176, 830);
            this.dataGridView.TabIndex = 0;
            this.dataGridView.SelectionChanged += new System.EventHandler(this.dataGridView_SelectionChanged);
            this.dataGridView.DoubleClick += new System.EventHandler(this.dataGridView_DoubleClick);
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(36, 36);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backToolStripButton,
            this.addToolStripButton,
            this.copyToolStripButton,
            this.editToolStripButton,
            this.previewToolStripButton,
            this.deleteToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip.Size = new System.Drawing.Size(1176, 58);
            this.toolStrip.TabIndex = 1;
            // 
            // backToolStripButton
            // 
            this.backToolStripButton.AutoSize = false;
            this.backToolStripButton.Image = global::PanelTester.Properties.Resources.Back;
            this.backToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.backToolStripButton.Name = "backToolStripButton";
            this.backToolStripButton.Size = new System.Drawing.Size(60, 55);
            this.backToolStripButton.Text = "Wróć";
            this.backToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.backToolStripButton.Click += new System.EventHandler(this.backToolStripButton_Click);
            // 
            // addToolStripButton
            // 
            this.addToolStripButton.AutoSize = false;
            this.addToolStripButton.Image = global::PanelTester.Properties.Resources.Add;
            this.addToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addToolStripButton.Name = "addToolStripButton";
            this.addToolStripButton.Size = new System.Drawing.Size(60, 55);
            this.addToolStripButton.Text = "Dodaj";
            this.addToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.addToolStripButton.Click += new System.EventHandler(this.addToolStripButton_Click);
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.AutoSize = false;
            this.copyToolStripButton.Image = global::PanelTester.Properties.Resources.Copy;
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(60, 55);
            this.copyToolStripButton.Text = "Kopiuj";
            this.copyToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.copyToolStripButton.Click += new System.EventHandler(this.copyToolStripButton_Click);
            // 
            // editToolStripButton
            // 
            this.editToolStripButton.AutoSize = false;
            this.editToolStripButton.Image = global::PanelTester.Properties.Resources.Edit;
            this.editToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editToolStripButton.Name = "editToolStripButton";
            this.editToolStripButton.Size = new System.Drawing.Size(60, 55);
            this.editToolStripButton.Text = "Edytuj";
            this.editToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.editToolStripButton.Click += new System.EventHandler(this.editToolStripButton_Click);
            // 
            // previewToolStripButton
            // 
            this.previewToolStripButton.AutoSize = false;
            this.previewToolStripButton.Image = global::PanelTester.Properties.Resources.Preview;
            this.previewToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.previewToolStripButton.Name = "previewToolStripButton";
            this.previewToolStripButton.Size = new System.Drawing.Size(60, 55);
            this.previewToolStripButton.Text = "Podgląd";
            this.previewToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.previewToolStripButton.Click += new System.EventHandler(this.previewToolStripButton_Click);
            // 
            // deleteToolStripButton
            // 
            this.deleteToolStripButton.AutoSize = false;
            this.deleteToolStripButton.Image = global::PanelTester.Properties.Resources.Delete;
            this.deleteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteToolStripButton.Name = "deleteToolStripButton";
            this.deleteToolStripButton.Size = new System.Drawing.Size(60, 55);
            this.deleteToolStripButton.Text = "Usuń";
            this.deleteToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.deleteToolStripButton.Click += new System.EventHandler(this.deleteToolStripButton_Click);
            // 
            // BaseRegistryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1176, 888);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.toolStrip);
            this.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.Name = "BaseRegistryForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.BaseRegistryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.toolStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        
        #endregion

        protected InheritedDataGridView dataGridView;
        protected InheritedToolStripButton copyToolStripButton;
        protected InheritedToolStripButton editToolStripButton;
        protected InheritedToolStripButton deleteToolStripButton;
        protected InheritedToolStripButton previewToolStripButton;
        protected InheritedToolStrip toolStrip;
        protected InheritedToolStripButton addToolStripButton;
        protected InheritedToolStripButton backToolStripButton;
    }
}