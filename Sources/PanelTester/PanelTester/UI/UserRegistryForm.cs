﻿using PanelTester.Common;
using PanelTester.Data;
using System;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class UserRegistryForm : BaseRegistryForm
    {
        public UserRegistryForm()
        {
            InitializeComponent();
        }

        protected override void AddRecord()
        {
            AddUser(new User());
        }

        protected override void DeleteRecord()
        {
            if (dataGridView.SelectedRows.Count == 1)
            {
                User user = dataGridView.SelectedRows[0].DataBoundItem as User;
                if (AppEngine.GetInstance().User.Id == user.Id)
                {
                    MessageBox.Show("Nie można usunąć zalogowanego użytkownika", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                Database database = Database.GetInstance();
                database.UserList.Remove(user);
                database.SaveData(database.UserList);
                RefreshGrid();
            }
        }

        protected override void EditRecord()
        {
            if (dataGridView.SelectedRows.Count == 1)
            {
                User user = dataGridView.SelectedRows[0].DataBoundItem as User;
                UserDialog dlg = new UserDialog(UserDialog.UserFormMode.Edit, user);
                Database database = Database.GetInstance();
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    database.SaveData(database.UserList);
                }
                else
                {
                    database.LoadData(ref database.UserList);
                    RefreshGrid();
                }
            }
        }

        protected override bool IsCopyAllowed()
        {
            return false;
        }

        protected override void PreviewRecord()
        {
            if (dataGridView.SelectedRows.Count == 1)
            {
                UserDialog dlg = new UserDialog(UserDialog.UserFormMode.Profile, dataGridView.SelectedRows[0].DataBoundItem as User);
                dlg.ShowDialog();
            }
        }

        protected override void RefreshGrid()
        {
            RefreshGrid(new BindingSource(new SortableBindingList<User>(AppEngine.GetInstance().Database.UserList), null));
        }

        private bool AddUser(User user)
        {
            UserDialog dlg = new UserDialog(UserDialog.UserFormMode.New, user);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Database database = Database.GetInstance();
                database.UserList.Add(user);
                database.SaveData(database.UserList);
                RefreshGrid();
                return true;
            }
            return false;
        }

        private void UserRegistryForm_Load(object sender, EventArgs e)
        {
        }
    }
}