﻿namespace PanelTester.UI
{
    partial class BoardEditorDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label typeLabel;
            System.Windows.Forms.Label unitIDLabel;
            System.Windows.Forms.Label firwareTimeoutLabel;
            System.Windows.Forms.Label label5;
            this.offsetNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.countNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ipTextBox = new System.Windows.Forms.TextBox();
            this.boardBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.createDateTextBox = new System.Windows.Forms.TextBox();
            this.authorTextBox = new System.Windows.Forms.TextBox();
            this.commentTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.activeCheckBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.symbolDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.captionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testInputColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testResultColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boardRegistryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.testButton = new System.Windows.Forms.Button();
            this.boardTypeComboBox = new System.Windows.Forms.ComboBox();
            this.unitIDNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.firwareTimeoutNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.registryValueNumericUpDown = new System.Windows.Forms.NumericUpDown();
            typeLabel = new System.Windows.Forms.Label();
            unitIDLabel = new System.Windows.Forms.Label();
            firwareTimeoutLabel = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            this.bottomPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.offsetNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardRegistryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitIDNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firwareTimeoutNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registryValueNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(0, 516);
            this.bottomPanel.Size = new System.Drawing.Size(992, 59);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(868, 4);
            this.cancelButton.Size = new System.Drawing.Size(120, 51);
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(748, 4);
            this.okButton.Size = new System.Drawing.Size(120, 51);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(label5);
            this.mainPanel.Controls.Add(this.registryValueNumericUpDown);
            this.mainPanel.Controls.Add(firwareTimeoutLabel);
            this.mainPanel.Controls.Add(this.firwareTimeoutNumericUpDown);
            this.mainPanel.Controls.Add(unitIDLabel);
            this.mainPanel.Controls.Add(this.unitIDNumericUpDown);
            this.mainPanel.Controls.Add(typeLabel);
            this.mainPanel.Controls.Add(this.boardTypeComboBox);
            this.mainPanel.Controls.Add(this.testButton);
            this.mainPanel.Controls.Add(this.dataGridView);
            this.mainPanel.Controls.Add(this.offsetNumericUpDown);
            this.mainPanel.Controls.Add(this.countNumericUpDown);
            this.mainPanel.Controls.Add(this.typeComboBox);
            this.mainPanel.Controls.Add(this.label3);
            this.mainPanel.Controls.Add(this.label2);
            this.mainPanel.Controls.Add(this.label1);
            this.mainPanel.Controls.Add(this.ipTextBox);
            this.mainPanel.Controls.Add(this.createDateTextBox);
            this.mainPanel.Controls.Add(this.authorTextBox);
            this.mainPanel.Controls.Add(this.commentTextBox);
            this.mainPanel.Controls.Add(this.nameTextBox);
            this.mainPanel.Controls.Add(this.label11);
            this.mainPanel.Controls.Add(this.label13);
            this.mainPanel.Controls.Add(this.label10);
            this.mainPanel.Controls.Add(this.activeCheckBox);
            this.mainPanel.Controls.Add(this.label4);
            this.mainPanel.Size = new System.Drawing.Size(992, 516);
            // 
            // typeLabel
            // 
            typeLabel.AutoSize = true;
            typeLabel.Location = new System.Drawing.Point(156, 111);
            typeLabel.Name = "typeLabel";
            typeLabel.Size = new System.Drawing.Size(70, 20);
            typeLabel.TabIndex = 62;
            typeLabel.Text = "Typ płyty";
            // 
            // unitIDLabel
            // 
            unitIDLabel.AutoSize = true;
            unitIDLabel.Location = new System.Drawing.Point(167, 79);
            unitIDLabel.Name = "unitIDLabel";
            unitIDLabel.Size = new System.Drawing.Size(59, 20);
            unitIDLabel.TabIndex = 63;
            unitIDLabel.Text = "Unit ID";
            // 
            // firwareTimeoutLabel
            // 
            firwareTimeoutLabel.AutoSize = true;
            firwareTimeoutLabel.Location = new System.Drawing.Point(32, 176);
            firwareTimeoutLabel.Name = "firwareTimeoutLabel";
            firwareTimeoutLabel.Size = new System.Drawing.Size(194, 20);
            firwareTimeoutLabel.TabIndex = 64;
            firwareTimeoutLabel.Text = "Czas inicjalizacji płyty [sek]";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(88, 144);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(138, 20);
            label5.TabIndex = 66;
            label5.Text = "Wartość rejestru 0";
            // 
            // offsetNumericUpDown
            // 
            this.offsetNumericUpDown.Location = new System.Drawing.Point(634, 76);
            this.offsetNumericUpDown.Name = "offsetNumericUpDown";
            this.offsetNumericUpDown.Size = new System.Drawing.Size(120, 26);
            this.offsetNumericUpDown.TabIndex = 11;
            this.offsetNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.offsetNumericUpDown.ValueChanged += new System.EventHandler(this.offsetNumericUpDown_ValueChanged);
            // 
            // countNumericUpDown
            // 
            this.countNumericUpDown.Location = new System.Drawing.Point(634, 44);
            this.countNumericUpDown.Name = "countNumericUpDown";
            this.countNumericUpDown.Size = new System.Drawing.Size(120, 26);
            this.countNumericUpDown.TabIndex = 10;
            this.countNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.countNumericUpDown.ValueChanged += new System.EventHandler(this.countNumericUpDown_ValueChanged);
            // 
            // typeComboBox
            // 
            this.typeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Items.AddRange(new object[] {
            "AOut - wyjścia analogowe",
            "AIn - wejścia analogowe",
            "DOut - wyjścia cyfrowe",
            "DIn - wejścia cyfrowe"});
            this.typeComboBox.Location = new System.Drawing.Point(464, 12);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(290, 28);
            this.typeComboBox.TabIndex = 9;
            this.typeComboBox.SelectedIndexChanged += new System.EventHandler(this.typeComboBox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(554, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 26);
            this.label3.TabIndex = 61;
            this.label3.Text = "Offset";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(554, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 26);
            this.label2.TabIndex = 60;
            this.label2.Text = "Ilość";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(10, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 26);
            this.label1.TabIndex = 59;
            this.label1.Text = "Adres IP";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ipTextBox
            // 
            this.ipTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.boardBindingSource, "AddressIP", true));
            this.ipTextBox.Location = new System.Drawing.Point(232, 44);
            this.ipTextBox.MaxLength = 15;
            this.ipTextBox.Name = "ipTextBox";
            this.ipTextBox.Size = new System.Drawing.Size(200, 26);
            this.ipTextBox.TabIndex = 1;
            // 
            // boardBindingSource
            // 
            this.boardBindingSource.DataSource = typeof(PanelTester.Data.Board);
            // 
            // createDateTextBox
            // 
            this.createDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.boardBindingSource, "CreateTime", true));
            this.createDateTextBox.Location = new System.Drawing.Point(232, 322);
            this.createDateTextBox.Name = "createDateTextBox";
            this.createDateTextBox.ReadOnly = true;
            this.createDateTextBox.Size = new System.Drawing.Size(200, 26);
            this.createDateTextBox.TabIndex = 7;
            this.createDateTextBox.TabStop = false;
            // 
            // authorTextBox
            // 
            this.authorTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.boardBindingSource, "Author", true));
            this.authorTextBox.Location = new System.Drawing.Point(232, 290);
            this.authorTextBox.Name = "authorTextBox";
            this.authorTextBox.ReadOnly = true;
            this.authorTextBox.Size = new System.Drawing.Size(200, 26);
            this.authorTextBox.TabIndex = 6;
            this.authorTextBox.TabStop = false;
            // 
            // commentTextBox
            // 
            this.commentTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.boardBindingSource, "Comment", true));
            this.commentTextBox.Location = new System.Drawing.Point(232, 206);
            this.commentTextBox.Multiline = true;
            this.commentTextBox.Name = "commentTextBox";
            this.commentTextBox.Size = new System.Drawing.Size(200, 78);
            this.commentTextBox.TabIndex = 5;
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.boardBindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(232, 12);
            this.nameTextBox.MaxLength = 100;
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(200, 26);
            this.nameTextBox.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(10, 322);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(216, 26);
            this.label11.TabIndex = 57;
            this.label11.Text = "Data utworzenia";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(12, 290);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(216, 26);
            this.label13.TabIndex = 56;
            this.label13.Text = "Autor";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(12, 206);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(216, 26);
            this.label10.TabIndex = 55;
            this.label10.Text = "Komentarz";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // activeCheckBox
            // 
            this.activeCheckBox.AutoSize = true;
            this.activeCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.boardBindingSource, "IsActive", true));
            this.activeCheckBox.Location = new System.Drawing.Point(232, 354);
            this.activeCheckBox.Name = "activeCheckBox";
            this.activeCheckBox.Size = new System.Drawing.Size(88, 24);
            this.activeCheckBox.TabIndex = 8;
            this.activeCheckBox.Text = "Aktywna";
            this.activeCheckBox.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(10, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(216, 26);
            this.label4.TabIndex = 54;
            this.label4.Text = "Nazwa";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.AutoGenerateColumns = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.symbolDataGridViewTextBoxColumn,
            this.captionDataGridViewTextBoxColumn,
            this.testInputColumn,
            this.testResultColumn});
            this.dataGridView.DataSource = this.boardRegistryBindingSource;
            this.dataGridView.Location = new System.Drawing.Point(464, 108);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.ShowEditingIcon = false;
            this.dataGridView.ShowRowErrors = false;
            this.dataGridView.Size = new System.Drawing.Size(508, 388);
            this.dataGridView.TabIndex = 12;
            // 
            // symbolDataGridViewTextBoxColumn
            // 
            this.symbolDataGridViewTextBoxColumn.DataPropertyName = "Symbol";
            this.symbolDataGridViewTextBoxColumn.HeaderText = "Symbol";
            this.symbolDataGridViewTextBoxColumn.Name = "symbolDataGridViewTextBoxColumn";
            this.symbolDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // captionDataGridViewTextBoxColumn
            // 
            this.captionDataGridViewTextBoxColumn.DataPropertyName = "Caption";
            this.captionDataGridViewTextBoxColumn.HeaderText = "Etykieta";
            this.captionDataGridViewTextBoxColumn.Name = "captionDataGridViewTextBoxColumn";
            this.captionDataGridViewTextBoxColumn.Width = 170;
            // 
            // testInputColumn
            // 
            this.testInputColumn.HeaderText = "Wartość testowa";
            this.testInputColumn.Name = "testInputColumn";
            // 
            // testResultColumn
            // 
            this.testResultColumn.HeaderText = "Wynik testu";
            this.testResultColumn.Name = "testResultColumn";
            this.testResultColumn.ReadOnly = true;
            // 
            // boardRegistryBindingSource
            // 
            this.boardRegistryBindingSource.DataSource = typeof(PanelTester.Data.BoardRegistry);
            // 
            // testButton
            // 
            this.testButton.Location = new System.Drawing.Point(760, 76);
            this.testButton.Name = "testButton";
            this.testButton.Size = new System.Drawing.Size(212, 26);
            this.testButton.TabIndex = 62;
            this.testButton.Text = "Testuj";
            this.testButton.UseVisualStyleBackColor = true;
            this.testButton.Click += new System.EventHandler(this.testButton_Click);
            // 
            // boardTypeComboBox
            // 
            this.boardTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.boardBindingSource, "Type", true));
            this.boardTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.boardTypeComboBox.FormattingEnabled = true;
            this.boardTypeComboBox.Location = new System.Drawing.Point(232, 108);
            this.boardTypeComboBox.Name = "boardTypeComboBox";
            this.boardTypeComboBox.Size = new System.Drawing.Size(200, 28);
            this.boardTypeComboBox.TabIndex = 3;
            this.boardTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.boardTypeComboBox_SelectedIndexChanged);
            // 
            // unitIDNumericUpDown
            // 
            this.unitIDNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.boardBindingSource, "UnitID", true));
            this.unitIDNumericUpDown.Location = new System.Drawing.Point(232, 77);
            this.unitIDNumericUpDown.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.unitIDNumericUpDown.Name = "unitIDNumericUpDown";
            this.unitIDNumericUpDown.Size = new System.Drawing.Size(200, 26);
            this.unitIDNumericUpDown.TabIndex = 2;
            this.unitIDNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // firwareTimeoutNumericUpDown
            // 
            this.firwareTimeoutNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.boardBindingSource, "InstallTimeout", true));
            this.firwareTimeoutNumericUpDown.Location = new System.Drawing.Point(232, 174);
            this.firwareTimeoutNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.firwareTimeoutNumericUpDown.Name = "firwareTimeoutNumericUpDown";
            this.firwareTimeoutNumericUpDown.Size = new System.Drawing.Size(200, 26);
            this.firwareTimeoutNumericUpDown.TabIndex = 4;
            this.firwareTimeoutNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // registryValueNumericUpDown
            // 
            this.registryValueNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.boardBindingSource, "RegistryValue", true));
            this.registryValueNumericUpDown.Location = new System.Drawing.Point(232, 142);
            this.registryValueNumericUpDown.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.registryValueNumericUpDown.Name = "registryValueNumericUpDown";
            this.registryValueNumericUpDown.Size = new System.Drawing.Size(200, 26);
            this.registryValueNumericUpDown.TabIndex = 65;
            this.registryValueNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // BoardEditorDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.ClientSize = new System.Drawing.Size(992, 575);
            this.Name = "BoardEditorDialog";
            this.Text = "Edytor płyty głównej";
            this.Load += new System.EventHandler(this.BoardEditorDialog_Load);
            this.bottomPanel.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.offsetNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardRegistryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitIDNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firwareTimeoutNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registryValueNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource boardBindingSource;
        private System.Windows.Forms.NumericUpDown offsetNumericUpDown;
        private System.Windows.Forms.NumericUpDown countNumericUpDown;
        private System.Windows.Forms.ComboBox typeComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ipTextBox;
        private System.Windows.Forms.TextBox createDateTextBox;
        private System.Windows.Forms.TextBox authorTextBox;
        private System.Windows.Forms.TextBox commentTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox activeCheckBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.BindingSource boardRegistryBindingSource;
        private System.Windows.Forms.Button testButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn symbolDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn captionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn testInputColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn testResultColumn;
        private System.Windows.Forms.NumericUpDown firwareTimeoutNumericUpDown;
        private System.Windows.Forms.NumericUpDown unitIDNumericUpDown;
        private System.Windows.Forms.ComboBox boardTypeComboBox;
        private System.Windows.Forms.NumericUpDown registryValueNumericUpDown;
    }
}
