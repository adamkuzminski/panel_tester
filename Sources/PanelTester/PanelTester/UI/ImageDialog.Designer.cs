﻿namespace PanelTester.UI
{
    partial class ImageDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.loadImageToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.clearImageToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.bottomPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // okButton
            // 
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.pictureBox);
            this.mainPanel.Controls.Add(this.toolStrip);
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadImageToolStripButton,
            this.clearImageToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(728, 25);
            this.toolStrip.TabIndex = 0;
            // 
            // loadImageToolStripButton
            // 
            this.loadImageToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.loadImageToolStripButton.Image = global::PanelTester.Properties.Resources.Open;
            this.loadImageToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.loadImageToolStripButton.Name = "loadImageToolStripButton";
            this.loadImageToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.loadImageToolStripButton.Text = "Wczytaj obraz";
            this.loadImageToolStripButton.Click += new System.EventHandler(this.loadImageToolStripButton_Click);
            // 
            // clearImageToolStripButton
            // 
            this.clearImageToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.clearImageToolStripButton.Image = global::PanelTester.Properties.Resources.Clear;
            this.clearImageToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearImageToolStripButton.Name = "clearImageToolStripButton";
            this.clearImageToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.clearImageToolStripButton.Text = "Wyczyść obraz";
            this.clearImageToolStripButton.Click += new System.EventHandler(this.clearImageToolStripButton_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "jpg";
            this.openFileDialog.Filter = "Obrazy|*.jpg|Wszystkie pliki|*.*";
            this.openFileDialog.ShowReadOnly = true;
            this.openFileDialog.Title = "Wybierz obraz";
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 25);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(728, 477);
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // ImageDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.ClientSize = new System.Drawing.Size(728, 502);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "ImageDialog";
            this.Text = "Obraz";
            this.bottomPanel.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripButton loadImageToolStripButton;
        private System.Windows.Forms.ToolStripButton clearImageToolStripButton;
    }
}
