﻿using PanelTester.Data;
using System;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class UserDialog : BaseDialog
    {
        private UserFormMode mode;

        private User user;

        public UserDialog(UserFormMode mode, User user) : base(mode == UserFormMode.Profile)
        {
            InitializeComponent();

            this.mode = mode;
            this.user = user;

            User loggedUser = AppEngine.GetInstance().User;

            if ((mode == UserFormMode.Profile) || ((mode == UserFormMode.Edit) && (loggedUser != user) && (loggedUser.Role <= user.Role)))
            {
                DisableControls();
            }
        }

        public enum UserFormMode
        {
            New,
            Edit,
            Profile
        }

        public override bool SaveData(out string invalidField)
        {
            invalidField = "";

            if (user == null)
                user = new User();

            Database database = Database.GetInstance();
            String data;
            if (idTextBox.Enabled)
            {
                data = idTextBox.Text;
                if ((data == null) || (data.Trim().Length == 0))
                {
                    invalidField = idLabel.Text;
                    return false;
                }
                data = data.Trim();

                foreach (User user in database.UserList)
                {
                    if (user.Id.Equals(data))
                    {
                        MessageBox.Show("W systemie istnieje użytkownik o podanym identyfikatorze", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                }
                user.Id = data;
            }

            if (nameTextBox.Enabled)
            {
                data = nameTextBox.Text;
                if ((data == null) || (data.Trim().Length == 0))
                {
                    invalidField = nameLabel.Text;
                    return false;
                }
                user.Name = data.Trim();
            }

            if (roleComboBox.Enabled)
            {
                if (roleComboBox.SelectedItem == null)
                {
                    invalidField = roleLabel.Text;
                    return false;
                }
                user.Role = (Role)Enum.Parse(typeof(Role), (string)roleComboBox.SelectedItem);
            }

            if (passwordTextBox.Enabled)
            {
                data = passwordTextBox.Text;
                if ((data == null) || (data.Trim().Length == 0))
                {
                    invalidField = passwordLabel.Text;
                    return false;
                }
                if (repeatPasswordTextBox.Enabled)
                {
                    String pass = repeatPasswordTextBox.Text;
                    if ((pass == null) || (pass.Trim().Length == 0))
                    {
                        invalidField = repeatPasswordLabel.Text;
                        return false;
                    }
                    if (!pass.Equals(data))
                    {
                        MessageBox.Show("Hasło i powtórzenie hasła nie są identyczne", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                }
                user.Password = data;
            }

            return true;
        }

        protected override bool IsKeyboardRequired()
        {
            return true;
        }

        private void changePasswordButton_Click(object sender, EventArgs e)
        {
            passwordTextBox.Text = "";
            passwordTextBox.Enabled = true;
            repeatPasswordTextBox.Text = "";
            repeatPasswordTextBox.Enabled = true;
            changePasswordButton.Visible = false;
        }

        private void idTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                nameTextBox.Focus();
        }

        private void roleComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //Role role = (Role)Enum.Parse(typeof(Role), (string)roleComboBox.SelectedItem);
            //User loggedUser = AppEngine.GetInstance().User;
            //if (role > loggedUser.Role)
            //    roleComboBox.SelectedItem =  Enum.GetName(typeof(Role), loggedUser.Role);
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            switch (mode)
            {
                case UserFormMode.New:
                    Text = "Nowy użytkownik";
                    break;

                case UserFormMode.Edit:
                    Text = "Edytuj użytkownika";
                    break;

                case UserFormMode.Profile:
                    Text = "Profil użytkownika";
                    break;
            }

            roleComboBox.Items.Clear();

            User loggedUser = AppEngine.GetInstance().User;
            foreach (Role role in Enum.GetValues(typeof(Role)))
            {
                if ((mode == UserFormMode.Profile) ||
                    (role < loggedUser.Role) ||
                    ((user != null) && (user.Role == role)))
                    roleComboBox.Items.Add(role.ToString());
            }

            if (user != null)
            {
                idTextBox.Text = user.Id;
                nameTextBox.Text = user.Name;
                roleComboBox.SelectedItem = Enum.GetName(typeof(Role), user.Role);
                passwordTextBox.Text = user.Password;
                repeatPasswordTextBox.Text = user.Password;
            }

            idTextBox.Enabled = (mode == UserFormMode.New);
            nameTextBox.Enabled = (mode != UserFormMode.Profile);
            roleComboBox.Enabled = (mode != UserFormMode.Profile);
            passwordTextBox.Enabled = (mode == UserFormMode.New);
            changePasswordButton.Visible = (mode == UserFormMode.Edit);
            repeatPasswordLabel.Visible = (mode != UserFormMode.Profile);
            repeatPasswordTextBox.Visible = (mode != UserFormMode.Profile);
            repeatPasswordTextBox.Enabled = (mode == UserFormMode.New);
        }
    }
}