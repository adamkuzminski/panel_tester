﻿using PanelTester.Common;
using PanelTester.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class TestCaseEditorDialog : PanelTester.UI.BaseDialog
    {
        private const int KBoardRegistryTabIndex = 2;
        private const int KMainDataTabIndex = 0;
        private const int KPowerTabIndex = 3;
        private const int KSocketTabIndex = 1;
        private const int KStepTabIndex = 4;
        private int currentStepIndex;
        private bool initStepInputDataGrid;
        private bool configModified;

        public TestCaseEditorDialog(TestCase testCase, bool readOnly) : base(readOnly)
        {
            InitializeComponent();
            testCaseBindingSource.Add(testCase);
            currentStepIndex = 0;

            if (readOnly)
            {
                DisableControls(mainDataTabPage);
                DisableControls(powerTabPage);
                DisableControls(stepToolStrip);

                socketPinDataGridView.ReadOnly = true;
                boardRegistryDataGridView.ReadOnly = true;
                stepInputDataGridView.ReadOnly = true;
                stepOutputDataGridView.ReadOnly = true;
            }
        }

        protected override bool IsKeyboardRequired()
        {
            return true;
        }

        private void activeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            TestCase testCase = GetTestCase();
            if (activeCheckBox.Checked)
            {
                Database database = Database.GetInstance();
                foreach (TestCase tc in database.TestCaseList)
                    if ((tc.Model == testCase.Model) && tc.IsActive && (tc.Version != testCase.Version))
                    {
                        tc.IsActive = false;
                        database.SaveTestCase(tc);
                    }
            }
        }

        private bool AddTestStep(TestCase testCase)
        {
            List<TestCaseStep> list = testCase.StepList;
            if (list.Count == 0)
            {
                if (testCase.PowerModeList.Count == 0)
                {
                    MessageBox.Show("Nie wybrano wariantów zasilania", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }

                //Very first step - prepare lists
                TestCaseStep step = new TestCaseStep();

                //Sockets
                foreach (Socket socket in testCase.SocketList)
                {
                    foreach (SocketPin pin in socket.PinList)
                    {
                        if (pin.InUse && pin.IsVariable())
                        {
                            if ((socket.Type == SocketType.AOut) || (socket.Type == SocketType.DOut))
                                step.OutputList.Add(new TestCaseStepOutput(pin));
                            else
                                step.InputList.Add(new TestCaseStepInput(pin, testCase.PowerModeList));
                        }
                    }
                }

                //Board registries
                foreach (BoardRegistrySet boardRegistrySet in testCase.RegistrySetList)
                {
                    foreach (BoardRegistry boardRegistry in boardRegistrySet.RegistryList)
                    {
                        if (boardRegistry.InUse)
                        {
                            if ((boardRegistrySet.Type == BoardRegistryType.AOut) || (boardRegistrySet.Type == BoardRegistryType.DOut))
                                step.OutputList.Add(new TestCaseStepOutput(boardRegistry));
                            else
                                step.InputList.Add(new TestCaseStepInput(boardRegistry, testCase.PowerModeList));
                        }
                    }
                }

                list.Add(step);
                currentStepIndex = 0;
            }
            else
            {
                TestCaseStep step = list[list.Count - 1].Clone(false);
                list.Add(step);
            }
            return true;
        }

        private void addStepToolStripButton_Click(object sender, EventArgs e)
        {
            //Add new test case step
            TestCase testCase = GetTestCase();
            AddTestStep(testCase);
            //Show step data
            ShowStep(testCase.StepList.Count - 1);
        }

        private void boardComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            TestCase testCase = GetTestCase();
            Board board = (Board)boardComboBox.SelectedItem;
            if ((board != null) && (testCase.BoardId != board.Id))
            {
                if ((testCase.BoardId == null) || (MessageBox.Show("Czy na pewno chcesz zmienić typ płyty głównej", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
                {
                    //Copy board data
                    testCase.SetBoard(board);
                    RefreshBoardRegistryDataGrid();
                }
            }
        }

        private void boardRegistryDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            BoardRegistry boardRegistry = boardRegistryDataGridView.Rows[e.RowIndex].DataBoundItem as BoardRegistry;
            e.CellStyle.BackColor = (boardRegistry.InUse && !boardRegistryDataGridView.Columns[e.ColumnIndex].ReadOnly) ? Consts.StandardRowColor : Consts.NotUsedRowColor;
        }

        private void boardRegistryTypeToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshBoardRegistryDataGrid();
        }

        private void copyStepToolStripButton_Click(object sender, EventArgs e)
        {
            TestCase testCase = GetTestCase();
            List<TestCaseStep> list = testCase.StepList;
            if (list.Count > 0)
            {
                if ((currentStepIndex < 0) || (currentStepIndex >= list.Count))
                    currentStepIndex = list.Count - 1;
                TestCaseStep step = list[currentStepIndex].Clone(true);
                list.Add(step);
                ShowStep(list.Count - 1);
            }
        }

        private void ClearStepDataGrid()
        {
            initStepInputDataGrid = true;
            stepOutputDataGridView.DataSource = null;
            stepInputDataGridView.DataSource = null;
            for (int i = stepInputDataGridView.Columns.Count - 1; i >= 2; i--)
                stepInputDataGridView.Columns.RemoveAt(i);
            stepInputDataGridView.Columns[1].HeaderText = "Zasilanie";
        }

        private void deleteStepToolStripButton_Click(object sender, EventArgs e)
        {
            TestCase testCase = GetTestCase();
            if ((currentStepIndex >= 0) && (currentStepIndex < testCase.StepList.Count) && (MessageBox.Show("Czy usunąć bieżący krok scenariusza", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
            {
                testCase.StepList.RemoveAt(currentStepIndex);
                if (testCase.StepList.Count == 0)
                    ClearStepDataGrid();
                ShowStep(currentStepIndex - 1);
            }
        }

        private TestCase GetTestCase()
        {
            return (TestCase)testCaseBindingSource[0];
        }

        private bool IsPowerModeAvailable(PowerMode powerMode)
        {
            Socket socket = GetTestCase().SocketList[(int)SocketType.AOut];
            //Check L1
            if ((powerMode == PowerMode.All) || (powerMode == PowerMode.L1) || (powerMode == PowerMode.L1_L2) || (powerMode == PowerMode.L1_L3))
                if (socket.GetFirstPinInUseByType(SocketPinType.Out230VACL1Power) == null)
                    return false;

            //Check L2
            if ((powerMode == PowerMode.All) || (powerMode == PowerMode.L2) || (powerMode == PowerMode.L1_L2) || (powerMode == PowerMode.L2_L3))
                if (socket.GetFirstPinInUseByType(SocketPinType.Out230VACL2Power) == null)
                    return false;

            //Check L3
            if ((powerMode == PowerMode.All) || (powerMode == PowerMode.L3) || (powerMode == PowerMode.L1_L3) || (powerMode == PowerMode.L2_L3))
                if (socket.GetFirstPinInUseByType(SocketPinType.Out230VACL3Power) == null)
                    return false;

            return true;
        }

        private void nextStepToolStripButton_Click(object sender, EventArgs e)
        {
            ShowStep(currentStepIndex + 1);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < tabControl.TabCount; i++)
                if (!ValidateData(i))
                {
                    if (MessageBox.Show("Czy zapisać zmiany w nieuzupełnionym scenariuszu", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        break;

                    tabControl.SelectedIndex = i;
                    return;
                }

            DialogResult = DialogResult.OK;
        }

        private void powerAddButton_Click(object sender, EventArgs e)
        {
            if (powerSourceListBox.SelectedItem != null)
            {
                PowerMode powerMode = (PowerMode)powerSourceListBox.SelectedItem;
                powerTargetListBox.Items.Add(powerMode);
                powerSourceListBox.Items.Remove(powerMode);
                UpdatePowerModes();
            }
        }

        private void powerDownButton_Click(object sender, EventArgs e)
        {
            int index = powerTargetListBox.SelectedIndex;
            if (index < powerTargetListBox.Items.Count - 1)
            {
                PowerMode powerMode = (PowerMode)powerTargetListBox.SelectedItem;
                powerTargetListBox.Items.RemoveAt(index);
                powerTargetListBox.Items.Insert(index + 1, powerMode);
                UpdatePowerModes();
            }
        }

        private void powerListBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            PowerMode powerMode = (PowerMode)(sender as ListBox).Items[e.Index];
            Color foreColor = IsPowerModeAvailable(powerMode) ? e.ForeColor : Consts.ErrorColor;
            e.Graphics.DrawString(typeof(PowerMode).GetMember(powerMode.ToString()).FirstOrDefault()?.GetCustomAttribute<DescriptionAttribute>()?.Description, e.Font, new SolidBrush(foreColor), e.Bounds);
            e.DrawFocusRectangle();
        }

        private void powerRemoveButton_Click(object sender, EventArgs e)
        {
            if (powerTargetListBox.SelectedItem != null)
            {
                PowerMode powerMode = (PowerMode)powerTargetListBox.SelectedItem;
                powerSourceListBox.Items.Add(powerMode);
                powerTargetListBox.Items.Remove(powerMode);
                UpdatePowerModes();
            }
        }

        private void powerSourceListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshPowerButtons();
        }

        private void powerTargetListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshPowerButtons();
        }

        private void powerUpButton_Click(object sender, EventArgs e)
        {
            int index = powerTargetListBox.SelectedIndex;
            if (index > 0)
            {
                PowerMode powerMode = (PowerMode)powerTargetListBox.SelectedItem;
                powerTargetListBox.Items.RemoveAt(index);
                powerTargetListBox.Items.Insert(index - 1, powerMode);
                UpdatePowerModes();
            }
        }

        private void prevStepToolStripButton_Click(object sender, EventArgs e)
        {
            ShowStep(currentStepIndex - 1);
        }

        private void RefreshBoardRegistryDataGrid()
        {
            BoardRegistryType boardRegistryType = (BoardRegistryType)boardRegistryTypeToolStripComboBox.SelectedIndex;
            if (boardRegistryType != BoardRegistryType.Unknown)
            {
                TestCase testCase = GetTestCase();
                var bindingList = new BindingList<BoardRegistry>(testCase.RegistrySetList[(int)boardRegistryType].RegistryList);
                boardRegistryBindingSource = new BindingSource(bindingList, null);
                boardRegistryDataGridView.DataSource = boardRegistryBindingSource;
            }
        }

        private void refreshBoardToolStripButton_Click(object sender, EventArgs e)
        {
            TestCase testCase = GetTestCase();
            Board dbBoard = Database.GetInstance().FindById<Board>(testCase.BoardId);
            if (dbBoard == null)
            {
                MessageBox.Show("Nie znaleziono odpowiedniej płyty", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                Dictionary<string, bool> map = new Dictionary<string, bool>();
                foreach (BoardRegistrySet boardRegistrySet in testCase.RegistrySetList)
                    foreach (BoardRegistry boardRegistry in boardRegistrySet.RegistryList)
                        map.Add(boardRegistry.Symbol, boardRegistry.InUse);
                testCase.SetBoard(dbBoard);
                foreach (BoardRegistrySet boardRegistrySet in testCase.RegistrySetList)
                    foreach (BoardRegistry boardRegistry in boardRegistrySet.RegistryList)
                        boardRegistry.InUse = map.TryGetValue(boardRegistry.Symbol, out bool value) && value;
                RefreshBoardRegistryDataGrid();
                MessageBox.Show("Operacja zakończona", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                configModified = true;
            }
        }

        private void RefreshPowerButtons()
        {
            powerAddButton.Enabled = !readOnly && (powerSourceListBox.SelectedItem != null);
            powerRemoveButton.Enabled = !readOnly && (powerTargetListBox.SelectedItem != null);
            powerUpButton.Enabled = !readOnly && ((powerTargetListBox.SelectedItem != null) && (powerTargetListBox.SelectedIndex > 0));
            powerDownButton.Enabled = !readOnly && ((powerTargetListBox.SelectedItem != null) && (powerTargetListBox.SelectedIndex < powerTargetListBox.Items.Count - 1));
        }

        private void RefreshSocketPinDataGrid()
        {
            SocketType socketType = (SocketType)socketTypeToolStripComboBox.SelectedIndex;
            if (socketType != SocketType.Unknown)
            {
                TestCase testCase = GetTestCase();
                var bindingList = new BindingList<SocketPin>(testCase.SocketList[(int)socketType].PinList);
                socketPinBindingSource = new BindingSource(bindingList, null);
                socketPinDataGridView.DataSource = socketPinBindingSource;
            }
        }

        private bool SelectErrorImage(string inputFileName, out string outputFileName)
        {
            outputFileName = "";
            if (inputFileName == null)
                inputFileName = "";

            TestCase testCase = GetTestCase();
            string errorImagePath = testCase.GetErrorImagePath();
            Directory.CreateDirectory(errorImagePath);

            ImageDialog imageDialog = new ImageDialog
            {
                Text = "Wybierz obraz dla błędu",
                FileName = Path.Combine(errorImagePath, inputFileName)
            };
            if (imageDialog.ShowDialog() != DialogResult.OK)
                return false;

            string fileName = imageDialog.FileName;
            if ((fileName.Length > 0) && !errorImagePath.Equals(Path.GetDirectoryName(fileName), StringComparison.CurrentCultureIgnoreCase))
            {
                string fileNamePrefix = Path.GetFileName(fileName);
                //Copy image to local folder
                string destFileName = Path.Combine(errorImagePath, fileNamePrefix);
                int index = 1;
                while (File.Exists(destFileName))
                    destFileName = Path.Combine(errorImagePath, String.Format("{0}_{1}", index++, fileNamePrefix));
                File.Copy(fileName, destFileName, true);
                fileName = destFileName;
            }
            outputFileName = Path.GetFileName(fileName);
            return true;
        }

        private bool ShowStep(int stepIndex)
        {
            List<TestCaseStep> list = GetTestCase().StepList;
            if ((stepIndex < 0) && (list.Count > 0))
                stepIndex = 0;
            else if ((stepIndex >= list.Count))
                stepIndex = list.Count - 1;

            currentStepIndex = stepIndex;
            UpdateStepToolStrip();

            if ((stepIndex < 0) || (stepIndex >= list.Count))
                return false;

            TestCase testCase = GetTestCase();

            if (initStepInputDataGrid)
            {
                initStepInputDataGrid = false;

                //Dynamic input grid columns
                DataGridViewColumn powerColumn = stepInputDataGridView.Columns[1];
                for (int i = 0; i < testCase.PowerModeList.Count; i++)
                {
                    PowerMode powerMode = testCase.PowerModeList[i];
                    string headerText = typeof(PowerMode).GetMember(powerMode.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description;

                    if (i > 0)
                        powerColumn = (DataGridViewColumn)powerColumn.Clone();
                    powerColumn.DataPropertyName = String.Format("InUse{0}", i);
                    powerColumn.HeaderText = headerText;
                    if (i > 0)
                        stepInputDataGridView.Columns.Add(powerColumn);
                }
            }

            TestCaseStep step = list[stepIndex];
            //Outputs
            {
                var bindingList = new BindingList<TestCaseStepOutput>(step.OutputList);
                testCaseStepOutputBindingSource = new BindingSource(bindingList, null);
                stepOutputDataGridView.DataSource = testCaseStepOutputBindingSource;
            }

            //Inputs
            {
                var bindingList = new BindingList<TestCaseStepInput>(step.InputList);
                testCaseStepInputBindingSource = new BindingSource(bindingList, null);
                stepInputDataGridView.DataSource = testCaseStepInputBindingSource;
            }

            //Other
            stepDelayToolStripTextBox.TextBox.DataBindings.Clear();
            stepDelayToolStripTextBox.TextBox.DataBindings.Add("Text", step, "Delay", false, DataSourceUpdateMode.OnPropertyChanged);

            stepErrorMessageToolStripTextBox.TextBox.DataBindings.Clear();
            stepErrorMessageToolStripTextBox.TextBox.DataBindings.Add("Text", step, "ErrorMessage", false, DataSourceUpdateMode.OnPropertyChanged);

            return true;
        }

        private void socketPinDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            SocketPin socketPin = socketPinDataGridView.Rows[e.RowIndex].DataBoundItem as SocketPin;
            DataGridViewColumn column = socketPinDataGridView.Columns[e.ColumnIndex];
            if (column == pinTypeDataGridViewTextBoxColumn)
                e.Value = typeof(SocketPinType).GetMember(socketPin.PinType.ToString()).FirstOrDefault().GetCustomAttribute<PinAttribute>().Label;

            if (socketPin.ReadOnly)
                e.CellStyle.BackColor = Consts.DisabledRowColor;
            else
                e.CellStyle.BackColor = (socketPin.InUse && !column.ReadOnly) ? Consts.StandardRowColor : Consts.NotUsedRowColor;
        }

        private void socketPinDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == 1) && (e.RowIndex != -1))
            {
                //In Use column
                SocketPin socketPin = socketPinDataGridView.Rows[e.RowIndex].DataBoundItem as SocketPin;
                int[] connectedPins = socketPin.ConnectedPins;
                if (!socketPin.ReadOnly && (connectedPins != null))
                {
                    bool inUse = false;
                    foreach (DataGridViewRow row in socketPinDataGridView.Rows)
                    {
                        int[] rowConnectedPins = (row.DataBoundItem as SocketPin).ConnectedPins;
                        if (rowConnectedPins != null)
                        {
                            foreach (int pin in connectedPins)
                                if (rowConnectedPins.Contains(pin))
                                {
                                    inUse |= (row.DataBoundItem as SocketPin).InUse;
                                    if (inUse)
                                        break;
                                }
                            if (inUse)
                                break;
                        }
                    }
                    foreach (int pin in connectedPins)
                        socketPinDataGridView.Rows[pin - 1].Cells[e.ColumnIndex].Value = inUse;
                }
            }
        }

        private void socketPinDataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            //Check box cell value changed override
            if ((socketPinDataGridView.CurrentCell.ColumnIndex == 1) && socketPinDataGridView.IsCurrentCellDirty)
                socketPinDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            configModified = true;
        }

        private void socketPinDataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            for (int i = 0; i < e.RowCount; i++)
            {
                DataGridViewRow row = socketPinDataGridView.Rows[i + e.RowIndex];
                row.Cells[1].ReadOnly = (row.DataBoundItem as SocketPin).ReadOnly;
            }
        }

        private void socketTypeToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshSocketPinDataGrid();
        }

        private void stepDelayToolStripTextBox_TextChanged(object sender, EventArgs e)
        {
            int selectionStart = stepDelayToolStripTextBox.SelectionStart;
            bool invalidCharacters = false;
            string oldText = stepDelayToolStripTextBox.Text;
            string newText = "";
            for (int i = 0; i < oldText.Length; i++)
            {
                char c = oldText[i];
                if (Char.IsDigit(c))
                    newText += c;
                else
                    invalidCharacters = true;
            }
            if (invalidCharacters)
            {
                stepDelayToolStripTextBox.Text = newText;
                stepDelayToolStripTextBox.SelectionStart = selectionStart - 1;
            }
        }

        private void stepErrorImageToolStripButton_Click(object sender, EventArgs e)
        {
            TestCase testCase = GetTestCase();
            if ((currentStepIndex >= 0) && (currentStepIndex < testCase.StepList.Count))
            {
                TestCaseStep step = testCase.StepList[currentStepIndex];
                string fileName;
                if (SelectErrorImage(step.ErrorImage, out fileName))
                {
                    step.ErrorImage = fileName;
                    UpdateStepToolStrip();
                }
            }
        }

        private void stepInputDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            stepInputNameLabel.DataBindings.Clear();
            stepInputMinValueNumericUpDown.DataBindings.Clear();
            stepInputMaxValueNumericUpDown.DataBindings.Clear();
            stepInputLogicValueCheckBox.DataBindings.Clear();
            stepInputErrorTextBox.DataBindings.Clear();
            stepInputFatalErrorCheckBox.DataBindings.Clear();
            stepInputImageButton.DataBindings.Clear();

            TestCaseStepInput input = stepInputDataGridView.Rows[e.RowIndex].DataBoundItem as TestCaseStepInput;

            if ((e.ColumnIndex > 0) && (e.ColumnIndex - 1 < input.ItemList.Length))
            {
                stepInputNameLabel.DataBindings.Add("Text", input, "Label", false, DataSourceUpdateMode.Never);
                stepInputPowerModeLabel.Text = stepInputDataGridView.Columns[e.ColumnIndex].HeaderText;

                TestCaseStepInputItem item = input.ItemList[e.ColumnIndex - 1];
                stepInputPanel.Enabled = !readOnly && item.InUse;

                stepInputErrorTextBox.DataBindings.Add("Text", item, "ErrorMessage", false, DataSourceUpdateMode.OnPropertyChanged);
                stepInputFatalErrorCheckBox.DataBindings.Add("Checked", item, "IsFatalError", false, DataSourceUpdateMode.OnPropertyChanged);
                stepInputImageButton.DataBindings.Add("Text", item, "ErrorImage", false, DataSourceUpdateMode.OnPropertyChanged);

                if (input.Logic)
                {
                    stepInputMaxValueNumericUpDown.Visible = false;
                    stepInputMinValueNumericUpDown.Visible = false;
                    stepInputLogicValueCheckBox.Visible = true;
                    stepInputLogicValueCheckBox.DataBindings.Add("Checked", item, "MaxValue", false, DataSourceUpdateMode.OnPropertyChanged);
                }
                else
                {
                    stepInputMaxValueNumericUpDown.Visible = true;
                    stepInputMinValueNumericUpDown.Visible = true;
                    stepInputLogicValueCheckBox.Visible = false;

                    stepInputMaxValueNumericUpDown.Minimum = input.RangeMin;
                    stepInputMaxValueNumericUpDown.Maximum = input.RangeMax;
                    stepInputMinValueNumericUpDown.Minimum = input.RangeMin;
                    stepInputMinValueNumericUpDown.Maximum = input.RangeMax;
                    if (input.Integer)
                    {
                        stepInputMaxValueNumericUpDown.DecimalPlaces = 0;
                        stepInputMaxValueNumericUpDown.Increment = 1;
                        stepInputMinValueNumericUpDown.DecimalPlaces = 0;
                        stepInputMinValueNumericUpDown.Increment = 1;
                    }
                    else
                    {
                        stepInputMaxValueNumericUpDown.DecimalPlaces = 1;
                        stepInputMaxValueNumericUpDown.Increment = (decimal)0.1;
                        stepInputMinValueNumericUpDown.DecimalPlaces = 1;
                        stepInputMinValueNumericUpDown.Increment = (decimal)0.1;
                    }
                    stepInputMinValueNumericUpDown.DataBindings.Add("Text", item, "MinValue", false, DataSourceUpdateMode.OnPropertyChanged);
                    stepInputMaxValueNumericUpDown.DataBindings.Add("Text", item, "MaxValue", false, DataSourceUpdateMode.OnPropertyChanged);
                }

                stepInputPanel.Visible = true;
            }
            else
            {
                stepInputPanel.Visible = false;
            }
        }

        private void stepInputDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            TestCaseStepInput input = (stepInputDataGridView.Rows[e.RowIndex].DataBoundItem as TestCaseStepInput);
            if ((e.ColumnIndex > 0) && !input.GetInUse(e.ColumnIndex - 1))
                e.CellStyle.BackColor = Consts.NotUsedRowColor;
            else
                e.CellStyle.BackColor = input.Socket ? Consts.StandardRowColor : Consts.AlternativeRowColor;
        }

        private void stepInputDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                TestCaseStepInput input = stepInputDataGridView.Rows[e.RowIndex].DataBoundItem as TestCaseStepInput;
                if ((e.ColumnIndex > 0) && (e.ColumnIndex - 1 < input.ItemList.Length))
                {
                    TestCaseStepInputItem item = input.ItemList[e.ColumnIndex - 1];
                    stepInputPanel.Enabled = !readOnly && item.InUse;
                }
            }
        }

        private void stepInputDataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            //Check box cell value changed override
            if (stepInputDataGridView.IsCurrentCellDirty)
                stepInputDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void stepInputImageButton_Click(object sender, EventArgs e)
        {
            string inputFileName = stepInputImageButton.Text;
            string outputFileName;
            if (SelectErrorImage(inputFileName, out outputFileName))
                stepInputImageButton.Text = outputFileName;
        }

        private void stepOutputDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            TestCaseStepOutput output = (stepOutputDataGridView.Rows[e.RowIndex].DataBoundItem as TestCaseStepOutput);
            if (output.InUse)
                e.CellStyle.BackColor = output.Socket ? Consts.StandardRowColor : Consts.AlternativeRowColor;
            else
                e.CellStyle.BackColor = Consts.NotUsedRowColor;
        }

        private void stepOutputDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex >= 0) && (e.ColumnIndex == 0))
            {
                TestCaseStepOutput output = stepOutputDataGridView.Rows[e.RowIndex].DataBoundItem as TestCaseStepOutput;
                stepOutputPanel.Enabled = !readOnly && output.InUse;
            }
        }

        private void stepOutputDataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            //Check box cell value changed override
            if (stepOutputDataGridView.IsCurrentCellDirty)
                stepOutputDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void stepOutputDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            stepOutputNameLabel.DataBindings.Clear();
            stepOutputValueNumericUpDown.DataBindings.Clear();
            if (stepOutputDataGridView.SelectedRows.Count > 0)
            {
                TestCaseStepOutput output = stepOutputDataGridView.SelectedRows[0].DataBoundItem as TestCaseStepOutput;
                stepOutputNameLabel.DataBindings.Add("Text", output, "Label", false, DataSourceUpdateMode.Never);
                stepOutputPanel.Enabled = !readOnly && output.InUse;

                if (output.Logic)
                {
                    stepOutputValueLabel.Visible = false;
                    stepOutputValueNumericUpDown.Visible = false;
                }
                else
                {
                    stepOutputValueLabel.Visible = true;
                    stepOutputValueNumericUpDown.Visible = true;
                    stepOutputValueNumericUpDown.Minimum = output.RangeMin;
                    stepOutputValueNumericUpDown.Maximum = output.RangeMax;
                    if (output.Integer)
                    {
                        stepOutputValueNumericUpDown.DecimalPlaces = 0;
                        stepOutputValueNumericUpDown.Increment = 1;
                    }
                    else
                    {
                        stepOutputValueNumericUpDown.DecimalPlaces = 1;
                        stepOutputValueNumericUpDown.Increment = (decimal)0.1;
                    }
                    stepOutputValueNumericUpDown.DataBindings.Add("Text", output, "Value", false, DataSourceUpdateMode.OnPropertyChanged);
                }

                stepOutputPanel.Visible = true;
            }
            else
            {
                stepOutputPanel.Visible = false;
            }
        }

        private void tabControl_Deselecting(object sender, TabControlCancelEventArgs e)
        {
            if (!tabControl.Enabled)
                return;

            ValidateConfig();

            if (!ValidateData(e.TabPageIndex) && (e.TabPageIndex == KMainDataTabIndex))
                e.Cancel = true;
        }

        private void tabControl_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPageIndex == KPowerTabIndex) //Power page
            {
                powerTargetListBox.Refresh();
                powerSourceListBox.Refresh();
            }
        }

        private void tabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {
            //Test case must has valid all data before enter the steps page
            if (e.TabPageIndex == KStepTabIndex)
                for (int i = 0; i < tabControl.TabCount - 1; i++)
                    if (!ValidateData(i))
                    {
                        e.Cancel = true;
                        break;
                    }
        }

        private void TestCaseEditorDialog_Load(object sender, EventArgs e)
        {
            initStepInputDataGrid = true;

            TestCase testCase = GetTestCase();
            boardComboBox.Text = testCase.BoardName;
            foreach (Board board in Database.GetInstance().BoardList)
                if (board.IsActive)
                {
                    boardBindingSource.Add(board);
                    if (testCase.BoardId == board.Id)
                        boardComboBox.SelectedItem = board;
                }
            socketTypeToolStripComboBox.SelectedIndex = 0;
            boardRegistryTypeToolStripComboBox.SelectedIndex = 0;

            if (testCase.Resistance > 0)
                resistanceComboBox.Text = testCase.Resistance.ToString();
            resistanceComboBox.DataBindings.Add("SelectedItem", testCase, "Resistance", false, DataSourceUpdateMode.OnPropertyChanged);

            UpdatePowerListBoxes();

            ShowStep(0);

            nameTextBox.Focus();
        }

        private void UpdatePowerListBoxes()
        {
            TestCase testCase = GetTestCase();

            //testCase.SocketList[(int)SocketType.AOut]

            powerSourceListBox.Items.Clear();
            foreach (PowerMode powerMode in Enum.GetValues(typeof(PowerMode)))
                if (!testCase.PowerModeList.Contains(powerMode))
                    powerSourceListBox.Items.Add(powerMode);

            powerTargetListBox.Items.Clear();
            foreach (PowerMode powerMode in testCase.PowerModeList)
                powerTargetListBox.Items.Add(powerMode);

            RefreshPowerButtons();
        }

        private void UpdatePowerModes()
        {
            TestCase testCase = GetTestCase();
            testCase.PowerModeList.Clear();
            for (int i = 0; i < powerTargetListBox.Items.Count; i++)
                testCase.PowerModeList.Add((PowerMode)powerTargetListBox.Items[i]);

            configModified = true;

            RefreshPowerButtons();
        }

        private void UpdateStepToolStrip()
        {
            TestCase testCase = GetTestCase();
            int steps = testCase.StepList.Count;
            copyStepToolStripButton.Enabled = !readOnly && (steps > 0);
            deleteStepToolStripButton.Enabled = !readOnly && (steps > 0);
            prevStepToolStripButton.Enabled = (currentStepIndex > 0);
            nextStepToolStripButton.Enabled = (steps > currentStepIndex + 1);
            deleteStepToolStripButton.Enabled = !readOnly && (steps > 0);
            stepInfoToolStripLabel.Text = (steps > 0) ? String.Format("{0} z {1}", currentStepIndex + 1, steps) : "Brak kroków";

            string text = "Wybierz zdjęcie błędu...";
            if ((currentStepIndex >= 0) && (currentStepIndex < steps))
            {
                TestCaseStep step = testCase.StepList[currentStepIndex];
                if ((step.ErrorImage != null) && (step.ErrorImage.Length > 0))
                    text = step.ErrorImage;
            }

            stepErrorImageToolStripButton.Text = text;
        }

        private void ValidateConfig()
        {
            TestCase testCase = GetTestCase();
            if (configModified && (testCase.StepList.Count > 0))
            {
                List<TestCaseStep> oldSteps = new List<TestCaseStep>();
                testCase.StepList.ForEach(s => oldSteps.Add(s.Clone(true)));
                testCase.StepList.Clear();

                for (int i = 0; i < oldSteps.Count; i++)
                {
                    TestCaseStep oldStep = oldSteps[i];
                    AddTestStep(testCase);
                    testCase.StepList[i].Update(oldStep);
                }

                MessageBox.Show("Zmieniono konfigurację scenariusza.\nKroki scenariusza zostały zaktualizowane", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearStepDataGrid();
                UpdateStepToolStrip();
                ShowStep(0);
            }
            configModified = false;
        }

        /*private bool UpdateSocketsInSteps()
        {
            TestCase testCase = GetTestCase();
            if (testCase.IsLocked() || (testCase.StepList.Count == 0))
                return false;

            TestCaseStep refStep = testCase.StepList[0];

            //Sockets
            foreach (Socket socket in testCase.SocketList)
            {
                foreach (SocketPin pin in socket.PinList)
                {
                    if (!pin.IsVariable())
                        continue;

                    if ((socket.Type == SocketType.AOut) || (socket.Type == SocketType.DOut))
                    {
                        bool found = false;
                        for (int i = refStep.OutputList.Count - 1; i >= 0; i--)
                        {
                            TestCaseStepOutput output = refStep.OutputList[i];
                            if (output.Socket && (output.Number == pin.Number))
                            {
                                found = true;
                                if (!pin.InUse)
                                    foreach (TestCaseStep step in testCase.StepList)
                                        step.OutputList.RemoveAt(i);
                                break;
                            }
                        }
                        if (!found && pin.InUse)
                            foreach (TestCaseStep step in testCase.StepList)
                                step.OutputList.Add(new TestCaseStepOutput(pin));
                    }
                    else
                    {
                        step.InputList.Add(new TestCaseStepInput(pin, testCase.PowerModeList));
                    }
                }
            }

            return true;
        }*/

        private bool ValidateData(int tabIndex)
        {
            string errorMessage = "";
            TestCase testCase = GetTestCase();

            switch (tabIndex)
            {
                case KMainDataTabIndex: //Main data
                    {
                        errorMessage = "Uzupełnij dane: ";
                        if ((testCase.Name == null) || (testCase.Name.Trim().Length == 0))
                        {
                            errorMessage += "Nazwa";
                            break;
                        }
                        if ((testCase.ModelDocNumber == null) || (testCase.ModelDocNumber.Trim().Length == 0))
                        {
                            errorMessage += "Numer dokumentacji";
                            break;
                        }
                        if ((testCase.ProductName == null) || (testCase.ProductName.Trim().Length == 0))
                        {
                            errorMessage += "Nazwa produktu";
                            break;
                        }
                        if ((testCase.GetBoard() == null) || (testCase.GetBoard().AddressIP == null) || (testCase.GetBoard().AddressIP.Trim().Length == 0))
                        {
                            errorMessage += "Typ płyty głównej";
                            break;
                        }
                        if (testCase.Resistance <= 0)
                        {
                            errorMessage += "Wartość rezystancji wtyczek";
                            break;
                        }
                        return true;
                    }

                case KSocketTabIndex: //Socket
                    {
                        Socket socket = testCase.SocketList[(int)SocketType.AOut];
                        //Power sockets
                        bool error = false;
                        do
                        {
                            if (socket.GetFirstPinInUseByType(SocketPinType.Out230VACL1Power) != null)
                                break;
                            if (socket.GetFirstPinInUseByType(SocketPinType.Out230VACL2Power) != null)
                                break;
                            if (socket.GetFirstPinInUseByType(SocketPinType.Out230VACL3Power) != null)
                                break;
                            error = true;
                        }
                        while (false);
                        if (error)
                        {
                            errorMessage = "Wybierz przynajmniej jedno źródło zasilania\nZasilanie 230VAC L1, L2 lub L3 w Złączu 1";
                            break;
                        }
                        //Continous
                        if (socket.GetFirstPinInUseByType(SocketPinType.OutN) == null)
                        {
                            errorMessage = "Wybierz przynajmniej jeden pin " + typeof(SocketPinType).GetMember(SocketPinType.OutN.ToString()).FirstOrDefault().GetCustomAttribute<PinAttribute>().Label + " w Złączu 1";
                            break;
                        }
                        if (socket.GetFirstPinInUseByType(SocketPinType.OutPE) == null)
                        {
                            errorMessage = "Wybierz przynajmniej jeden pin " + typeof(SocketPinType).GetMember(SocketPinType.OutPE.ToString()).FirstOrDefault().GetCustomAttribute<PinAttribute>().Label + " w Złączu 1";
                            break;
                        }

                        //Continous
                        if (testCase.SocketList[(int)SocketType.AIn].GetFirstPinInUseByType(SocketPinType.InN) == null)
                        {
                            errorMessage = "Wybierz przynajmniej jeden pin " + typeof(SocketPinType).GetMember(SocketPinType.InN.ToString()).FirstOrDefault().GetCustomAttribute<PinAttribute>().Label + " w Złączu 2";
                            break;
                        }
                        if (testCase.SocketList[(int)SocketType.DIn].GetFirstPinInUseByType(SocketPinType.InPE) == null)
                        {
                            errorMessage = "Wybierz przynajmniej jeden pin " + typeof(SocketPinType).GetMember(SocketPinType.InPE.ToString()).FirstOrDefault().GetCustomAttribute<PinAttribute>().Label + " w Złączu 4";
                            break;
                        }

                        return true;
                    }

                case KBoardRegistryTabIndex: //Registry
                    //Nothing to check
                    return true;

                case KPowerTabIndex: //Power
                    {
                        if (testCase.PowerModeList.Count == 0)
                        {
                            errorMessage = "Wybierz przynajmniej jedną konfigurację zasilania";
                            break;
                        }
                        bool error = false;
                        foreach (PowerMode powerMode in testCase.PowerModeList)
                            if (!IsPowerModeAvailable(powerMode))
                            {
                                errorMessage = String.Format("Wariant zasilania {0} nie może zostać użyty",
                                    typeof(PowerMode).GetMember(powerMode.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description);
                                error = true;
                                break;
                            }
                        if (!error)
                            return true;
                    }
                    break;

                case KStepTabIndex: //Steps
                    {
                        if (testCase.StepList.Count == 0)
                        {
                            errorMessage = "Scenariusz nie ma zdefiniowanych kroków";
                            break;
                        }
                    }
                    return true;

                default:
                    return true;
            }
            MessageBox.Show(errorMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            return false;
        }

        private void boardRegistryDataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            configModified = true;
        }
    }
}