﻿namespace PanelTester.UI
{
    partial class TestInitDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.orderTextBox = new System.Windows.Forms.TextBox();
            this.modelComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.modelRichTextBox = new System.Windows.Forms.RichTextBox();
            this.quantityNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.orderRichTextBox = new System.Windows.Forms.RichTextBox();
            this.bottomPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(0, 518);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.orderTextBox);
            this.mainPanel.Controls.Add(this.modelComboBox);
            this.mainPanel.Controls.Add(this.orderRichTextBox);
            this.mainPanel.Controls.Add(this.label3);
            this.mainPanel.Controls.Add(this.quantityNumericUpDown);
            this.mainPanel.Controls.Add(this.modelRichTextBox);
            this.mainPanel.Controls.Add(this.label2);
            this.mainPanel.Controls.Add(this.label1);
            this.mainPanel.Size = new System.Drawing.Size(728, 518);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(126, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Model";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // orderTextBox
            // 
            this.orderTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.orderTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.orderTextBox.Location = new System.Drawing.Point(184, 262);
            this.orderTextBox.MaxLength = 20;
            this.orderTextBox.Name = "orderTextBox";
            this.orderTextBox.Size = new System.Drawing.Size(360, 62);
            this.orderTextBox.TabIndex = 1;
            this.orderTextBox.TextChanged += new System.EventHandler(this.orderTextBox_TextChanged);
            this.orderTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.orderTextBox_KeyUp);
            // 
            // modelComboBox
            // 
            this.modelComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.modelComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.modelComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.modelComboBox.FormattingEnabled = true;
            this.modelComboBox.Location = new System.Drawing.Point(184, 24);
            this.modelComboBox.Name = "modelComboBox";
            this.modelComboBox.Size = new System.Drawing.Size(360, 63);
            this.modelComboBox.TabIndex = 0;
            this.modelComboBox.TextChanged += new System.EventHandler(this.modelComboBox_TextChanged);
            this.modelComboBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.modelComboBox_KeyUp);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(109, 282);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Zlecenie";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // modelRichTextBox
            // 
            this.modelRichTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.modelRichTextBox.BackColor = System.Drawing.Color.LightYellow;
            this.modelRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modelRichTextBox.Location = new System.Drawing.Point(184, 85);
            this.modelRichTextBox.Name = "modelRichTextBox";
            this.modelRichTextBox.ReadOnly = true;
            this.modelRichTextBox.Size = new System.Drawing.Size(360, 151);
            this.modelRichTextBox.TabIndex = 4;
            this.modelRichTextBox.TabStop = false;
            this.modelRichTextBox.Text = "";
            // 
            // quantityNumericUpDown
            // 
            this.quantityNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.quantityNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.quantityNumericUpDown.Location = new System.Drawing.Point(184, 424);
            this.quantityNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.quantityNumericUpDown.Name = "quantityNumericUpDown";
            this.quantityNumericUpDown.Size = new System.Drawing.Size(120, 62);
            this.quantityNumericUpDown.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(90, 442);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ilość paneli";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // orderRichTextBox
            // 
            this.orderRichTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.orderRichTextBox.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.orderRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderRichTextBox.Location = new System.Drawing.Point(184, 322);
            this.orderRichTextBox.Name = "orderRichTextBox";
            this.orderRichTextBox.ReadOnly = true;
            this.orderRichTextBox.Size = new System.Drawing.Size(360, 74);
            this.orderRichTextBox.TabIndex = 7;
            this.orderRichTextBox.TabStop = false;
            this.orderRichTextBox.Text = "Nowe zlecenie";
            // 
            // TestInitDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.ClientSize = new System.Drawing.Size(728, 579);
            this.Name = "TestInitDialog";
            this.Text = "Rozpoczęcie zlecenia";
            this.Load += new System.EventHandler(this.TestInitDialog_Load);
            this.bottomPanel.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox modelComboBox;
        private System.Windows.Forms.TextBox orderTextBox;
        private System.Windows.Forms.RichTextBox orderRichTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown quantityNumericUpDown;
        private System.Windows.Forms.RichTextBox modelRichTextBox;
        private System.Windows.Forms.Label label2;
    }
}
