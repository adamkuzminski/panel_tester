﻿using PanelTester.Common;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class TestMessageDialog : PanelTester.UI.BaseForm
    {
        public TestMessageDialog(string title, string message, MessageType messageType)
        {
            InitializeComponent();

            titleLabel.Text = title;
            messageLabel.Text = message;

            switch (messageType)
            {
                case MessageType.Error:
                    titleLabel.BackColor = Consts.ErrorColor;
                    titleLabel.ForeColor = Color.White;
                    okButton.Text = "OK";
                    cancelButton.Visible = false;
                    break;

                case MessageType.Information:
                    titleLabel.BackColor = Consts.PositiveColor;
                    titleLabel.ForeColor = Color.Black;
                    okButton.Text = "OK";
                    cancelButton.Visible = false;
                    break;

                case MessageType.Question:
                    titleLabel.BackColor = Consts.PositiveColor;
                    titleLabel.ForeColor = Color.Black;
                    okButton.Text = "Tak";
                    cancelButton.Text = "Nie";
                    break;

                case MessageType.Warning:
                    titleLabel.BackColor = Consts.WarningColor;
                    titleLabel.ForeColor = Color.Black;
                    okButton.Text = "OK";
                    cancelButton.Visible = false;
                    break;
            }
        }

        public static DialogResult Show(string title, string message, MessageType messageType)
        {
            TestMessageDialog dlg = new TestMessageDialog(title, message, messageType);
            return dlg.ShowDialog();
        }

        public enum MessageType
        {
            Information,
            Warning,
            Error,
            Question
        }
    }
}