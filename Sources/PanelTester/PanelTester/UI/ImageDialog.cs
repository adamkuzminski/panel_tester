﻿using System;
using System.IO;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class ImageDialog : PanelTester.UI.BaseDialog
    {
        public ImageDialog()
        {
            InitializeComponent();
        }

        public string FileName
        {
            get
            {
                if (pictureBox.Image == null)
                    return "";
                else
                    return pictureBox.ImageLocation;
            }
            set
            {
                if ((value == null) || (value.Length == 0) || !File.Exists(value))
                {
                    pictureBox.Image = null;
                }
                else
                {
                    pictureBox.ImageLocation = value;
                    pictureBox.LoadAsync();
                }
            }
        }

        private void clearImageToolStripButton_Click(object sender, EventArgs e)
        {
            FileName = null;
        }

        private void loadImageToolStripButton_Click(object sender, EventArgs e)
        {
            openFileDialog.FileName = FileName;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileName = openFileDialog.FileName;
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}