﻿using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class InputDialog : PanelTester.UI.BaseDialog
    {
        public InputDialog(string title, string value)
        {
            InitializeComponent();

            label.Text = title;
            textBox.Text = value;
        }

        public static string ShowDialog(string title, string value = "")
        {
            if (value == null)
                value = "";
            InputDialog dlg = new InputDialog(title, value);
            if (dlg.ShowDialog() == DialogResult.OK)
                return dlg.textBox.Text;
            else
                return "";
        }

        protected override bool IsKeyboardRequired()
        {
            return true;
        }

        private void textBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                okButton.PerformClick();
        }
    }
}