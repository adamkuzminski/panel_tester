﻿using PanelTester.Common;
using PanelTester.Data;
using System;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class TestCaseRegistryForm : PanelTester.UI.BaseRegistryForm
    {
        public TestCaseRegistryForm()
        {
            InitializeComponent();
        }

        protected override void AddRecord()
        {
            string model = InputDialog.ShowDialog("Wprowadź model panelu").Trim();
            if (model.Length == 0)
            {
                MessageBox.Show("Nie wprowadzono modelu panelu", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            TestCase refTestCase = Database.GetInstance().FindTestCaseByModel(model);
            TestCase testCase;
            if (refTestCase == null)
            {
                testCase = new TestCase();
                testCase.Model = model;
            }
            else
            {
                if (MessageBox.Show("Znaleziono scenariusz dla podanego modelu.\nCzy chcesz wprowadzić nową wersję?",
                        Application.ProductName,
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.No)
                    return;
                testCase = refTestCase.Copy(false);
            }
            
            AddTestCase(testCase);
        }

        private bool AddTestCase(TestCase testCase)
        {
            TestCaseEditorDialog dlg = new TestCaseEditorDialog(testCase, false);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Database database = Database.GetInstance();
                database.TestCaseList.Add(testCase);
                database.SaveTestCase(testCase);
                RefreshGrid();
                return true;
            }
            return false;
        }

        protected override void CopyRecord()
        {
            if (dataGridView.SelectedRows.Count == 1)
            {
                AddTestCase((dataGridView.SelectedRows[0].DataBoundItem as TestCase).Copy(true));
            }
        }

        protected override void DeleteRecord()
        {            
            if ((dataGridView.SelectedRows.Count > 0) && (MessageBox.Show("Usunięcie scenariusza testowego jest nieodwracalne!\nCzy na pewno usunąć scenariusz?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes))
            {
                Database.GetInstance().DeleteTestCase(dataGridView.SelectedRows[0].DataBoundItem as TestCase);
                RefreshGrid();
            }
        }

        protected override void EditRecord()
        {
            if (dataGridView.SelectedRows.Count == 1)
            {
                DataGridViewRow row = dataGridView.SelectedRows[0];
                int rowIndex = row.Index;
                TestCase testCase = row.DataBoundItem as TestCase;
                TestCaseEditorDialog dlg = new TestCaseEditorDialog(testCase, false);
                Database database = Database.GetInstance();
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    database.SaveTestCase(testCase);
                }
                else
                {
                    database.LoadTestCaseData();
                    RefreshGrid();
                }
            }
        }

        protected override void PreviewRecord()
        {
            if (dataGridView.SelectedRows.Count == 1)
            {
                TestCase testCase = (TestCase)dataGridView.SelectedRows[0].DataBoundItem;
                TestCaseEditorDialog dlg = new TestCaseEditorDialog(testCase, true);
                dlg.ShowDialog();
            }
        }

        protected override void RefreshGrid()
        {
            RefreshGrid(new BindingSource(new SortableBindingList<TestCase>(AppEngine.GetInstance().Database.TestCaseList), null));
        }

       /*private bool ModifyTestCase(string model, TestCase selectedTestCase)
        {
            bool addNew = false;
            TestCase lastTestCase = Database.GetInstance().FindTestCaseByModel(model);
            TestCase testCase = null;
            if (lastTestCase == null)
            {
                testCase = new TestCase();
                testCase.Model = model;
                addNew = true;
            }
            else
            {
                if (lastTestCase.IsLocked())
                {
                    if (MessageBox.Show((selectedTestCase == null) ?
                        "Znaleziono scenariusz dla podanego modelu.\nCzy chcesz wprowadzić nową wersję" :
                        "Nie można edytować scenariusza.\nCzy chcesz wprowadzić nową wersję",
                        Application.ProductName,
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    testCase = testCase.CreateNewVersion(false);
                    addNew = true;
                }
                else
                {
                    if ((selectedTestCase == null) || (selectedTestCase != lastTestCase))
                    {
                        if (MessageBox.Show(
                            "Znaleziono otwarty scenariusz dla podanego modelu.\nCzy chcesz go edytować",
                            Application.ProductName,
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.No)
                            return false;
                        testCase = lastTestCase;
                    }
                    else
                    {
                        testCase = selectedTestCase;
                    }
                }
            }

            TestCaseEditorDialog form = new TestCaseEditorDialog(testCase, false);
            Database database = Database.GetInstance();
            if (form.ShowDialog() == DialogResult.OK)
            {
                if (addNew)
                    database.TestCaseList.Add(testCase);
                database.SaveTestCase(testCase);
                if (addNew)
                    RefreshGrid();
                return true;
            }
            else
            {
                if (!addNew)
                    testCase = database.LoadTestCase(testCase.FileName);
            }

            return false;
        }*/

        private void TestCaseRegistryForm_Load(object sender, EventArgs e)
        {
        }
    }
}