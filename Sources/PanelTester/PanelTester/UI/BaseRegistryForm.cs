﻿using PanelTester.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Linq;

namespace PanelTester.UI
{
    public partial class BaseRegistryForm : BaseForm
    {
        private const int WM_SETREDRAW = 11;

        public BaseRegistryForm()
        {
            InitializeComponent();
        }

        protected virtual void AddRecord()
        {
        }

        protected virtual void CopyRecord()
        {
        }

        protected virtual void DeleteRecord()
        {
        }

        protected virtual void EditRecord()
        {
        }

        protected virtual bool IsCopyAllowed()
        {
            return true;
        }

        protected virtual void PreviewRecord()
        {
        }

        protected virtual void RefreshGrid()
        {
        }

        protected void RefreshGrid(BindingSource bindingSource, int defaultSortColumnIndex = 0, ListSortDirection defaultSortDirection = ListSortDirection.Ascending)
        {
            RefreshGrid(dataGridView, bindingSource, defaultSortColumnIndex, defaultSortDirection);
        }

        protected void RefreshGrid(DataGridView view, BindingSource bindingSource, int defaultSortColumnIndex = 0, ListSortDirection defaultSortDirection = ListSortDirection.Ascending)
        {
            List<string> idList = new List<string>();
            foreach (DataGridViewRow row in view.SelectedRows)
            {
                idList.Add((row.DataBoundItem as BaseEntity).Id);
            }            
                
            DataGridViewColumn sortColumn = view.SortedColumn;
            SortOrder sortOrder = view.SortOrder;
            view.DataSource = bindingSource;
            view.Sort(view.Columns[(sortColumn == null) ? defaultSortColumnIndex : sortColumn.Index], (sortOrder == SortOrder.None) ? defaultSortDirection : ((sortOrder == SortOrder.Ascending) ? ListSortDirection.Ascending : ListSortDirection.Descending));

            foreach (DataGridViewRow row in view.Rows)
            {
                row.Selected = idList.Where(id => (row.DataBoundItem as BaseEntity).Id == id).FirstOrDefault() != null;
            }
        }

        protected void ResumeGrid(DataGridView view)
        {
            SendMessage(view.Handle, WM_SETREDRAW, true, 0);
            view.Refresh();
        }

        protected void SuspendGrid(DataGridView view)
        {
            SendMessage(view.Handle, WM_SETREDRAW, false, 0);
        }

        protected virtual void UpdateToolStripButtons()
        {
            User user = AppEngine.GetInstance().User;
            Role role = (user == null) ? Role.Operator : user.Role;
            int count = dataGridView.SelectedRows.Count;
            bool lockDelete = false;
            bool lockEdit = false;
            foreach (DataGridViewRow row in dataGridView.SelectedRows)
                if (row.DataBoundItem is BaseEntity)
                {
                    BaseEntity entity = row.DataBoundItem as BaseEntity;
                    User owner = (row.DataBoundItem is User) ? (row.DataBoundItem as User) : AppEngine.GetInstance().Database.FindById<User>(entity.AuthorUserId);
                    if ((owner != null) && (owner.Role >= role) && (owner != user))
                    {
                        lockDelete = true;
                        lockEdit = true;
                        break;
                    }
                    else if (entity.IsLocked())
                    {
                        lockDelete = (role < Role.Superadmin);
                        lockEdit = (role < Role.Admin);
                    }
                }

            addToolStripButton.Visible = (ModifyUserRole <= role);
            deleteToolStripButton.Visible = (ModifyUserRole <= role);
            deleteToolStripButton.Enabled = !lockDelete && (count > 0);
            editToolStripButton.Visible = (ModifyUserRole <= role);
            editToolStripButton.Enabled = !lockEdit && (count == 1);
            copyToolStripButton.Visible = (ModifyUserRole <= role) && IsCopyAllowed();
            copyToolStripButton.Enabled = (count == 1);
            previewToolStripButton.Visible = (ReadUserRole <= role);
            previewToolStripButton.Enabled = (count == 1);
        }

        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);

        private void addToolStripButton_Click(object sender, EventArgs e)
        {
            AddRecord();
        }

        private void backToolStripButton_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void BaseRegistryForm_Load(object sender, EventArgs e)
        {
            ReadUserRole = Role.Engineer;
            ModifyUserRole = Role.Admin;

            RefreshGrid();

            UpdateToolStripButtons();
        }

        private void copyToolStripButton_Click(object sender, System.EventArgs e)
        {
            CopyRecord();
        }

        private void dataGridView_DoubleClick(object sender, EventArgs e)
        {
            if (editToolStripButton.Visible && editToolStripButton.Enabled)
                EditRecord();
        }

        private void dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            UpdateToolStripButtons();
        }

        private void deleteToolStripButton_Click(object sender, System.EventArgs e)
        {
            int selectedRows = dataGridView.SelectedRows.Count;
            if ((selectedRows > 0) && (MessageBox.Show((selectedRows > 1) ? "Czy usunąć wybrane wiersze" : "Czy usunąć wybrany wiersz", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
                DeleteRecord();
        }

        private void editToolStripButton_Click(object sender, System.EventArgs e)
        {
            EditRecord();
        }

        private void previewToolStripButton_Click(object sender, EventArgs e)
        {
            PreviewRecord();
        }
    }
}