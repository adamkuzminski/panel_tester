﻿using OpcUaHelper;
using PanelTester.Common;
using PanelTester.Data;
using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class CheckHardwareDialog : PanelTester.UI.BaseDialog
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ValueStatus modbusStatus;

        private PlcEngine plcEngine;

        private ValueStatus plcStatus;

        private ValueStatus printerStatus;

        private ValueStatus[] resistanceStatuses = new ValueStatus[4];

        public CheckHardwareDialog()
        {
            InitializeComponent();
        }

        public TestCase TestCase { get; set; }

        public override bool SaveData(out String invalidField)
        {
            invalidField = "";
            return true;
        }

        private void CheckHardwareDialog_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (plcEngine != null)
                plcEngine.Dispose();
        }

        private void CheckHardwareDialog_Load(object sender, EventArgs e)
        {
            plcEngine = new PlcEngine();

            Task.Run(() =>
            {
                CheckPLC();
                CheckModbus();
                CheckPrinter();
            });

            UpdateButtons();
        }

        private void CheckModbus()
        {
            modbusStatus = ValueStatus.Unknown;
            UpdateButtons();

            new Task(() =>
            {
                try
                {
                    modbusStatus = ValueStatus.Unknown;
                    ModbusEngine modbus = new ModbusEngine(Database.GetInstance().FindById<Board>(TestCase.BoardId));
                    modbusStatus = modbus.Connect() ? ValueStatus.Ready : ValueStatus.NotReady;
                }
                catch
                {
                    modbusStatus = ValueStatus.NotReady;
                }
                UpdateButtons();
            }).Start();
        }

        private void CheckPLC()
        {
            plcStatus = ValueStatus.Unknown;
            UpdateButtons();

            new Task(() =>
            {
                try
                {
                    plcStatus = plcEngine.Connect() ? ValueStatus.Ready : ValueStatus.NotReady;

                    if (plcStatus == ValueStatus.Ready)
                    {
                        OpcUaClient opc = plcEngine.OpcClient;
                        opc.MonitorValue(PlcEngine.GetNodeId(PlcEngine.GetResistanceValue1), new Action<bool, Action>(MonitorResistanceValue1));
                        opc.MonitorValue(PlcEngine.GetNodeId(PlcEngine.GetResistanceValue2), new Action<bool, Action>(MonitorResistanceValue2));
                        opc.MonitorValue(PlcEngine.GetNodeId(PlcEngine.GetResistanceValue3), new Action<bool, Action>(MonitorResistanceValue3));
                        opc.MonitorValue(PlcEngine.GetNodeId(PlcEngine.GetResistanceValue4), new Action<bool, Action>(MonitorResistanceValue4));
                        CheckResistance();
                    }
                }
                catch
                {
                    plcStatus = ValueStatus.NotReady;
                }
                UpdateButtons();
            }).Start();
        }

        private void CheckPrinter()
        {
            printerStatus = ValueStatus.Unknown;
            UpdateButtons();

            new Task(() =>
            {
                try
                {
                    printerStatus = ValueStatus.Unknown;
                    PrinterEngine printer = new PrinterEngine();
                    printerStatus = printer.Connect(false) ? ValueStatus.Ready : ValueStatus.NotReady;
                    if (printerStatus == ValueStatus.Ready)
                        printer.Disconnect();
                }
                catch
                {
                    printerStatus = ValueStatus.NotReady;
                }

                UpdateButtons();
            }).Start();
        }

        private void CheckResistance()
        {
            new Task(() =>
            {
                if (plcStatus == ValueStatus.Ready)
                {
                    UInt16 resistance = (UInt16)(TestCase.Resistance * 6.2);
                    try
                    {
                        plcEngine.WriteAnalogInt(PlcEngine.SetResistanceValue1, resistance);
                        plcEngine.WriteAnalogInt(PlcEngine.SetResistanceValue2, resistance);
                        plcEngine.WriteAnalogInt(PlcEngine.SetResistanceValue3, resistance);
                        plcEngine.WriteAnalogInt(PlcEngine.SetResistanceValue4, resistance);
                    }
                    catch (Exception e)
                    {
                        log.Error("CheckResistance", e);
                    }
                }

                UpdateButtons();
            }).Start();
        }

        private ValueStatus GetResistanceStatus()
        {
            if (plcStatus == ValueStatus.NotReady)
                return ValueStatus.NotReady;

            ValueStatus status = ValueStatus.Unknown;
            for (int i = 0; i < resistanceStatuses.Length; i++)
                if (resistanceStatuses[i] > status)
                    status = resistanceStatuses[i];
            return status;
        }

        private Color GetStatusColor(ValueStatus status)
        {
            switch (status)
            {
                case ValueStatus.NotReady:
                    return Consts.ErrorColor;

                case ValueStatus.Ready:
                    return Consts.PositiveColor;

                case ValueStatus.Unknown:
                default:
                    return Consts.WarningColor;
            }
        }

        private void modbusButton_Click(object sender, EventArgs e)
        {
            CheckModbus();
        }

        private void MonitorResistanceValue1(bool value, Action action)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    MonitorResistanceValue1(value, action);
                }));
                return;
            }
            ValidateResistance(0, value);
        }

        private void MonitorResistanceValue2(bool value, Action action)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    MonitorResistanceValue2(value, action);
                }));
                return;
            }
            ValidateResistance(1, value);
        }

        private void MonitorResistanceValue3(bool value, Action action)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    MonitorResistanceValue3(value, action);
                }));
                return;
            }
            ValidateResistance(2, value);
        }

        private void MonitorResistanceValue4(bool value, Action action)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    MonitorResistanceValue4(value, action);
                }));
                return;
            }
            ValidateResistance(3, value);
        }

        private void plcButton_Click(object sender, EventArgs e)
        {
            CheckPLC();
        }

        private void printerButton_Click(object sender, EventArgs e)
        {
            CheckPrinter();
        }

        private void socketButton_Click(object sender, EventArgs e)
        {
            CheckResistance();
        }

        private void UpdateButtons()
        {
            try
            {
                ValueStatus resStatus = GetResistanceStatus();
                okButton.Enabled = (AppEngine.GetInstance().User.Role > Role.Operator) ||
                    (plcStatus == ValueStatus.Ready) && (modbusStatus == ValueStatus.Ready) && (printerStatus == ValueStatus.Ready) &&
                    (resStatus == ValueStatus.Ready);

                printerButton.BackColor = GetStatusColor(printerStatus);
                modbusButton.BackColor = GetStatusColor(modbusStatus);
                plcButton.BackColor = GetStatusColor(plcStatus);
                socketButton.BackColor = GetStatusColor(GetResistanceStatus());

                res1Label.BackColor = GetStatusColor(resistanceStatuses[0]);
                res2Label.BackColor = GetStatusColor(resistanceStatuses[1]);
                res3Label.BackColor = GetStatusColor(resistanceStatuses[2]);
                res4Label.BackColor = GetStatusColor(resistanceStatuses[3]);

                Invoke((MethodInvoker)delegate
                {
                    if ((plcStatus == ValueStatus.Unknown) || (modbusStatus == ValueStatus.Unknown) || (printerStatus == ValueStatus.Unknown) || (resStatus == ValueStatus.Unknown))
                        infoLabel.Text = "Proszę czekać. Trwa weryfikacja urządzeń";
                    else
                        infoLabel.Text = "Wciśnij odpowiedni przycisk, aby ponownie dokonać weryfikacji";
                });
                Application.DoEvents();
            }
            catch (Exception e)
            {
                log.Error("UpdateButtons", e);
            }
        }

        private void ValidateResistance(int index, bool value)
        {
            /*
            int resistance = TestCase.Resistance;
            int diff = (resistance == 0)? 0 : (int)Math.Abs(100.0 - value * 100.0 / resistance);
            log.Debug(String.Format("ValidateResistance {0}, Value = {1}, Target = {2}, Diff = {3}", index + 1, value, resistance, diff));
            resistanceStatuses[index] = (diff <= 5) ? ValueStatus.Ready : ValueStatus.NotReady;
            */
            resistanceStatuses[index] = value ? ValueStatus.Ready : ValueStatus.NotReady;

            UpdateButtons();
        }
    }
}