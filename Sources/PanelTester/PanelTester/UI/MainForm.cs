﻿using OpcUaHelper;
using PanelTester.Data;
using System;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class MainForm : BaseForm
    {
        private OpcUaClient opcUaClient = new OpcUaClient();

        public MainForm()
        {
            InitializeComponent();
        }

        protected override void ApplyUser()
        {
            User user = AppEngine.GetInstance().User;
            userToolStripDropDownButton.Text = (user == null) ? "" : user.Id + " - " + user.Role.ToString();

            UpdateToolStrip();

            base.ApplyUser();
        }

        private void boardsToolStripButton_Click(object sender, EventArgs e)
        {
            BoardRegistryForm form = new BoardRegistryForm();
            form.ShowDialog();
        }

        private void editorToolStripButton_Click(object sender, EventArgs e)
        {
            TestCaseRegistryForm form = new TestCaseRegistryForm();
            form.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AppEngine.GetInstance().Login();
        }

        private void MainForm_Activated(object sender, EventArgs e)
        {
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Text = Application.ProductName + " v." + Application.ProductVersion;
            AppEngine.GetInstance().CheckLogin();
        }

        private void MonitorTestValue(bool value, Action action)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    MonitorTestValue(value, action);
                }));
                return;
            }

            Console.Out.Write(value.ToString());
        }

        private void reportsToolStripButton_Click(object sender, EventArgs e)
        {
            TestHistoryRegistryForm form = new TestHistoryRegistryForm();
            form.ShowDialog();
        }

        private void settingsToolStripButton_Click(object sender, EventArgs e)
        {
            SettingsDialog dlg = new SettingsDialog();
            dlg.ShowDialog();
        }

        private void startToolStripButton_Click(object sender, EventArgs e)
        {
            TestInitDialog testInitDialog = new TestInitDialog();
            if (testInitDialog.ShowDialog() != DialogResult.OK)
                return;
            CheckHardwareDialog checkDialog = new CheckHardwareDialog();
            checkDialog.TestCase = testInitDialog.TestCase;
            if (checkDialog.ShowDialog() != DialogResult.OK)
                return;
            TestDialog testDialog = new TestDialog();
            testDialog.Order = testInitDialog.Order;
            testDialog.TestCase = testInitDialog.TestCase;
            testDialog.ShowDialog();
        }

        private void UpdateToolStrip()
        {
            User user = AppEngine.GetInstance().User;
            Role role = (user == null) ? Role.Operator : user.Role;

            boardsToolStripButton.Visible = role > Role.Operator;
            editorToolStripButton.Visible = role > Role.Operator;
            settingsToolStripButton.Visible = role > Role.Operator;
            //reportsToolStripButton.Visible
            usersToolStripMenuItem.Visible = role > Role.Engineer;
        }

        private void userProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserDialog dlg = new UserDialog(UserDialog.UserFormMode.Profile, AppEngine.GetInstance().User);
            dlg.ShowDialog();
        }

        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserRegistryForm form = new UserRegistryForm();
            form.ShowDialog();
        }
    }
}