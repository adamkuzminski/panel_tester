﻿namespace PanelTester.UI
{
    partial class SettingsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.printerGroupBox = new System.Windows.Forms.GroupBox();
            this.selectPrinterButton = new System.Windows.Forms.Button();
            this.printerTextBox = new System.Windows.Forms.TextBox();
            this.narrowBarNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.thickBarNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.barcodeHorzOffsetNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.printerDarknessNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.testPrintButton = new System.Windows.Forms.Button();
            this.refreshPrinterButton = new System.Windows.Forms.Button();
            this.printerComboBox = new System.Windows.Forms.ComboBox();
            this.databaseGroupBox = new System.Windows.Forms.GroupBox();
            this.dbPathButton = new System.Windows.Forms.Button();
            this.dbTextBox = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.plcGroupBox = new System.Windows.Forms.GroupBox();
            this.plcTestButton = new System.Windows.Forms.Button();
            this.plcNodeIdTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.plcAddressTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.otherGroupBox = new System.Windows.Forms.GroupBox();
            this.keyboardCheckBox = new System.Windows.Forms.CheckBox();
            this.bottomPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.printerGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.narrowBarNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thickBarNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcodeHorzOffsetNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printerDarknessNumericUpDown)).BeginInit();
            this.databaseGroupBox.SuspendLayout();
            this.plcGroupBox.SuspendLayout();
            this.otherGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(0, 498);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.otherGroupBox);
            this.mainPanel.Controls.Add(this.plcGroupBox);
            this.mainPanel.Controls.Add(this.printerGroupBox);
            this.mainPanel.Controls.Add(this.databaseGroupBox);
            this.mainPanel.Size = new System.Drawing.Size(728, 498);
            // 
            // printerGroupBox
            // 
            this.printerGroupBox.Controls.Add(this.selectPrinterButton);
            this.printerGroupBox.Controls.Add(this.printerTextBox);
            this.printerGroupBox.Controls.Add(this.narrowBarNumericUpDown);
            this.printerGroupBox.Controls.Add(this.label4);
            this.printerGroupBox.Controls.Add(this.thickBarNumericUpDown);
            this.printerGroupBox.Controls.Add(this.label3);
            this.printerGroupBox.Controls.Add(this.barcodeHorzOffsetNumericUpDown);
            this.printerGroupBox.Controls.Add(this.label2);
            this.printerGroupBox.Controls.Add(this.label1);
            this.printerGroupBox.Controls.Add(this.printerDarknessNumericUpDown);
            this.printerGroupBox.Controls.Add(this.testPrintButton);
            this.printerGroupBox.Controls.Add(this.refreshPrinterButton);
            this.printerGroupBox.Controls.Add(this.printerComboBox);
            this.printerGroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.printerGroupBox.Location = new System.Drawing.Point(0, 72);
            this.printerGroupBox.Name = "printerGroupBox";
            this.printerGroupBox.Size = new System.Drawing.Size(728, 206);
            this.printerGroupBox.TabIndex = 1;
            this.printerGroupBox.TabStop = false;
            this.printerGroupBox.Text = "Drukarka";
            // 
            // selectPrinterButton
            // 
            this.selectPrinterButton.Location = new System.Drawing.Point(516, 57);
            this.selectPrinterButton.Name = "selectPrinterButton";
            this.selectPrinterButton.Size = new System.Drawing.Size(100, 28);
            this.selectPrinterButton.TabIndex = 3;
            this.selectPrinterButton.Text = "Wybierz";
            this.selectPrinterButton.UseVisualStyleBackColor = true;
            this.selectPrinterButton.Click += new System.EventHandler(this.selectPrinterButton_Click);
            // 
            // printerTextBox
            // 
            this.printerTextBox.Location = new System.Drawing.Point(13, 26);
            this.printerTextBox.Name = "printerTextBox";
            this.printerTextBox.ReadOnly = true;
            this.printerTextBox.Size = new System.Drawing.Size(497, 26);
            this.printerTextBox.TabIndex = 0;
            // 
            // narrowBarNumericUpDown
            // 
            this.narrowBarNumericUpDown.Location = new System.Drawing.Point(469, 148);
            this.narrowBarNumericUpDown.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.narrowBarNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.narrowBarNumericUpDown.Name = "narrowBarNumericUpDown";
            this.narrowBarNumericUpDown.Size = new System.Drawing.Size(80, 26);
            this.narrowBarNumericUpDown.TabIndex = 8;
            this.narrowBarNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(315, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Cienka linia kodu";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // thickBarNumericUpDown
            // 
            this.thickBarNumericUpDown.Location = new System.Drawing.Point(469, 116);
            this.thickBarNumericUpDown.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.thickBarNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.thickBarNumericUpDown.Name = "thickBarNumericUpDown";
            this.thickBarNumericUpDown.Size = new System.Drawing.Size(80, 26);
            this.thickBarNumericUpDown.TabIndex = 7;
            this.thickBarNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(315, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Gruba linia kodu";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // barcodeHorzOffsetNumericUpDown
            // 
            this.barcodeHorzOffsetNumericUpDown.Location = new System.Drawing.Point(166, 148);
            this.barcodeHorzOffsetNumericUpDown.Maximum = new decimal(new int[] {
            800,
            0,
            0,
            0});
            this.barcodeHorzOffsetNumericUpDown.Name = "barcodeHorzOffsetNumericUpDown";
            this.barcodeHorzOffsetNumericUpDown.Size = new System.Drawing.Size(80, 26);
            this.barcodeHorzOffsetNumericUpDown.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Przesunięcie kodu";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Zaciemnienie";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // printerDarknessNumericUpDown
            // 
            this.printerDarknessNumericUpDown.Location = new System.Drawing.Point(166, 116);
            this.printerDarknessNumericUpDown.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.printerDarknessNumericUpDown.Name = "printerDarknessNumericUpDown";
            this.printerDarknessNumericUpDown.Size = new System.Drawing.Size(80, 26);
            this.printerDarknessNumericUpDown.TabIndex = 5;
            // 
            // testPrintButton
            // 
            this.testPrintButton.Location = new System.Drawing.Point(516, 26);
            this.testPrintButton.Name = "testPrintButton";
            this.testPrintButton.Size = new System.Drawing.Size(200, 28);
            this.testPrintButton.TabIndex = 2;
            this.testPrintButton.Text = "Próbny wydruk";
            this.testPrintButton.UseVisualStyleBackColor = true;
            this.testPrintButton.Click += new System.EventHandler(this.testPrintButton_Click);
            // 
            // refreshPrinterButton
            // 
            this.refreshPrinterButton.Location = new System.Drawing.Point(616, 57);
            this.refreshPrinterButton.Name = "refreshPrinterButton";
            this.refreshPrinterButton.Size = new System.Drawing.Size(100, 28);
            this.refreshPrinterButton.TabIndex = 4;
            this.refreshPrinterButton.Text = "Odśwież";
            this.refreshPrinterButton.UseVisualStyleBackColor = true;
            this.refreshPrinterButton.Click += new System.EventHandler(this.refreshPrinterButton_Click);
            // 
            // printerComboBox
            // 
            this.printerComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.printerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.printerComboBox.FormattingEnabled = true;
            this.printerComboBox.Location = new System.Drawing.Point(13, 58);
            this.printerComboBox.Name = "printerComboBox";
            this.printerComboBox.Size = new System.Drawing.Size(497, 27);
            this.printerComboBox.Sorted = true;
            this.printerComboBox.TabIndex = 1;
            this.printerComboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.printerComboBox_DrawItem);
            // 
            // databaseGroupBox
            // 
            this.databaseGroupBox.Controls.Add(this.dbPathButton);
            this.databaseGroupBox.Controls.Add(this.dbTextBox);
            this.databaseGroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.databaseGroupBox.Location = new System.Drawing.Point(0, 0);
            this.databaseGroupBox.Name = "databaseGroupBox";
            this.databaseGroupBox.Size = new System.Drawing.Size(728, 72);
            this.databaseGroupBox.TabIndex = 0;
            this.databaseGroupBox.TabStop = false;
            this.databaseGroupBox.Text = "Katalog z danymi";
            // 
            // dbPathButton
            // 
            this.dbPathButton.Location = new System.Drawing.Point(616, 25);
            this.dbPathButton.Name = "dbPathButton";
            this.dbPathButton.Size = new System.Drawing.Size(100, 28);
            this.dbPathButton.TabIndex = 1;
            this.dbPathButton.Text = "Zmień";
            this.dbPathButton.UseVisualStyleBackColor = true;
            this.dbPathButton.Click += new System.EventHandler(this.dbPathButton_Click);
            // 
            // dbTextBox
            // 
            this.dbTextBox.Location = new System.Drawing.Point(13, 26);
            this.dbTextBox.Name = "dbTextBox";
            this.dbTextBox.Size = new System.Drawing.Size(597, 26);
            this.dbTextBox.TabIndex = 0;
            // 
            // plcGroupBox
            // 
            this.plcGroupBox.Controls.Add(this.plcTestButton);
            this.plcGroupBox.Controls.Add(this.plcNodeIdTextBox);
            this.plcGroupBox.Controls.Add(this.label6);
            this.plcGroupBox.Controls.Add(this.plcAddressTextBox);
            this.plcGroupBox.Controls.Add(this.label5);
            this.plcGroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.plcGroupBox.Location = new System.Drawing.Point(0, 278);
            this.plcGroupBox.Name = "plcGroupBox";
            this.plcGroupBox.Size = new System.Drawing.Size(728, 109);
            this.plcGroupBox.TabIndex = 2;
            this.plcGroupBox.TabStop = false;
            this.plcGroupBox.Text = "PLC";
            // 
            // plcTestButton
            // 
            this.plcTestButton.Location = new System.Drawing.Point(516, 28);
            this.plcTestButton.Name = "plcTestButton";
            this.plcTestButton.Size = new System.Drawing.Size(100, 28);
            this.plcTestButton.TabIndex = 10;
            this.plcTestButton.Text = "Test";
            this.plcTestButton.UseVisualStyleBackColor = true;
            this.plcTestButton.Click += new System.EventHandler(this.plcTestButton_Click);
            // 
            // plcNodeIdTextBox
            // 
            this.plcNodeIdTextBox.Location = new System.Drawing.Point(96, 61);
            this.plcNodeIdTextBox.Name = "plcNodeIdTextBox";
            this.plcNodeIdTextBox.Size = new System.Drawing.Size(414, 26);
            this.plcNodeIdTextBox.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "NodeId";
            // 
            // plcAddressTextBox
            // 
            this.plcAddressTextBox.Location = new System.Drawing.Point(96, 29);
            this.plcAddressTextBox.Name = "plcAddressTextBox";
            this.plcAddressTextBox.Size = new System.Drawing.Size(414, 26);
            this.plcAddressTextBox.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Adres";
            // 
            // otherGroupBox
            // 
            this.otherGroupBox.Controls.Add(this.keyboardCheckBox);
            this.otherGroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.otherGroupBox.Location = new System.Drawing.Point(0, 387);
            this.otherGroupBox.Name = "otherGroupBox";
            this.otherGroupBox.Size = new System.Drawing.Size(728, 100);
            this.otherGroupBox.TabIndex = 3;
            this.otherGroupBox.TabStop = false;
            this.otherGroupBox.Text = "Inne";
            this.otherGroupBox.Visible = false;
            // 
            // keyboardCheckBox
            // 
            this.keyboardCheckBox.AutoSize = true;
            this.keyboardCheckBox.Location = new System.Drawing.Point(13, 26);
            this.keyboardCheckBox.Name = "keyboardCheckBox";
            this.keyboardCheckBox.Size = new System.Drawing.Size(232, 24);
            this.keyboardCheckBox.TabIndex = 0;
            this.keyboardCheckBox.Text = "Pokazuj klawiaturę ekranową";
            this.keyboardCheckBox.UseVisualStyleBackColor = true;
            // 
            // SettingsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.ClientSize = new System.Drawing.Size(728, 559);
            this.Name = "SettingsDialog";
            this.Text = "Konfiguracja";
            this.Load += new System.EventHandler(this.SettingsDialog_Load);
            this.bottomPanel.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.printerGroupBox.ResumeLayout(false);
            this.printerGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.narrowBarNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thickBarNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcodeHorzOffsetNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printerDarknessNumericUpDown)).EndInit();
            this.databaseGroupBox.ResumeLayout(false);
            this.databaseGroupBox.PerformLayout();
            this.plcGroupBox.ResumeLayout(false);
            this.plcGroupBox.PerformLayout();
            this.otherGroupBox.ResumeLayout(false);
            this.otherGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox printerGroupBox;
        private System.Windows.Forms.Button testPrintButton;
        private System.Windows.Forms.Button refreshPrinterButton;
        private System.Windows.Forms.ComboBox printerComboBox;
        private System.Windows.Forms.GroupBox databaseGroupBox;
        private System.Windows.Forms.Button dbPathButton;
        private System.Windows.Forms.TextBox dbTextBox;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown printerDarknessNumericUpDown;
        private System.Windows.Forms.NumericUpDown narrowBarNumericUpDown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown thickBarNumericUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown barcodeHorzOffsetNumericUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button selectPrinterButton;
        private System.Windows.Forms.TextBox printerTextBox;
        private System.Windows.Forms.GroupBox plcGroupBox;
        private System.Windows.Forms.Button plcTestButton;
        private System.Windows.Forms.TextBox plcNodeIdTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox plcAddressTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox otherGroupBox;
        private System.Windows.Forms.CheckBox keyboardCheckBox;
    }
}
