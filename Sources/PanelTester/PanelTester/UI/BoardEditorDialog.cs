﻿using PanelTester.Common;
using PanelTester.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class BoardEditorDialog : BaseDialog
    {
        private static int[] AnalogInCount = { 25, 11, 11, 18, 8 };
        private static int[] AnalogInOffset = { 0, 25, 36, 47, 65 };
        private static int[] AnalogOutCount = { 4, 2, 4, 6, 2 };
        private static int[] AnalogOutOffset = { 1, 5, 7, 11, 17 };
        private static int[] DigitalInCount = { 5, 5, 5, 7, 0 };
        private static int[] DigitalInOffset = { 0, 5, 10, 15, 22 };
        private static int[] DigitalOutCount = { 25, 8, 8, 17, 10 };
        private static int[] DigitalOutOffset = { 0, 25, 33, 41, 58 };

        private bool enableBoardType = false;

        public BoardEditorDialog(Board board, bool readOnly) : base(readOnly)
        {
            InitializeComponent();
            boardBindingSource.Add(board);

            if (readOnly)
            {
                DisableControls();
                typeComboBox.Enabled = true;
            }
        }

        public override bool SaveData(out string invalidField)
        {
            Board board = GetBoard();
            if ((board.Name == null) || (board.Name.Trim().Length == 0))
            {
                invalidField = ReflectionExtensions.GetPropertyDisplayName<Board>(i => i.Name);
                return false;
            }

            IPAddress ipAddress;
            if ((board.AddressIP == null) || !IPAddress.TryParse(board.AddressIP, out ipAddress))
            {
                invalidField = ReflectionExtensions.GetPropertyDisplayName<Board>(i => i.AddressIP);
                return false;
            }

            //board.AddressIP = ipAddress.ToString();
            //board.Comment = commentTextBox.Text.Trim();
            //board.IsActive = activeCheckBox.Checked;
            //board.RegistrySetList = registrySetList;

            invalidField = "";
            return true;
        }

        protected override bool IsKeyboardRequired()
        {
            return true;
        }

        private void BoardEditorDialog_Load(object sender, EventArgs e)
        {
            typeComboBox.SelectedIndex = 0;
            boardTypeComboBox.DataSource = Enum.GetValues(typeof(BoardType));
            boardTypeComboBox.SelectedItem = GetBoard().Type;
            enableBoardType = true;
            testButton.Visible = false;// AppEngine.GetInstance().User.Role == Role.Superadmin;
            testInputColumn.Visible = testButton.Visible;
            testResultColumn.Visible = testButton.Visible;
        }

        private void boardTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (enableBoardType)
            {
                BoardType boardType = (BoardType)boardTypeComboBox.SelectedItem;
                if ((boardType != BoardType.Unknown) && (MessageBox.Show("Czy wprowadzić domyślną konfigurację wejść i wyjść?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
                {
                    for (int i = 0; i < (int)BoardRegistryType.Count; i++)
                    {
                        BoardRegistryType registryType = (BoardRegistryType)i;
                        BoardRegistrySet registrySet = GetBoard().RegistrySetList[i];
                        registrySet.RegistryList.Clear();

                        registrySet.Offset = GetBoardRegistryOffset(boardType, registryType);
                        for (int j = 0; j < GetBoardRegistryCount(boardType, registryType); j++)
                        {
                            BoardRegistry item = new BoardRegistry(j + 1, registryType);
                            item.SetSymbol(registrySet.Offset);
                            registrySet.RegistryList.Add(item);
                        }
                    }
                    GetBoard().RegistryValue = (byte)boardType;
                    registryValueNumericUpDown.Value = (byte)boardType;

                    RefreshRegistryData();
                }
            }
        }

        private void countNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            BoardRegistryType type = GetRegistryType();
            if (type == BoardRegistryType.Unknown)
                return;
            BoardRegistrySet registrySet = GetBoard().RegistrySetList[(int)type];
            List<BoardRegistry> list = registrySet.RegistryList;

            int count = (int)countNumericUpDown.Value;
            if (list.Count == count)
                return;

            if (list.Count < count)
            {
                int offset = GetBoard().RegistrySetList[(int)type].Offset;
                for (int i = list.Count; i < count; i++)
                {
                    BoardRegistry item = new BoardRegistry(i + 1, type);
                    item.SetSymbol(offset);
                    list.Add(item);
                }
            }
            else
            {
                for (int i = list.Count - 1; i >= count; i--)
                    list.RemoveAt(i);
            }

            RefreshRegistryData();
        }

        private Board GetBoard()
        {
            return (Board)boardBindingSource[0];
        }

        private int GetBoardRegistryCount(BoardType boardType, BoardRegistryType registryType)
        {
            int index = (int)boardType - 1;
            if (index < 0)
                return 0;

            switch (registryType)
            {
                case BoardRegistryType.AIn:
                    return AnalogInCount[index];

                case BoardRegistryType.AOut:
                    return AnalogOutCount[index];

                case BoardRegistryType.DIn:
                    return DigitalInCount[index];

                case BoardRegistryType.DOut:
                    return DigitalOutCount[index];

                default:
                    return 0;
            }
        }

        private int GetBoardRegistryOffset(BoardType boardType, BoardRegistryType registryType)
        {
            int index = (int)boardType - 1;
            if (index < 0)
                return 0;

            switch (registryType)
            {
                case BoardRegistryType.AIn:
                    return AnalogInOffset[index];

                case BoardRegistryType.AOut:
                    return AnalogOutOffset[index];

                case BoardRegistryType.DIn:
                    return DigitalInOffset[index];

                case BoardRegistryType.DOut:
                    return DigitalOutOffset[index];

                default:
                    return 0;
            }
        }

        private BoardRegistryType GetRegistryType()
        {
            return (BoardRegistryType)typeComboBox.SelectedIndex;
        }

        private void offsetNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            BoardRegistryType type = GetRegistryType();
            if (type == BoardRegistryType.Unknown)
                return;
            BoardRegistrySet registrySet = GetBoard().RegistrySetList[(int)type];
            registrySet.Offset = (int)offsetNumericUpDown.Value;
            foreach (BoardRegistry boardRegistry in registrySet.RegistryList)
            {
                boardRegistry.SetSymbol(registrySet.Offset);
            }

            RefreshRegistryData();
        }

        private void RefreshRegistryData()
        {
            BoardRegistryType registryType = GetRegistryType();
            if (registryType == BoardRegistryType.Unknown)
            {
                countNumericUpDown.Value = 0;
                offsetNumericUpDown.Value = 0;
                countNumericUpDown.Enabled = false;
                offsetNumericUpDown.Enabled = false;
                dataGridView.DataSource = null;
            }
            else
            {
                BoardRegistrySet registrySet = GetBoard().RegistrySetList[(int)registryType];
                List<BoardRegistry> list = registrySet.RegistryList;
                countNumericUpDown.Value = list.Count;
                offsetNumericUpDown.Value = registrySet.Offset;
                countNumericUpDown.Enabled = !readOnly;
                offsetNumericUpDown.Enabled = !readOnly;
                var bindingList = new BindingList<BoardRegistry>(list);
                boardRegistryBindingSource = new BindingSource(bindingList, null);
                dataGridView.DataSource = boardRegistryBindingSource;
            }
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            using (ModbusEngine modbusEngine = new ModbusEngine(GetBoard()))
            {
                bool result = true;
                try
                {
                    do
                    {
                        if (!modbusEngine.Connect())
                        {
                            MessageBox.Show(modbusEngine.LastErrorMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            result = false;
                            break;
                        }

                        BoardRegistryType registryType = GetRegistryType();
                        BoardRegistrySet registrySet = GetBoard().RegistrySetList[(int)registryType];
                        for (int i = 0; i < registrySet.RegistryList.Count; i++)
                        {
                            DataGridViewRow row = dataGridView.Rows[i];
                            string data = row.Cells[2].Value as string;
                            try
                            {
                                switch (registryType)
                                {
                                    case BoardRegistryType.DOut:
                                        if ((data != null) && (data.Length > 0))
                                        {
                                            bool value = Utils.StringToBoolean(row.Cells[2].Value as string);
                                            row.Cells[2].Value = Utils.BooleanToInt(value);
                                            modbusEngine.WriteDigital(i, value);
                                        }
                                        row.Cells[3].Value = Utils.BooleanToInt(modbusEngine.ReadDigital(i, registryType));
                                        break;

                                    case BoardRegistryType.DIn:
                                        row.Cells[2].Value = "Tylko do odczytu";
                                        row.Cells[3].Value = Utils.BooleanToInt(modbusEngine.ReadDigital(i, registryType));
                                        break;

                                    case BoardRegistryType.AOut:
                                        if ((data != null) && (data.Length > 0))
                                        {
                                            ushort value;
                                            if (!UInt16.TryParse(data, out value))
                                                value = 0;
                                            row.Cells[2].Value = value;
                                            modbusEngine.WriteAnalog(i, value);
                                        }
                                        row.Cells[3].Value = modbusEngine.ReadAnalog(i, registryType);
                                        break;

                                    case BoardRegistryType.AIn:
                                        row.Cells[2].Value = "Tylko do odczytu";
                                        row.Cells[3].Value = modbusEngine.ReadAnalog(i, registryType);
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {
                                row.Cells[3].Value = ex.Message;
                                result = false;
                            }
                        }
                    }
                    while (false);

                    if (result)
                        MessageBox.Show("Test zakończył się prawidłowo", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Wystąpiły błędy podczas testu", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void typeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshRegistryData();
        }
    }
}