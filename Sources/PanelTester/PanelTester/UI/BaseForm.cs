﻿using PanelTester.Common;
using PanelTester.Data;
using System.Drawing;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class BaseForm : Form
    {
        public static Role ReadUserRole { get; set; } = Role.Superadmin;
        public static Role ModifyUserRole { get; set; } = Role.Superadmin;

        public BaseForm()
        {
            InitializeComponent();
        }

        public void OnUserChanged(User user)
        {
            ApplyUser();
        }

        private void BaseForm_Load(object sender, System.EventArgs e)
        {
            ApplyUser();
        }

        protected virtual void ApplyUser()
        {
            Color color = SystemColors.Control;
            User user = AppEngine.GetInstance().User;
            if (user != null)
            {
                switch (user.Role)
                {
                    case Role.Admin:
                        color = Consts.AdminBackColor;
                        break;

                    case Role.Superadmin:
                        color = Consts.SuperadminBackColor;
                        break;

                    case Role.Engineer:
                        color = Consts.EngineerBackColor;
                        break;

                    case Role.Operator:
                        color = Consts.OperatorBackColor;
                        break;
                }
            }

            foreach (Control control in Controls)
            {
                if (control is ToolStrip)
                    control.BackColor = color;
            }
        }
    }
}