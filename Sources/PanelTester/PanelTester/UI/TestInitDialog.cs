﻿using PanelTester.Common;
using PanelTester.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class TestInitDialog : PanelTester.UI.BaseDialog
    {
        private Dictionary<string, TestCase> testCaseMap;

        public TestInitDialog()
        {
            InitializeComponent();
        }

        public Order Order { get; set; }

        public TestCase TestCase { get; set; }

        public override bool SaveData(out String invalidField)
        {
            invalidField = "";

            TestCase testCase = GetTestCase();
            if (testCase == null)
            {
                invalidField = "Model";
                return false;
            }

            if (testCase.GetBoard() == null)
            {
                invalidField = "Nieznana płyta główna";
                return false;
            }

            string orderNumber = orderTextBox.Text.Trim();
            if (orderNumber.Length == 0)
            {
                invalidField = "Numer zlecenia";
                return false;
            }

            Order order = GetOrder();
            if (order != null)
            {
                if (!order.AuthorUserId.Equals(AppEngine.GetInstance().User.Id))
                {
                    invalidField = "Numer zlecenia";
                    return false;
                }
                if (order.TestCaseId != testCase.Id)
                {
                    invalidField = "Zmień model lub numer zlecenia";
                    return false;
                }
            }

            int declaredQuantity = (int)quantityNumericUpDown.Value;
            if (declaredQuantity <= 0)
            {
                invalidField = "Deklarowana ilość paneli";
                return false;
            }

            //Save data
            if (order == null)
            {
                Database database = Database.GetInstance();
                order = new Order
                {
                    BoardId = testCase.BoardId,
                    BoardName = testCase.BoardName,
                    BoardType = testCase.GetBoard().Type,
                    DeclaredQuantity = declaredQuantity,
                    Model = testCase.Model,
                    ModelDocNumber = testCase.ModelDocNumber,
                    OrderNumber = orderNumber,
                    ProductName = testCase.ProductName,
                    StartDate = DateTime.Now,
                    TestCaseId = testCase.Id,
                    TestCaseName = testCase.Name,
                    TestCaseVersion = testCase.Version
                };
                database.OrderList.Add(order);
                database.SaveOrder(order);
            }

            Order = order;
            TestCase = testCase;
            return true;
        }

        protected override bool IsKeyboardRequired()
        {
            return true;
        }

        private Order GetOrder()
        {
            string orderNumber = orderTextBox.Text.Trim();
            foreach (Order order in Database.GetInstance().OrderList)
                if (order.OrderNumber.Equals(orderNumber, StringComparison.CurrentCultureIgnoreCase))
                    return order;

            return null;
        }

        private TestCase GetTestCase()
        {
            if (testCaseMap.TryGetValue(modelComboBox.Text, out TestCase testCase))
                return testCase;
            else
                return null;
        }

        private void modelComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                foreach (string model in modelComboBox.Items)
                {
                    if (model.Equals(modelComboBox.Text))
                    {
                        modelComboBox.SelectedItem = model;
                        orderTextBox.Focus();
                        break;
                    }
                }
            }
        }

        private void modelComboBox_TextChanged(object sender, EventArgs e)
        {
            modelRichTextBox.Clear();

            TestCase testCase = GetTestCase();
            if (testCase == null)
            {
                modelComboBox.ForeColor = Consts.ErrorColor;
            }
            else
            {
                modelComboBox.ForeColor = Color.Black;
                modelRichTextBox.Text =
                    String.Format(
                        "Scenariusz: {0}\r\nWersja: {1}\r\nNumer dokumentacji: {2}\r\nNazwa produktu: {3}\r\nPłyta główna: {4}\r\nAutor: {5} [{6}]\r\nKomentarz: {7}",
                        testCase.Name,
                        testCase.Version,
                        testCase.ModelDocNumber,
                        testCase.ProductName,
                        testCase.BoardName,
                        testCase.Author, testCase.AuthorUserId,
                        testCase.Comment);
            }

            UpdateOrderNumber();
        }

        private void orderTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (quantityNumericUpDown.Enabled)
                    quantityNumericUpDown.Focus();
                else
                    okButton.PerformClick();
            }
        }

        private void orderTextBox_TextChanged(object sender, EventArgs e)
        {
            UpdateOrderNumber();
        }

        private void TestInitDialog_Load(object sender, EventArgs e)
        {
            Database database = Database.GetInstance();

            //Models
            testCaseMap = new Dictionary<string, TestCase>();

            foreach (TestCase testCase in database.TestCaseList)
            {
                if (!testCase.IsActive)
                    continue;
                if (testCaseMap.ContainsKey(testCase.Model))
                {
                    if (testCaseMap[testCase.Model].Version < testCase.Version)
                        testCaseMap[testCase.Model] = testCase;
                }
                else
                {
                    testCaseMap.Add(testCase.Model, testCase);
                }
            }

            var testCaseList = testCaseMap.ToList();
            testCaseList.Sort((tc1, tc2) => tc1.Value.Model.CompareTo(tc2.Value.Model));
            foreach (KeyValuePair<string, TestCase> kv in testCaseList)
                modelComboBox.Items.Add(kv.Key);

            UpdateOrderNumber();
        }

        private void UpdateOrderNumber()
        {
            orderRichTextBox.Clear();

            Order order = GetOrder();
            if (order == null)
            {
                if (orderTextBox.Text.Trim().Length == 0)
                {
                    orderRichTextBox.BackColor = Consts.WarningColor;
                    orderRichTextBox.Text = "Wprowadź numer zlecenia";
                }
                else
                {
                    orderRichTextBox.BackColor = Consts.PositiveColor;
                    orderRichTextBox.Text = "Nowe zlecenie";
                }
                quantityNumericUpDown.Enabled = true;
            }
            else
            {
                quantityNumericUpDown.Enabled = false;
                quantityNumericUpDown.Value = order.DeclaredQuantity;

                TestCase testCase = GetTestCase();
                User user = AppEngine.GetInstance().User;
                if (order.AuthorUserId.Equals(user.Id))
                {
                    if ((testCase == null) || (testCase.Id == order.TestCaseId))
                    {
                        orderRichTextBox.BackColor = Consts.InfoColor;
                        orderRichTextBox.Text = String.Format("Kontynuacja zlecenia z dnia {0}", order.StartDate.ToLongDateString());
                    }
                    else
                    {
                        orderRichTextBox.BackColor = Consts.WarningColor;
                        orderRichTextBox.Text = "Nie można kontynuować zlecenia\r\ndla innego scenariusza testowego!";
                    }
                }
                else
                {
                    orderRichTextBox.BackColor = Consts.WarningColor;
                    orderRichTextBox.Text = "Nie można kontynuować zlecenia\r\ninnego użytkownika!";
                }
            }
        }
    }
}