﻿using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;

namespace PanelTester.UI
{
    [Designer(typeof(ComponentDesigner))]
    public class InheritedToolStripButton : ToolStripButton { }
}