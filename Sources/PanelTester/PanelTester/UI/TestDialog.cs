﻿using OpcUaHelper;
using PanelTester.Common;
using PanelTester.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class TestDialog : BaseForm
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private bool abortTest;
        private int currentPowerModeIndex;
        private int currentStepIndex;
        private int infoColumnIndex;
        private List<string> infoPictureList;
        private int infoPictureListIndex;
        private int infoRowIndex;
        private ModbusEngine modbusEngine;
        private OrderTestResult orderTestResult;
        private PanelTestResult panelTestResult;
        private PlcEngine plcEngine;
        private bool[] powerOn = new bool[3];
        private SocketPin[] powerPins = new SocketPin[3];
        private PrinterEngine printerEngine;
        private bool skipBoardInit;
        private TestMode testMode;
        private bool[] usedOutputs;

        public TestDialog()
        {
            InitializeComponent();
        }

        private enum Power
        {
            L1 = 0,
            L2,
            L3
        }

        private enum TestMode
        {
            Idle = 0,
            SetupPanel,
            TestGND,

            InitBoard,
            Test,

            Print,
            Result
        }

        public Order Order { get; set; }

        public TestCase TestCase { get; set; }

        private void AbortTest(bool setFlag = true)
        {
            if ((testMode > TestMode.Idle) && (testMode < TestMode.Print))
            {
                if (setFlag)
                    abortTest = true;
                stopButton.Text = "Proszę czekać...";
                if (testMode != TestMode.Test)
                    SetPanelTestResult(false);
            }
        }

        private bool CheckInput(int inputIndex, TestCaseStepInput input, TestCaseStepInputItem item, ref PowerModeTestResult powerModeTestResult)
        {
            PowerModeItemTestResult itemTestResult = new PowerModeItemTestResult();
            powerModeTestResult.ItemList.Add(itemTestResult);
            itemTestResult.InputIndex = inputIndex;
            string expectedValue = "?";

            try
            {                
                if (input.Logic)
                {
                    bool value;
                    string varName;
                    if (input.Socket)
                    {
                        varName = GetSocketNodeId(input.Number - 1, (SocketType)input.ParentType);
                        value = plcEngine.ReadDigital(varName);
                    }
                    else
                    {
                        varName = "";
                        value = modbusEngine.ReadDigital(input.Number - 1, (BoardRegistryType)input.ParentType);
                    }

                    expectedValue = (item.MaxValue > 0) ? "1" : "0";
                    itemTestResult.Result = value == (item.MaxValue > 0);
                    itemTestResult.Value = Utils.BooleanToInt(value);

                    if (!itemTestResult.Result)
                        log.WarnFormat("====> CHECK DIGITAL INPUT {0}{1} FAILED: {2} <> {3}", input.Label, (varName.Length > 0)? ", " + varName : "", item.MaxValue > 0, value);
                }
                else
                {
                    double value;
                    string varName;
                    if (input.Socket)
                    {
                        varName = GetSocketNodeId(input.Number - 1, (SocketType)input.ParentType);
                        value = plcEngine.ReadAnalogDouble(varName, input.RangeMax);
                    }
                    else
                    {
                        varName = "";
                        value = modbusEngine.ReadAnalog(input.Number - 1, (BoardRegistryType)input.ParentType);
                    }
                    itemTestResult.Value = input.Integer ? (int)value : value;
                    itemTestResult.Result = (item.MinValue <= value) && (value <= item.MaxValue);
                    expectedValue = string.Format("{0} - {1}", item.MinValue, item.MaxValue);
                    if (!itemTestResult.Result)
                        log.WarnFormat("====> CHECK ANALOG INPUT {0}{1} FAILED: {2} <= {3} <= {4}", input.Label, (varName.Length > 0) ? ", " + varName : "", item.MinValue, value, item.MaxValue);
                }
            }
            catch (Exception e)
            {
                log.Error("CheckInput", e);
                itemTestResult.Result = false;
            }

            if (itemTestResult.Result)
            {
                if (powerModeTestResult.Result != TestResult.Error)
                    powerModeTestResult.Result = TestResult.OK;
                itemTestResult.Info = string.Format("Wartość wejścia {0} prawidłowa. Oczekiwana wartość: {1}, odczytana: {2}", input.Label, expectedValue, itemTestResult.Value);
            }
            else
            {
                string errorMessage = Utils.GetString(item.ErrorMessage);
                if (errorMessage.Length > 0)
                    errorMessage = ". " + errorMessage;
                itemTestResult.Info = string.Format("Błąd wejścia {0}. Oczekiwana wartość: {1}, odczytana: {2}{3}", input.Label, expectedValue, itemTestResult.Value, errorMessage);
                itemTestResult.ErrorImage = item.ErrorImage;
                powerModeTestResult.AddWrongInput(input.Label);
                powerModeTestResult.AddErrorImage(itemTestResult.ErrorImage);
                powerModeTestResult.Result = TestResult.Error;
            }
            powerModeTestResult.AddInfoText(itemTestResult.Info);

            return itemTestResult.Result;
        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ShowCellInfo(e.RowIndex, e.ColumnIndex);
        }

        private void dataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if ((e.RowIndex >= 0) && (e.ColumnIndex == 0))
                e.Value = (int)e.Value + 1;
        }

        private void dataGridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                TestStatus testStatus;
                TestResult testResult;
                StepTestResult stepTestResult = dataGridView.Rows[e.RowIndex].DataBoundItem as StepTestResult;

                if (e.ColumnIndex > 0)
                {
                    PowerModeTestResult powerModeTestResult = stepTestResult.PowerModeTestResultList[e.ColumnIndex - 1];
                    testStatus = powerModeTestResult.GetTestStatus();
                    testResult = powerModeTestResult.Result;
                }
                else
                {
                    testStatus = stepTestResult.GetTestStatus();
                    testResult = stepTestResult.Result;
                }

                Color color;
                switch (testStatus)
                {
                    case TestStatus.NotTested:
                    default:
                        color = Consts.NotTestedColor;
                        break;

                    case TestStatus.Testing:
                        color = Consts.TestingColor;
                        break;

                    case TestStatus.Tested:
                        switch (testResult)
                        {
                            case TestResult.OK:
                            default:
                                color = Consts.PositiveColor;
                                break;

                            case TestResult.Error:
                                color = Consts.ErrorColor;
                                break;
                        }
                        break;
                }

                using (Brush backColorBrush = new SolidBrush(color))
                {
                    e.Graphics.FillRectangle(backColorBrush, e.CellBounds);
                    if (e.ColumnIndex == 0)
                        e.Graphics.DrawString((string)e.FormattedValue, dataGridView.Font, new SolidBrush(Color.Black), e.CellBounds);
                    e.Handled = true;
                }
            }
        }

        private void debug1Button_Click(object sender, EventArgs e)
        {
            if (testMode == TestMode.InitBoard)
                skipBoardInit = true;
            else
                SetNextTestMode();
        }

        private void debug2Button_Click(object sender, EventArgs e)
        {
            TestNextPowerMode();
        }

        private async void DoTest()
        {
            dataGridView.Refresh();
            await Task.Run(() =>
            {
                TestCaseStep step = GetCurrentStep();
                StepTestResult stepTestResult = panelTestResult.StepTestResultList[currentStepIndex];

                PowerModeTestResult powerModeTestResult = stepTestResult.PowerModeTestResultList[currentPowerModeIndex];
                powerModeTestResult.StartDate = DateTime.Now;
                powerModeTestResult.Result = TestResult.Unknown;

                log.Debug("Test kroku: " + currentStepIndex + 1);

                do
                {
                    if (abortTest)
                        break;
                    if (currentPowerModeIndex == 0)
                    {
                        //Outputs - variables only
                        //modbusEngine.ResetOutputCache();
                        for (int i = 0; i < step.OutputList.Count; i++)
                        {
                            if (abortTest)
                                break;
                            TestCaseStepOutput output = step.OutputList[i];
                            if (output.InUse)
                            {
                                SetOutput(output, true);
                                usedOutputs[i] = true;
                            }
                            else
                            {
                                //Disable previous output
                                if (usedOutputs[i])
                                    SetOutput(output, false);
                            }
                        }
                        //modbusEngine.Write();
                        stepTestResult.StartDate = DateTime.Now;
                    }

                    if (abortTest)
                        break;

                    //Set power
                    int currentPowerSet = (int)typeof(PowerMode).GetMember(TestCase.PowerModeList[currentPowerModeIndex].ToString()).FirstOrDefault().GetCustomAttribute<DefaultValueAttribute>().Value;
                    int mask = 1;
                    for (int i = 0; i < 3; i++)
                    {
                        if (abortTest)
                            break;

                        bool turnOn = ((currentPowerSet & mask) > 0);
                        if (turnOn ^ powerOn[i])
                            SetPower(i, turnOn);
                        mask = mask << 1;
                    }

                    if (abortTest)
                        break;

                    //Read inputs
                    Invoke((MethodInvoker)delegate
                    {
                        dataGridView.Refresh();
                        if ((infoRowIndex == currentStepIndex) && ((infoColumnIndex == 0) || (infoColumnIndex == currentPowerModeIndex + 1)))
                            RefreshCellInfo();

                        DateTime refTime = DateTime.Now;
                        int delta;
                        progressBar.Maximum = (int)(step.Delay * 0.01);
                        progressBar.Value = 0;
                        progressBar.Visible = true;
                        while (!abortTest && ((delta = (int)(DateTime.Now - refTime).TotalMilliseconds) < step.Delay))
                        {
                            progressBar.Value = (int)(delta * 0.01);
                            Thread.Sleep(100);
                            Application.DoEvents();
                        }
                        progressBar.Visible = false;
                    });

                    if (abortTest)
                        break;

                    //modbusEngine.Read();

                    for (int i = 0; i < step.InputList.Count; i++)
                    {
                        if (abortTest)
                            break;

                        TestCaseStepInput input = step.InputList[i];
                        TestCaseStepInputItem item = input.ItemList[currentPowerModeIndex];
                        if (input.GetInUse(currentPowerModeIndex) && !CheckInput(i, input, item, ref powerModeTestResult) && item.IsFatalError)
                            break;
                    }
                }
                while (false);

                if (abortTest)
                {
                    powerModeTestResult.AddInfoText("Test przerwany");
                    powerModeTestResult.Result = TestResult.Error;
                }

                DateTime now = DateTime.Now;
                powerModeTestResult.EndDate = now;

                if (powerModeTestResult.Result == TestResult.Error)
                {
                    //Stop tests - NEGATIVE result
                    powerModeTestResult.AddErrorImage(step.ErrorImage, true);
                    powerModeTestResult.AddInfoText(step.ErrorMessage, true);

                    Invoke((MethodInvoker)delegate
                    {
                        ShowCellInfo(currentStepIndex, currentPowerModeIndex + 1);
                    });

                    currentPowerModeIndex++;
                    stepTestResult.Result = TestResult.Error;
                    stepTestResult.EndDate = now;
                    //panelTestResult.ErrorMessage = string.Join("; ", powerModeTestResult.InfoList);                    
                }  
                else if (powerModeTestResult.Result == TestResult.Unknown)
                {
                    powerModeTestResult.Result = TestResult.OK;
                    log.Debug("Unknown test result");
                }

                if (powerModeTestResult.InfoList != null)
                    panelTestResult.Message = string.Join("| ", powerModeTestResult.InfoList);
                if (powerModeTestResult.WrongInputList != null)
                    panelTestResult.Errors = string.Join("; ", powerModeTestResult.WrongInputList);

                Invoke((MethodInvoker)delegate
                {
                    dataGridView.Refresh();

                    if (powerModeTestResult.Result == TestResult.Error)
                    {
                        ShowCellInfo(currentStepIndex, 0);
                        SetPanelTestResult(false);
                    }
                    else
                        TestNextPowerMode();
                });
            });
        }

        private void EndOrder()
        {
            Order.EndDate = DateTime.Now;
            Database.GetInstance().SaveOrder(Order);
            DialogResult = DialogResult.OK;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            EndOrder();
        }

        private Board GetBoard()
        {
            return TestCase.GetBoard();
        }

        private TestCaseStep GetCurrentStep()
        {
            if ((currentStepIndex >= 0) && (currentStepIndex < TestCase.StepList.Count))
                return TestCase.StepList[currentStepIndex];
            else
                return null;
        }

        private string GetSocketNodeId(int index, SocketType type)
        {
            try
            {
                return Socket.GetSocketPins(type)[index].NodeId;
            }
            catch (Exception e)
            {
                log.Error("GetSocketNodeId", e);
            }
            return "";
        }

        private void MonitorSafetyValue(bool value, Action action)
        {
            try
            {
                if (InvokeRequired)
                {
                    Invoke(new Action(() =>
                    {
                        MonitorSafetyValue(value, action);
                    }));
                    return;
                }
            }
            catch (Exception e)
            {
                log.Error("MonitorSafetyValue", e);
            }

            log.Debug("MonitorSaferyValue: " + value.ToString());
            labelSafetyInfo.BackColor = value ? Consts.ErrorColor : Consts.PositiveColor;
            labelSafetyInfo.ForeColor = value ? Color.White : Color.Black;
            if (value)
                AbortTest();
        }

        private void MonitorStartValue(bool value, Action action)
        {
            try
            {
                if (InvokeRequired)
                {
                    Invoke(new Action(() =>
                    {
                        MonitorStartValue(value, action);
                    }));
                    return;
                }
            }
            catch (Exception e)
            {
                log.Error("MonitorStartValue", e);
            }

            log.Debug("MonitorStartValue: " + value.ToString());
            if (value)
            {
                if (testMode == TestMode.SetupPanel)
                    SetNextTestMode();
                else
                    log.Warn("MonitorStartValue - wrong mode: " + testMode.ToString());
            }
        }

        private void MonitorStopValue(bool value, Action action)
        {
            try
            {
                if (InvokeRequired)
                {
                    Invoke(new Action(() =>
                    {
                        MonitorStopValue(value, action);
                    }));
                    return;
                }
            }
            catch (Exception e)
            {
                log.Error("MonitorStopValue", e);
            }

            log.Debug("MonitorStopValue: " + value.ToString());
            if (value)
            {
                AbortTest();
            }
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            StartNewPanel(false);
        }

        private void newTestToolStripButton_Click(object sender, EventArgs e)
        {
            StartNewPanel(false);
        }

        private void nextImageButton_Click(object sender, EventArgs e)
        {
            ShowInfoPicture(infoPictureListIndex + 1);
        }

        private void prevImageButton_Click(object sender, EventArgs e)
        {
            ShowInfoPicture(infoPictureListIndex - 1);
        }

        private void printButton_Click(object sender, EventArgs e)
        {
            SetTestMode(TestMode.Print);
        }

        private void RefreshCellInfo()
        {
            ShowCellInfo(infoRowIndex, infoColumnIndex);
        }

        private void rejectButton_Click(object sender, EventArgs e)
        {
            StartNewPanel(false);
        }

        private void repeatButton_Click(object sender, EventArgs e)
        {
            StartNewPanel(true);
        }

        private void Reset(bool resetModbus)
        {
            log.Debug("Reset begin");
            //Power
            for (int i = 0; i < powerPins.Length; i++)
                SetPower(i, false);

            //Cont test
            //plcEngine.WriteDigital(Socket.GetSocketPinByType(SocketPinType.OutN).NodeId, false);
            plcEngine.WriteDigital(Socket.GetSocketPinByType(SocketPinType.OutPE).NodeId, false);

            //Reset outputs
            foreach (SocketType socketType in new SocketType[] { SocketType.AOut, SocketType.DOut })
                foreach (SocketPin pin in Socket.GetSocketPins(socketType))
                    if (pin.IsVariable())
                    {
                        if (pin.IsLogic())
                            plcEngine.WriteDigital(pin.NodeId, false);
                        else
                            plcEngine.WriteAnalogInt(pin.NodeId, 0);
                    }

            if (resetModbus)
            {
                modbusEngine.ResetAnalog(GetBoard().RegistrySetList[(int)BoardRegistryType.AOut].RegistryList.Count, GetBoard().RegistrySetList[(int)BoardRegistryType.AOut].Offset);
                modbusEngine.ResetDigital(GetBoard().RegistrySetList[(int)BoardRegistryType.DOut].RegistryList.Count, GetBoard().RegistrySetList[(int)BoardRegistryType.DOut].Offset);
            }

            log.Debug("Reset end");
        }

        private void SetInfoText(string info)
        {
            SetInfoText(info, Color.Black, Color.White);
        }

        private void SetInfoText(string info, Color foreColor, Color backColor)
        {
            if (info.Length > 0)
            {
                String[] lines = info.Split('\n');
                if (lines.Length == 1)
                    info = Environment.NewLine + info;
                else
                    info = string.Join(Environment.NewLine, lines);
            }

            infoTextBox.Text = info;
            topInfoPanel.BackColor = backColor;
            infoTextBox.BackColor = backColor;
            infoTextBox.ForeColor = foreColor;
            Application.DoEvents();
        }

        private void SetNextTestMode()
        {
            if (testMode == TestMode.Result)
                SetTestMode(TestMode.Idle);
            else
                SetTestMode(testMode + 1);
        }

        private bool SetOutput(TestCaseStepOutput output, bool turnOn)
        {
            try
            {
                if (output.Socket)
                {
                    if (output.Logic)
                        plcEngine.WriteDigital(GetSocketNodeId(output.Number - 1, (SocketType)output.ParentType), turnOn ? output.InUse : false);
                    else
                        plcEngine.WriteAnalogDouble(GetSocketNodeId(output.Number - 1, (SocketType)output.ParentType), (turnOn ? output.Value : 0), output.RangeMax);
                }
                else
                {
                    //Board registry
                    if (output.Logic)
                        modbusEngine.WriteDigital(output.Number - 1, turnOn ? output.InUse : false);
                    else
                        modbusEngine.WriteAnalog(output.Number - 1, (ushort)(turnOn ? output.Value : 0));
                }
            }
            catch (Exception e)
            {
                log.Error("SetOutput", e);
                return false;
            }

            return true;
        }

        private void SetPanelTestResult(bool result)
        {
            testTimer.Enabled = false;
            panelTestResult.Result = result;
            panelTestResult.EndDate = DateTime.Now;
            Database.GetInstance().SaveOrderTestResult(orderTestResult);

            SetTestMode(result ? TestMode.Print : TestMode.Result);
        }

        private bool SetPower(int index, bool turnOn)
        {
            SocketPin powerPin = powerPins[index];
            powerOn[index] = turnOn;
            return plcEngine.WriteDigital(powerPin.NodeId, turnOn);
        }

        /*private bool SetTemperatures(int start, int step)
        {
            try
            {
                string[] nodes = new string[6]
                {
                    PlcEngine.SetTempValue1,
                    PlcEngine.SetTempValue2,
                    PlcEngine.SetTempValue3,
                    PlcEngine.SetTempValue4,
                    PlcEngine.SetTempValue5,
                    PlcEngine.SetTempValue6
                };

                int sensorToGradFactor = 123;
                for (int i = 0; i < nodes.Length; i++)
                {
                    UInt16 value = (UInt16)((start + i * step) * sensorToGradFactor);
                    plcEngine.WriteAnalog(nodes[i], value);
                }

                return true;
            }
            catch (Exception e)
            {
                log.Error("SetTemperatures", e);
                return false;
            }
        }*/

        private void SetTestMode(TestMode mode)
        {
            testMode = mode;
            progressBar.Visible = false;

            switch (testMode)
            {
                case TestMode.Idle:
                    testTimer.Enabled = false;
                    SetInfoText((panelTestResult == null) ? "Rozpocznij nowy test\n\nWciśnij przycisk 'Testuj kolejny panel'" : "Kontynuacja zlecenia\n\nWciśnij przycisk 'Powtórz test panelu'");
                    ShowCellInfo(-1, -1);
                    break;

                case TestMode.InitBoard:
                    SetPower(0, true);
                    skipBoardInit = false;
                    DateTime now = DateTime.Now;
                    int delta;
                    int timeout = GetBoard().InstallTimeout;
                    progressBar.Maximum = timeout;
                    progressBar.Value = 0;
                    progressBar.Visible = true;
                    while ((delta = (int)(DateTime.Now - now).TotalSeconds) < timeout)
                    {
                        progressBar.Value = delta;
                        SetInfoText(String.Format("Inicjalizacja płyty głównej: {0}s", Math.Max(0, timeout - delta)));
                        Thread.Sleep(250);
                        Application.DoEvents();
                        if (abortTest || skipBoardInit)
                            break;
                    }
                    progressBar.Visible = false;

                    if (!abortTest)
                        SetNextTestMode();

                    break;

                case TestMode.Print:
                    SetInfoText("Drukowanie etykiety");

                    Task.Run(() =>
                    {
                        printerEngine.PrintLabel(Order.Model, Order.ModelDocNumber, Order.ProductName, panelTestResult.UserId);
                    });

                    SetNextTestMode();
                    break;

                case TestMode.Result:
                    Reset(false);
                    if (panelTestResult.Result)
                    {
                        SetInfoText("Wynik testu: POZYTYWNY", Color.Black, Consts.PositiveColor);
                        if ((panelTestResult.PanelNumber >= Order.DeclaredQuantity) && (TestMessageDialog.Show("Koniec zlecenia", "Przetestowano deklarowaną ilość paneli w ramach zlecenia.\nCzy zakończyć zlecenie?", TestMessageDialog.MessageType.Question) == DialogResult.OK))
                            EndOrder();
                    }
                    else
                    {
                        string errorMessage = Utils.GetString(panelTestResult.Errors);
                        SetInfoText("Wynik testu: NEGATYWNY" + ((errorMessage.Length == 0) ? "" : "\n\n" + errorMessage), Color.White, Consts.ErrorColor);
                    }
                    break;

                case TestMode.SetupPanel:
                    abortTest = false;
                    testTimer.Enabled = true;
                    modbusEngine.InitBoard(GetBoard().RegistryValue);
                    SetInfoText("Podłącz przewody do panelu\nZamknij klatkę bezpieczeństwa\nWciśnij przycisk Reset, a następnie przycisk Start na obudowie testera");
                    break;

                case TestMode.Test:
                    SetInfoText("");
                    currentStepIndex = -1;
                    TestNextStep();
                    break;

                case TestMode.TestGND:
                    string title = "Test PE, N i GND";
                    SetInfoText(title);
                    string message;
                    bool result = false;
                    try
                    {                        
                        List<SocketPin> pinNList = Socket.GetSocketPinListByType(TestCase.SocketList, SocketPinType.InN);
                        List<SocketPin> pinPeList = Socket.GetSocketPinListByType(TestCase.SocketList, SocketPinType.InPE);
                        foreach (SocketPin pin in pinNList)
                                plcEngine.WriteDigital(pin.NodeId, true);
                        foreach (SocketPin pin in pinPeList)
                                plcEngine.WriteDigital(pin.NodeId, true);

                        //string outPEContNodeId = Socket.GetSocketPinByType(SocketPinType.OutPE).NodeId;
                        //plcEngine.WriteDigital(outPEContNodeId, true);
                        Thread.Sleep(2000);

                        List<string> errorNList = new List<string>();
                        foreach (SocketPin pin in pinNList)
                        { 
                                if (!plcEngine.ReadDigital(pin.NodeId.Substring(4) + "_OK"))
                                    errorNList.Add(pin.GetLabel());
                                plcEngine.WriteDigital(pin.NodeId, false);
                        }
                        List<string> errorPeList = new List<string>();
                        foreach (SocketPin pin in pinPeList)
                        {
                            if (!plcEngine.ReadDigital(pin.NodeId.Substring(4) + "_OK"))
                                errorPeList.Add(pin.GetLabel());
                            plcEngine.WriteDigital(pin.NodeId, false);
                        }

                        //bool peResult = plcEngine.ReadDigital(Socket.GetSocketPinByType(SocketPinType.InPECont).NodeId);
                        //plcEngine.WriteDigital(outPEContNodeId, false);
                        result = ((errorNList.Count == 0) && (errorPeList.Count == 0));
                        if (result)
                        {
                            message = "Stwierdzono ciągłość PE i N";
                        }
                        else
                        {
                            message = "Brak ciągłości";
                            if (errorNList.Count > 0)
                                message += " - N (" + string.Join(",", errorNList) + ")";
                            if (errorPeList.Count > 0)
                                message += " - PE (" + string.Join(",", errorPeList) + ")";
                        }
                    }
                    catch (Exception e)
                    {
                        log.Error("TestGND", e);
                        message = "Wystąpił nieoczekiwany błąd";
                    }

                    SetInfoText(title + "\n\n" + message, result ? Color.Black : Color.White, result ? Consts.PositiveColor : Consts.ErrorColor);

                    Task.Delay(result? 2000 : 5000).ContinueWith(_ =>
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            if (result)
                            {
                                SetNextTestMode();
                            }
                            else
                            {
                                panelTestResult.Message = message;
                                panelTestResult.Errors = message;
                                AbortTest(false);
                            }
                        });
                    });

                    break;
            }

            //Update controls
            bool decisionStep = ((testMode == TestMode.Idle) || (testMode == TestMode.Result));
            newButton.Visible = decisionStep && ((panelTestResult == null) || panelTestResult.Result);
            printButton.Visible = decisionStep && ((panelTestResult != null) && panelTestResult.Result);
            repeatButton.Visible = decisionStep && ((panelTestResult != null) && !panelTestResult.Result);
            rejectButton.Visible = decisionStep && ((panelTestResult != null) && !panelTestResult.Result);
            exitButton.Visible = decisionStep;
            stopButton.Visible = ((testMode > TestMode.Idle) && (testMode < TestMode.Print));
            stopButton.Text = "Zatrzymaj test";
        }

        private bool ShowCellInfo(int row, int column)
        {
            infoRichTextBox.Clear();
            infoPictureList = null;
            infoPictureListIndex = 0;

            if ((row >= 0) && (row < dataGridView.RowCount))
            {
                infoRowIndex = row;
                infoColumnIndex = column;

                //Show info
                StepTestResult stepTestResult = dataGridView.Rows[row].DataBoundItem as StepTestResult;
                TestStatus testStatus;
                TestResult testResult;
                PowerModeTestResult powerModeTestResult = null;
                if ((column > 0) && (column < dataGridView.Columns.Count))
                {
                    infoRichTextBox.AppendText(String.Format("Krok: {0}, zasilanie: {1}. ", row + 1, dataGridView.Columns[column].HeaderText));
                    powerModeTestResult = stepTestResult.PowerModeTestResultList[column - 1];
                    testStatus = powerModeTestResult.GetTestStatus();
                    testResult = powerModeTestResult.Result;
                }
                else
                {
                    infoRichTextBox.AppendText(String.Format("Krok: {0}. ", row + 1));
                    testStatus = stepTestResult.GetTestStatus();
                    testResult = stepTestResult.Result;

                    for (int i = stepTestResult.PowerModeTestResultList.Length - 1; i >= 0; i--)
                        if (stepTestResult.PowerModeTestResultList[i].Result != TestResult.Unknown)
                        {
                            powerModeTestResult = stepTestResult.PowerModeTestResultList[i];
                            break;
                        }
                }
                //infoRichTextBox.AppendText(Environment.NewLine);

                switch (testStatus)
                {
                    case TestStatus.Tested:
                        infoRichTextBox.AppendText("Wynik testu: ");
                        infoRichTextBox.AppendText((testResult == TestResult.OK) ? "Pozytywny" : "NEGATYWNY!");
                        infoRichTextBox.AppendText(Environment.NewLine);

                        if (powerModeTestResult != null)
                        {                            
                            if (powerModeTestResult.InfoList != null)
                            {                                
                                infoRichTextBox.AppendText(string.Join(Environment.NewLine, powerModeTestResult.InfoList));
                                infoRichTextBox.AppendText(Environment.NewLine);
                            }
                            infoPictureList = powerModeTestResult.ErrorImageList;
                        }

                        break;

                    case TestStatus.NotTested:
                        infoRichTextBox.AppendText("Konfiguracja jeszcze nieprzetestowana");
                        break;

                    case TestStatus.Testing:
                        infoRichTextBox.AppendText("W trakcie testowania");
                        break;
                }
            }

            ShowInfoPicture(0);

            return true;
        }

        private void ShowInfoPicture(int index)
        {
            bool prevVisible = false;
            bool nextVisible = false;
            infoPictureBox.Image = infoPictureBox.InitialImage;

            if ((infoPictureList != null) && (infoPictureList.Count > 0))
            {
                if (index < 0)
                    index = 0;
                if (index >= infoPictureList.Count)
                    index = infoPictureList.Count - 1;
                infoPictureListIndex = index;

                prevVisible = index > 0;
                nextVisible = index + 1 < infoPictureList.Count;

                string fileName = Path.Combine(TestCase.GetErrorImagePath(), infoPictureList[index]);
                if (File.Exists(fileName))
                {
                    infoPictureBox.ImageLocation = fileName;
                    infoPictureBox.LoadAsync();
                }
            }

            prevImageButton.Visible = prevVisible;
            nextImageButton.Visible = nextVisible;
        }

        private void StartNewPanel(bool repeat)
        {
            currentStepIndex = -1;
            currentPowerModeIndex = 0;

            ShowCellInfo(-1, -1);

            Reset(true);

            //Prepare new panel
            int panelNumber;
            int tryNumber;

            if (panelTestResult == null)
            {
                panelNumber = 1;
                tryNumber = 1;
            }
            else if (repeat)
            {
                panelNumber = panelTestResult.PanelNumber;
                tryNumber = panelTestResult.Try + 1;
            }
            else
            {
                panelNumber = panelTestResult.PanelNumber + 1;
                tryNumber = 1;
            }

            panelTestResult = new PanelTestResult
            {
                PanelNumber = panelNumber,
                StartDate = DateTime.Now,
                Try = tryNumber
            };

            User user = AppEngine.GetInstance().User;
            panelTestResult.UserId = user.Id;
            panelTestResult.UserName = user.Name;

            //Init steps
            panelTestResult.StepTestResultList = new List<StepTestResult>();
            int index = 0;
            foreach (TestCaseStep tcs in TestCase.StepList)
            {
                StepTestResult stepTestResult = new StepTestResult(TestCase.PowerModeList)
                {
                    StepIndex = index++
                };
                panelTestResult.StepTestResultList.Add(stepTestResult);
            }

            orderTestResult.PanelTestResultList.Add(panelTestResult);
            Database.GetInstance().SaveOrderTestResult(orderTestResult);

            //Init data grid
            var bindingList = new BindingList<StepTestResult>(panelTestResult.StepTestResultList);
            stepTestResultBindingSource = new BindingSource(bindingList, null);
            dataGridView.DataSource = stepTestResultBindingSource;

            //Update info
            panelNumberLabel.Text = String.Format("{0} z {1}", panelNumber, Order.DeclaredQuantity);
            tryNumberLabel.Text = Convert.ToString(tryNumber);

            SetTestMode(TestMode.SetupPanel);
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            log.Info("Stop button pressed");
            AbortTest();
        }

        private void TestDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            //SetTemperatures(0, 0);
            Reset(false);

            if (plcEngine != null)
                plcEngine.Dispose();

            if (modbusEngine != null)
                modbusEngine.Dispose();

            if (printerEngine != null)
                printerEngine.Dispose();
        }

        private void TestDialog_Load(object sender, EventArgs e)
        {
            orderNumberLabel.Text = Order.OrderNumber;
            panelModelLabel.Text = Order.Model;
            userLabel.Text = Order.AuthorUserId;

            //TestMessageDialog.Show("Koniec zlecenia", "Przetestowano deklarowaną ilość paneli w ramach zlecenia.\nCzy zakończyć zlecenie?", TestMessageDialog.MessageType.Warning);

#if !DEBUG
            debug1Button.Visible = false;
            debug2Button.Visible = false;
#endif

            Database database = Database.GetInstance();
            orderTestResult = database.LoadOrderTestResult(Order);
            if (orderTestResult == null)
            {
                orderTestResult = new OrderTestResult(Order)
                {
                    OrderNumber = Order.OrderNumber
                };
            }
            else if (orderTestResult.PanelTestResultList.Count > 0)
                panelTestResult = orderTestResult.PanelTestResultList[orderTestResult.PanelTestResultList.Count - 1];

            tryNumberLabel.Text = (panelTestResult == null) ? "" : panelTestResult.Try.ToString();
            testTimeLabel.Text = "";
            panelNumberLabel.Text = String.Format("{0} z {1}", (panelTestResult == null) ? 0 : panelTestResult.PanelNumber, Order.DeclaredQuantity);

            //Dynamic input grid columns
            DataGridViewColumn powerColumn = dataGridView.Columns[1];
            for (int i = 0; i < TestCase.PowerModeList.Count; i++)
            {
                PowerMode powerMode = TestCase.PowerModeList[i];
                string headerText = typeof(PowerMode).GetMember(powerMode.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description;

                if (i > 0)
                    powerColumn = (DataGridViewColumn)powerColumn.Clone();
                powerColumn.DataPropertyName = String.Format("PowerModeResult{0}", i);
                powerColumn.HeaderText = headerText;
                if (i > 0)
                    dataGridView.Columns.Add(powerColumn);
            }

            //Init data
            if (TestCase.StepList.Count == 0)
            {
                log.Error("TestCase has no steps");
                MessageBox.Show("Scenariusz nie ma zdefiniowanych kroków\n\nTesty nie mogą być kontunuowane", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (TestCase.GetBoard() == null)
            {
                log.Error("TestCase has no board");
                MessageBox.Show("Scenariusz nie ma zdefiniowanej poprawnej płyty\n\nTesty nie mogą być kontunuowane", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                DialogResult = DialogResult.Cancel;
                return;
            }

            usedOutputs = new bool[TestCase.StepList[0].OutputList.Count];

            Socket aoutSocket = TestCase.SocketList[(int)SocketType.AOut];
            powerPins[(int)Power.L1] = aoutSocket.GetFirstPinByType(SocketPinType.Out230VACL1Power);
            powerPins[(int)Power.L2] = aoutSocket.GetFirstPinByType(SocketPinType.Out230VACL2Power);
            powerPins[(int)Power.L3] = aoutSocket.GetFirstPinByType(SocketPinType.Out230VACL3Power);

            Role userRole = AppEngine.GetInstance().User.Role;

            //Init modbus
            bool result = false;
            try
            {
                modbusEngine = new ModbusEngine(TestCase);
                result = modbusEngine.Connect();
            }
            catch (Exception ex)
            {
                log.Error("Init modbus", ex);
            }
            if (!result)
            {
                log.ErrorFormat("Cannot connect with the display. Address: {0}, unitID: {1}", GetBoard().AddressIP, GetBoard().UnitID);
                MessageBox.Show(String.Format("Nie można nawiązać połączenia z wyświetlaczem.\nAdres: {0}, unit ID: {1}\n\nTesty nie mogą być kontunuowane", GetBoard().AddressIP, GetBoard().UnitID), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                if (userRole == Role.Operator)
                    DialogResult = DialogResult.Cancel;
            }

            //Init PLC
            try
            {
                plcEngine = new PlcEngine();
                if (plcEngine.Connect())
                {
                    OpcUaClient opc = plcEngine.OpcClient;
                    opc.MonitorValue(PlcEngine.GetNodeId(PlcEngine.GetStartValue), new Action<bool, Action>(MonitorStartValue));
                    opc.MonitorValue(PlcEngine.GetNodeId(PlcEngine.GetStopValue), new Action<bool, Action>(MonitorStopValue));
                    opc.MonitorValue(PlcEngine.GetNodeId(PlcEngine.GetSafetyValue), new Action<bool, Action>(MonitorSafetyValue));
                    //SetTemperatures(30, 5);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Init PLC", ex);
            }
            if (!result)
            {
                log.Error("Cannot connect with the PLC");
                MessageBox.Show("Nie można nawiązać połączenia ze sterownikiem PLC\n\nTesty nie mogą być kontunuowane", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                if (userRole == Role.Operator)
                    DialogResult = DialogResult.Cancel;
            }

            printerEngine = new PrinterEngine();

            SetTestMode(TestMode.Idle);
        }

        private void TestNextPowerMode()
        {
            if (currentPowerModeIndex + 1 >= TestCase.PowerModeList.Count)
            {
                StepTestResult stepTestResult = panelTestResult.StepTestResultList[currentStepIndex];
                stepTestResult.CalculateResult();
                stepTestResult.EndDate = DateTime.Now;
                TestNextStep();
            }
            else
            {
                currentPowerModeIndex++;
                SetInfoText(String.Format("Krok {0} z {1}\n\nZasilanie: {2}", currentStepIndex + 1, TestCase.StepList.Count,
                    typeof(PowerMode).GetMember(TestCase.PowerModeList[currentPowerModeIndex].ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description));

                DoTest();
            }
        }

        private void TestNextStep()
        {
            if (currentStepIndex + 1 >= TestCase.StepList.Count)
            {
                //Positive!
                SetPanelTestResult(true);
            }
            else
            {
                currentStepIndex++;
                currentPowerModeIndex = -1;
                TestNextPowerMode();
            }
        }

        private void testTimer_Tick(object sender, EventArgs e)
        {
            if (panelTestResult == null)
            {
                testTimeLabel.Text = "";
            }
            else
            {
                int seconds = (int)((DateTime.Now - panelTestResult.StartDate).TotalSeconds);
                int minutes = seconds / 60;
                if (minutes >= 60)
                    testTimeLabel.Text = "> 1h";
                else
                    testTimeLabel.Text = String.Format("{0}:{1:00}", minutes, seconds % 60);
            }
        }
    }
}