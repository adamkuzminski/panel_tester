﻿namespace PanelTester.UI
{
    partial class TestDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.controlPanel = new System.Windows.Forms.Panel();
            this.labelSafetyInfo = new System.Windows.Forms.Label();
            this.debug2Button = new System.Windows.Forms.Button();
            this.debug1Button = new System.Windows.Forms.Button();
            this.panelModelLabel = new System.Windows.Forms.Label();
            this.orderNumberLabel = new System.Windows.Forms.Label();
            this.userLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.testTimeLabel = new System.Windows.Forms.Label();
            this.tryNumberLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panelNumberLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.stopButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.rejectButton = new System.Windows.Forms.Button();
            this.printButton = new System.Windows.Forms.Button();
            this.topInfoPanel = new System.Windows.Forms.Panel();
            this.infoTextBox = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.newButton = new System.Windows.Forms.Button();
            this.repeatButton = new System.Windows.Forms.Button();
            this.infoPanel = new System.Windows.Forms.Panel();
            this.nextImageButton = new System.Windows.Forms.Button();
            this.prevImageButton = new System.Windows.Forms.Button();
            this.infoPictureBox = new System.Windows.Forms.PictureBox();
            this.infoRichTextBox = new System.Windows.Forms.RichTextBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.stepIndexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.powerColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stepTestResultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.testTimer = new System.Windows.Forms.Timer(this.components);
            this.splitter = new System.Windows.Forms.Splitter();
            this.splitterInfo = new System.Windows.Forms.Splitter();
            this.controlPanel.SuspendLayout();
            this.topInfoPanel.SuspendLayout();
            this.infoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.infoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepTestResultBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // controlPanel
            // 
            this.controlPanel.BackColor = System.Drawing.Color.White;
            this.controlPanel.Controls.Add(this.labelSafetyInfo);
            this.controlPanel.Controls.Add(this.debug2Button);
            this.controlPanel.Controls.Add(this.debug1Button);
            this.controlPanel.Controls.Add(this.panelModelLabel);
            this.controlPanel.Controls.Add(this.orderNumberLabel);
            this.controlPanel.Controls.Add(this.userLabel);
            this.controlPanel.Controls.Add(this.label5);
            this.controlPanel.Controls.Add(this.testTimeLabel);
            this.controlPanel.Controls.Add(this.tryNumberLabel);
            this.controlPanel.Controls.Add(this.label9);
            this.controlPanel.Controls.Add(this.label8);
            this.controlPanel.Controls.Add(this.label7);
            this.controlPanel.Controls.Add(this.panelNumberLabel);
            this.controlPanel.Controls.Add(this.label3);
            this.controlPanel.Controls.Add(this.label1);
            this.controlPanel.Controls.Add(this.stopButton);
            this.controlPanel.Controls.Add(this.exitButton);
            this.controlPanel.Controls.Add(this.rejectButton);
            this.controlPanel.Controls.Add(this.printButton);
            this.controlPanel.Controls.Add(this.topInfoPanel);
            this.controlPanel.Controls.Add(this.newButton);
            this.controlPanel.Controls.Add(this.repeatButton);
            this.controlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.controlPanel.Location = new System.Drawing.Point(0, 335);
            this.controlPanel.Name = "controlPanel";
            this.controlPanel.Size = new System.Drawing.Size(996, 240);
            this.controlPanel.TabIndex = 1;
            // 
            // labelSafetyInfo
            // 
            this.labelSafetyInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSafetyInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSafetyInfo.Location = new System.Drawing.Point(587, 8);
            this.labelSafetyInfo.Name = "labelSafetyInfo";
            this.labelSafetyInfo.Size = new System.Drawing.Size(145, 110);
            this.labelSafetyInfo.TabIndex = 22;
            this.labelSafetyInfo.Text = "Obwód bezpieczeństwa";
            this.labelSafetyInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // debug2Button
            // 
            this.debug2Button.Location = new System.Drawing.Point(3, 180);
            this.debug2Button.Name = "debug2Button";
            this.debug2Button.Size = new System.Drawing.Size(130, 34);
            this.debug2Button.TabIndex = 14;
            this.debug2Button.Text = "Debug 2";
            this.debug2Button.UseVisualStyleBackColor = true;
            this.debug2Button.Click += new System.EventHandler(this.debug2Button_Click);
            // 
            // debug1Button
            // 
            this.debug1Button.Location = new System.Drawing.Point(4, 140);
            this.debug1Button.Name = "debug1Button";
            this.debug1Button.Size = new System.Drawing.Size(130, 34);
            this.debug1Button.TabIndex = 13;
            this.debug1Button.Text = "Debug 1";
            this.debug1Button.UseVisualStyleBackColor = true;
            this.debug1Button.Click += new System.EventHandler(this.debug1Button_Click);
            // 
            // panelModelLabel
            // 
            this.panelModelLabel.AutoSize = true;
            this.panelModelLabel.Location = new System.Drawing.Point(140, 43);
            this.panelModelLabel.Name = "panelModelLabel";
            this.panelModelLabel.Size = new System.Drawing.Size(14, 20);
            this.panelModelLabel.TabIndex = 3;
            this.panelModelLabel.Text = "-";
            // 
            // orderNumberLabel
            // 
            this.orderNumberLabel.AutoSize = true;
            this.orderNumberLabel.Location = new System.Drawing.Point(140, 17);
            this.orderNumberLabel.Name = "orderNumberLabel";
            this.orderNumberLabel.Size = new System.Drawing.Size(14, 20);
            this.orderNumberLabel.TabIndex = 1;
            this.orderNumberLabel.Text = "-";
            // 
            // userLabel
            // 
            this.userLabel.AutoSize = true;
            this.userLabel.Location = new System.Drawing.Point(140, 69);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(14, 20);
            this.userLabel.TabIndex = 11;
            this.userLabel.Text = "-";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(738, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Numer panelu:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // testTimeLabel
            // 
            this.testTimeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.testTimeLabel.AutoSize = true;
            this.testTimeLabel.Location = new System.Drawing.Point(856, 43);
            this.testTimeLabel.Name = "testTimeLabel";
            this.testTimeLabel.Size = new System.Drawing.Size(14, 20);
            this.testTimeLabel.TabIndex = 10;
            this.testTimeLabel.Text = "-";
            // 
            // tryNumberLabel
            // 
            this.tryNumberLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tryNumberLabel.AutoSize = true;
            this.tryNumberLabel.Location = new System.Drawing.Point(856, 17);
            this.tryNumberLabel.Name = "tryNumberLabel";
            this.tryNumberLabel.Size = new System.Drawing.Size(14, 20);
            this.tryNumberLabel.TabIndex = 9;
            this.tryNumberLabel.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Użytkownik:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(761, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Czas testu:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(795, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Próba:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelNumberLabel
            // 
            this.panelNumberLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelNumberLabel.AutoSize = true;
            this.panelNumberLabel.Location = new System.Drawing.Point(856, 69);
            this.panelNumberLabel.Name = "panelNumberLabel";
            this.panelNumberLabel.Size = new System.Drawing.Size(14, 20);
            this.panelNumberLabel.TabIndex = 5;
            this.panelNumberLabel.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Model panelu:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numer zlecenia:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // stopButton
            // 
            this.stopButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.stopButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.stopButton.Image = global::PanelTester.Properties.Resources.Pause;
            this.stopButton.Location = new System.Drawing.Point(389, 8);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(220, 110);
            this.stopButton.TabIndex = 15;
            this.stopButton.Text = "Zatrzymaj test";
            this.stopButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.stopButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.stopButton.UseVisualStyleBackColor = false;
            this.stopButton.Visible = false;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.exitButton.Image = global::PanelTester.Properties.Resources.Exit;
            this.exitButton.Location = new System.Drawing.Point(615, 8);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(220, 110);
            this.exitButton.TabIndex = 17;
            this.exitButton.Text = "Zakończ testy";
            this.exitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // rejectButton
            // 
            this.rejectButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rejectButton.Image = global::PanelTester.Properties.Resources.Delete2;
            this.rejectButton.Location = new System.Drawing.Point(389, 8);
            this.rejectButton.Name = "rejectButton";
            this.rejectButton.Size = new System.Drawing.Size(220, 110);
            this.rejectButton.TabIndex = 20;
            this.rejectButton.Text = "Odrzuć panel";
            this.rejectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.rejectButton.UseVisualStyleBackColor = true;
            this.rejectButton.Click += new System.EventHandler(this.rejectButton_Click);
            // 
            // printButton
            // 
            this.printButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.printButton.Image = global::PanelTester.Properties.Resources.Print;
            this.printButton.Location = new System.Drawing.Point(389, 8);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(220, 110);
            this.printButton.TabIndex = 18;
            this.printButton.Text = "Ponownie wydrukuj etykietę";
            this.printButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // topInfoPanel
            // 
            this.topInfoPanel.Controls.Add(this.infoTextBox);
            this.topInfoPanel.Controls.Add(this.progressBar);
            this.topInfoPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.topInfoPanel.Location = new System.Drawing.Point(0, 124);
            this.topInfoPanel.Name = "topInfoPanel";
            this.topInfoPanel.Size = new System.Drawing.Size(996, 116);
            this.topInfoPanel.TabIndex = 21;
            // 
            // infoTextBox
            // 
            this.infoTextBox.BackColor = System.Drawing.SystemColors.HighlightText;
            this.infoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.infoTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.infoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.infoTextBox.Location = new System.Drawing.Point(0, 12);
            this.infoTextBox.Multiline = true;
            this.infoTextBox.Name = "infoTextBox";
            this.infoTextBox.ReadOnly = true;
            this.infoTextBox.Size = new System.Drawing.Size(996, 104);
            this.infoTextBox.TabIndex = 13;
            this.infoTextBox.TabStop = false;
            this.infoTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // progressBar
            // 
            this.progressBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.progressBar.Location = new System.Drawing.Point(0, 0);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(996, 10);
            this.progressBar.Step = 1;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar.TabIndex = 14;
            this.progressBar.Value = 23;
            this.progressBar.Visible = false;
            // 
            // newButton
            // 
            this.newButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.newButton.Image = global::PanelTester.Properties.Resources.Play2;
            this.newButton.Location = new System.Drawing.Point(163, 8);
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(220, 110);
            this.newButton.TabIndex = 16;
            this.newButton.Text = "Testuj kolejny panel";
            this.newButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.newButton.UseVisualStyleBackColor = true;
            this.newButton.Click += new System.EventHandler(this.newButton_Click);
            // 
            // repeatButton
            // 
            this.repeatButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.repeatButton.Image = global::PanelTester.Properties.Resources.Repeat;
            this.repeatButton.Location = new System.Drawing.Point(163, 8);
            this.repeatButton.Name = "repeatButton";
            this.repeatButton.Size = new System.Drawing.Size(220, 110);
            this.repeatButton.TabIndex = 19;
            this.repeatButton.Text = "Powtórz test panelu";
            this.repeatButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.repeatButton.UseVisualStyleBackColor = true;
            this.repeatButton.Click += new System.EventHandler(this.repeatButton_Click);
            // 
            // infoPanel
            // 
            this.infoPanel.BackColor = System.Drawing.Color.White;
            this.infoPanel.Controls.Add(this.splitterInfo);
            this.infoPanel.Controls.Add(this.nextImageButton);
            this.infoPanel.Controls.Add(this.prevImageButton);
            this.infoPanel.Controls.Add(this.infoPictureBox);
            this.infoPanel.Controls.Add(this.infoRichTextBox);
            this.infoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoPanel.Location = new System.Drawing.Point(504, 0);
            this.infoPanel.Name = "infoPanel";
            this.infoPanel.Size = new System.Drawing.Size(492, 335);
            this.infoPanel.TabIndex = 2;
            // 
            // nextImageButton
            // 
            this.nextImageButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.nextImageButton.Image = global::PanelTester.Properties.Resources.Right;
            this.nextImageButton.Location = new System.Drawing.Point(422, 236);
            this.nextImageButton.Name = "nextImageButton";
            this.nextImageButton.Size = new System.Drawing.Size(64, 64);
            this.nextImageButton.TabIndex = 3;
            this.nextImageButton.UseVisualStyleBackColor = true;
            this.nextImageButton.Visible = false;
            this.nextImageButton.Click += new System.EventHandler(this.nextImageButton_Click);
            // 
            // prevImageButton
            // 
            this.prevImageButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.prevImageButton.Image = global::PanelTester.Properties.Resources.Left;
            this.prevImageButton.Location = new System.Drawing.Point(6, 236);
            this.prevImageButton.Name = "prevImageButton";
            this.prevImageButton.Size = new System.Drawing.Size(64, 64);
            this.prevImageButton.TabIndex = 2;
            this.prevImageButton.UseVisualStyleBackColor = true;
            this.prevImageButton.Visible = false;
            this.prevImageButton.Click += new System.EventHandler(this.prevImageButton_Click);
            // 
            // infoPictureBox
            // 
            this.infoPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoPictureBox.InitialImage = global::PanelTester.Properties.Resources.Photo;
            this.infoPictureBox.Location = new System.Drawing.Point(0, 202);
            this.infoPictureBox.Name = "infoPictureBox";
            this.infoPictureBox.Size = new System.Drawing.Size(492, 133);
            this.infoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.infoPictureBox.TabIndex = 1;
            this.infoPictureBox.TabStop = false;
            // 
            // infoRichTextBox
            // 
            this.infoRichTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.infoRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.infoRichTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.infoRichTextBox.Name = "infoRichTextBox";
            this.infoRichTextBox.ReadOnly = true;
            this.infoRichTextBox.Size = new System.Drawing.Size(492, 202);
            this.infoRichTextBox.TabIndex = 0;
            this.infoRichTextBox.TabStop = false;
            this.infoRichTextBox.Text = "";
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.AutoGenerateColumns = false;
            this.dataGridView.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.stepIndexDataGridViewTextBoxColumn,
            this.powerColumn});
            this.dataGridView.DataSource = this.stepTestResultBindingSource;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Left;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.ShowEditingIcon = false;
            this.dataGridView.ShowRowErrors = false;
            this.dataGridView.Size = new System.Drawing.Size(501, 335);
            this.dataGridView.TabIndex = 3;
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellClick);
            this.dataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);
            this.dataGridView.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridView_CellPainting);
            // 
            // stepIndexDataGridViewTextBoxColumn
            // 
            this.stepIndexDataGridViewTextBoxColumn.DataPropertyName = "StepIndex";
            this.stepIndexDataGridViewTextBoxColumn.HeaderText = "Krok";
            this.stepIndexDataGridViewTextBoxColumn.Name = "stepIndexDataGridViewTextBoxColumn";
            this.stepIndexDataGridViewTextBoxColumn.ReadOnly = true;
            this.stepIndexDataGridViewTextBoxColumn.Width = 80;
            // 
            // powerColumn
            // 
            this.powerColumn.HeaderText = "Zasilanie";
            this.powerColumn.Name = "powerColumn";
            this.powerColumn.ReadOnly = true;
            this.powerColumn.Width = 120;
            // 
            // stepTestResultBindingSource
            // 
            this.stepTestResultBindingSource.DataSource = typeof(PanelTester.Data.StepTestResult);
            // 
            // testTimer
            // 
            this.testTimer.Interval = 1000;
            this.testTimer.Tick += new System.EventHandler(this.testTimer_Tick);
            // 
            // splitter
            // 
            this.splitter.Location = new System.Drawing.Point(501, 0);
            this.splitter.Name = "splitter";
            this.splitter.Size = new System.Drawing.Size(3, 335);
            this.splitter.TabIndex = 4;
            this.splitter.TabStop = false;
            // 
            // splitterInfo
            // 
            this.splitterInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterInfo.Location = new System.Drawing.Point(0, 202);
            this.splitterInfo.Name = "splitterInfo";
            this.splitterInfo.Size = new System.Drawing.Size(492, 3);
            this.splitterInfo.TabIndex = 4;
            this.splitterInfo.TabStop = false;
            // 
            // TestDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.ClientSize = new System.Drawing.Size(996, 575);
            this.Controls.Add(this.infoPanel);
            this.Controls.Add(this.splitter);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.controlPanel);
            this.Name = "TestDialog";
            this.Text = "Testowanie paneli";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TestDialog_FormClosing);
            this.Load += new System.EventHandler(this.TestDialog_Load);
            this.controlPanel.ResumeLayout(false);
            this.controlPanel.PerformLayout();
            this.topInfoPanel.ResumeLayout(false);
            this.topInfoPanel.PerformLayout();
            this.infoPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.infoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepTestResultBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel controlPanel;
        private System.Windows.Forms.Panel infoPanel;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label userLabel;
        private System.Windows.Forms.Label testTimeLabel;
        private System.Windows.Forms.Label tryNumberLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label panelNumberLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label panelModelLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label orderNumberLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource stepTestResultBindingSource;
        private System.Windows.Forms.Timer testTimer;
        private System.Windows.Forms.PictureBox infoPictureBox;
        private System.Windows.Forms.RichTextBox infoRichTextBox;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button debug2Button;
        private System.Windows.Forms.Button debug1Button;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button newButton;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button repeatButton;
        private System.Windows.Forms.Button rejectButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn stepIndexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn powerColumn;
        private System.Windows.Forms.Splitter splitter;
        private System.Windows.Forms.Panel topInfoPanel;
        private System.Windows.Forms.TextBox infoTextBox;
        private System.Windows.Forms.Button nextImageButton;
        private System.Windows.Forms.Button prevImageButton;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label labelSafetyInfo;
        private System.Windows.Forms.Splitter splitterInfo;
    }
}
