﻿using Csv;
using PanelTester.Common;
using PanelTester.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class TestHistoryRegistryForm : BaseRegistryForm
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public TestHistoryRegistryForm()
        {
            InitializeComponent();
        }

        protected override void AddRecord()
        {
            throw new NotImplementedException();
        }

        protected override void DeleteRecord()
        {
            Database database = Database.GetInstance();
            foreach (DataGridViewRow row in dataGridView.SelectedRows)
            {
                Order order = row.DataBoundItem as Order;
                database.DeleteOrder(order);
                database.DeleteOrderTestResult(order);
                database.OrderList.Remove(order);
            }
            RefreshGrid();
        }

        protected override void EditRecord()
        {
            throw new NotImplementedException();
        }

        protected override bool IsCopyAllowed()
        {
            return false;
        }

        protected override void RefreshGrid()
        {
            RefreshGrid(new BindingSource(new SortableBindingList<Order>(AppEngine.GetInstance().Database.OrderList), null), dataGridView.Columns.Count - 1, System.ComponentModel.ListSortDirection.Descending);
            UpdateDetailsGridView();
        }

        protected override void UpdateToolStripButtons()
        {
            base.UpdateToolStripButtons();
            addToolStripButton.Visible = false;
            editToolStripButton.Visible = false;
            previewToolStripButton.Visible = false;
            exportToolStripButton.Visible = deleteToolStripButton.Visible;
        }

        private void dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            UpdateDetailsGridView();
        }

        private void dataGridView_Sorted(object sender, EventArgs e)
        {
            UpdateDetailsGridView();
        }

        private bool ExportDataToFile(string fileName)
        {
            bool result = false;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                string[] headers = null;
                List<string[]> csvData = new List<string[]>();
                Database database = Database.GetInstance();
                StringBuilder sb = new StringBuilder();
                foreach (DataGridViewRow row in dataGridView.SelectedRows)
                {
                    Order order = row.DataBoundItem as Order;
                    OrderTestResult orderTestResult = database.LoadOrderTestResult(order);
                    if (orderTestResult != null)
                    {
                        if (headers == null)
                            headers = orderTestResult.GetCSVHeader();
                        csvData.AddRange(orderTestResult.GetCSVData());
                    }
                }

                using (TextWriter writer = new StreamWriter(fileName, false, Encoding.UTF8))
                {
                    CsvWriter.Write(writer, headers, csvData);
                }

                result = true;
            }
            catch (Exception e)
            {
                log.Error("ExportDataToFile", e);
            }
            Cursor.Current = Cursors.Default;
            return result;
        }

        private void exportToolStripButton_Click(object sender, System.EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (ExportDataToFile(saveFileDialog.FileName))
                    MessageBox.Show("Dane zostały wyeksportowane", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Wystąpił problem podczas próby eksportu danych", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void TestHistoryRegistryForm_Load(object sender, EventArgs e)
        {
        }

        private void UpdateDetailsGridView()
        {
            string stats = "";
            if (dataGridView.SelectedRows.Count == 0)
            {
                detailsDataGridView.DataSource = null;
            }
            else
            {
                Order order = dataGridView.SelectedRows[0].DataBoundItem as Order;
                Database database = Database.GetInstance();
                OrderTestResult orderTestResult = database.LoadOrderTestResult(order);
                if ((orderTestResult == null) || (orderTestResult.PanelTestResultList == null) || (orderTestResult.PanelTestResultList.Count == 0))
                {
                    detailsDataGridView.DataSource = null;
                }
                else
                {
                    RefreshGrid(detailsDataGridView, new BindingSource(new SortableBindingList<PanelTestResult>(orderTestResult.PanelTestResultList), null));

                    int totalCount = 0;
                    int correctCount = 0;
                    int repeatCount = 0;
                    foreach (PanelTestResult panelTestResult in orderTestResult.PanelTestResultList)
                    {
                        totalCount++;
                        if (panelTestResult.Result)
                            correctCount++;
                        if (panelTestResult.Try > 1)
                            repeatCount++;
                    }
                    stats = string.Format("Testy: {0}, poprawne panele: {1}, powtórzenia: {2}", totalCount, correctCount, repeatCount);
                }
            }

            statsToolStripStatusLabel.Text = stats;
        }
    }
}