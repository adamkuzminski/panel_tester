﻿using System;

namespace PanelTester.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.startToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.boardsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.editorToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.reportsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.settingsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.userToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.userProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelCompany = new System.Windows.Forms.Label();
            this.mainToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.BackColor = System.Drawing.SystemColors.Control;
            this.mainToolStrip.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.mainToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(72, 72);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startToolStripButton,
            this.boardsToolStripButton,
            this.editorToolStripButton,
            this.reportsToolStripButton,
            this.settingsToolStripButton,
            this.userToolStripDropDownButton});
            this.mainToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.mainToolStrip.Size = new System.Drawing.Size(1242, 94);
            this.mainToolStrip.TabIndex = 3;
            // 
            // startToolStripButton
            // 
            this.startToolStripButton.AutoSize = false;
            this.startToolStripButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.startToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("startToolStripButton.Image")));
            this.startToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.startToolStripButton.Name = "startToolStripButton";
            this.startToolStripButton.Size = new System.Drawing.Size(120, 91);
            this.startToolStripButton.Text = "Rozpocznij test";
            this.startToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.startToolStripButton.ToolTipText = "Rozpocznij test";
            this.startToolStripButton.Click += new System.EventHandler(this.startToolStripButton_Click);
            // 
            // boardsToolStripButton
            // 
            this.boardsToolStripButton.AutoSize = false;
            this.boardsToolStripButton.Image = global::PanelTester.Properties.Resources.Board;
            this.boardsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.boardsToolStripButton.Name = "boardsToolStripButton";
            this.boardsToolStripButton.Size = new System.Drawing.Size(120, 91);
            this.boardsToolStripButton.Text = "Płyty główne";
            this.boardsToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.boardsToolStripButton.Click += new System.EventHandler(this.boardsToolStripButton_Click);
            // 
            // editorToolStripButton
            // 
            this.editorToolStripButton.AutoSize = false;
            this.editorToolStripButton.Image = global::PanelTester.Properties.Resources.Editor;
            this.editorToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editorToolStripButton.Name = "editorToolStripButton";
            this.editorToolStripButton.Size = new System.Drawing.Size(120, 91);
            this.editorToolStripButton.Text = "Scenariusze testowe";
            this.editorToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.editorToolStripButton.Click += new System.EventHandler(this.editorToolStripButton_Click);
            // 
            // reportsToolStripButton
            // 
            this.reportsToolStripButton.AutoSize = false;
            this.reportsToolStripButton.Image = global::PanelTester.Properties.Resources.Report;
            this.reportsToolStripButton.ImageTransparentColor = System.Drawing.Color.MediumBlue;
            this.reportsToolStripButton.Name = "reportsToolStripButton";
            this.reportsToolStripButton.Size = new System.Drawing.Size(120, 91);
            this.reportsToolStripButton.Text = "Historia pomiarów";
            this.reportsToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.reportsToolStripButton.Click += new System.EventHandler(this.reportsToolStripButton_Click);
            // 
            // settingsToolStripButton
            // 
            this.settingsToolStripButton.AutoSize = false;
            this.settingsToolStripButton.Image = global::PanelTester.Properties.Resources.Settings;
            this.settingsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.settingsToolStripButton.Name = "settingsToolStripButton";
            this.settingsToolStripButton.Size = new System.Drawing.Size(120, 91);
            this.settingsToolStripButton.Text = "Konfiguracja";
            this.settingsToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.settingsToolStripButton.Click += new System.EventHandler(this.settingsToolStripButton_Click);
            // 
            // userToolStripDropDownButton
            // 
            this.userToolStripDropDownButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.userToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userProfileToolStripMenuItem,
            this.usersToolStripMenuItem,
            this.logoutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.userToolStripDropDownButton.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.userToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("userToolStripDropDownButton.Image")));
            this.userToolStripDropDownButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.userToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.userToolStripDropDownButton.Name = "userToolStripDropDownButton";
            this.userToolStripDropDownButton.Size = new System.Drawing.Size(85, 91);
            this.userToolStripDropDownButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            // 
            // userProfileToolStripMenuItem
            // 
            this.userProfileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.userProfileToolStripMenuItem.Name = "userProfileToolStripMenuItem";
            this.userProfileToolStripMenuItem.ShowShortcutKeys = false;
            this.userProfileToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.userProfileToolStripMenuItem.Text = "Profil użytkownika";
            this.userProfileToolStripMenuItem.ToolTipText = "Profil użytkownika";
            this.userProfileToolStripMenuItem.Click += new System.EventHandler(this.userProfileToolStripMenuItem_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.ShowShortcutKeys = false;
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.usersToolStripMenuItem.Text = "Zarządzanie użytkownikami";
            this.usersToolStripMenuItem.ToolTipText = "Zarządzanie użytkownikami";
            this.usersToolStripMenuItem.Click += new System.EventHandler(this.usersToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.ShowShortcutKeys = false;
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.logoutToolStripMenuItem.Text = "Wyloguj";
            this.logoutToolStripMenuItem.ToolTipText = "Wyloguj";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShowShortcutKeys = false;
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.exitToolStripMenuItem.Text = "Zakończ aplikację";
            this.exitToolStripMenuItem.ToolTipText = "Zakończ aplikację";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // labelCompany
            // 
            this.labelCompany.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCompany.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.labelCompany.Location = new System.Drawing.Point(0, 672);
            this.labelCompany.Name = "labelCompany";
            this.labelCompany.Size = new System.Drawing.Size(1242, 43);
            this.labelCompany.TabIndex = 4;
            this.labelCompany.Text = "DRAK 2018-2019";
            this.labelCompany.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PanelTester.Properties.Resources.Backer;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1242, 715);
            this.Controls.Add(this.labelCompany);
            this.Controls.Add(this.mainToolStrip);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.Name = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton startToolStripButton;
        private System.Windows.Forms.ToolStripDropDownButton userToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem userProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton editorToolStripButton;
        private System.Windows.Forms.ToolStripButton settingsToolStripButton;
        private System.Windows.Forms.ToolStripButton reportsToolStripButton;
        private System.Windows.Forms.ToolStripButton boardsToolStripButton;
        private System.Windows.Forms.Label labelCompany;
    }
}

