﻿namespace PanelTester.UI
{
    partial class CheckHardwareDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.plcButton = new System.Windows.Forms.Button();
            this.socketButton = new System.Windows.Forms.Button();
            this.modbusButton = new System.Windows.Forms.Button();
            this.printerButton = new System.Windows.Forms.Button();
            this.res1Label = new System.Windows.Forms.Label();
            this.res2Label = new System.Windows.Forms.Label();
            this.res3Label = new System.Windows.Forms.Label();
            this.res4Label = new System.Windows.Forms.Label();
            this.infoLabel = new System.Windows.Forms.Label();
            this.bottomPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.infoLabel);
            this.mainPanel.Controls.Add(this.res4Label);
            this.mainPanel.Controls.Add(this.res3Label);
            this.mainPanel.Controls.Add(this.res2Label);
            this.mainPanel.Controls.Add(this.res1Label);
            this.mainPanel.Controls.Add(this.printerButton);
            this.mainPanel.Controls.Add(this.modbusButton);
            this.mainPanel.Controls.Add(this.socketButton);
            this.mainPanel.Controls.Add(this.plcButton);
            // 
            // plcButton
            // 
            this.plcButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.plcButton.Location = new System.Drawing.Point(214, 51);
            this.plcButton.Name = "plcButton";
            this.plcButton.Size = new System.Drawing.Size(300, 80);
            this.plcButton.TabIndex = 0;
            this.plcButton.Text = "PLC";
            this.plcButton.UseVisualStyleBackColor = true;
            this.plcButton.Click += new System.EventHandler(this.plcButton_Click);
            // 
            // socketButton
            // 
            this.socketButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.socketButton.Location = new System.Drawing.Point(214, 137);
            this.socketButton.Name = "socketButton";
            this.socketButton.Size = new System.Drawing.Size(300, 80);
            this.socketButton.TabIndex = 1;
            this.socketButton.Text = "Zestaw wtyczek";
            this.socketButton.UseVisualStyleBackColor = true;
            this.socketButton.Click += new System.EventHandler(this.socketButton_Click);
            // 
            // modbusButton
            // 
            this.modbusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.modbusButton.Location = new System.Drawing.Point(214, 223);
            this.modbusButton.Name = "modbusButton";
            this.modbusButton.Size = new System.Drawing.Size(300, 80);
            this.modbusButton.TabIndex = 2;
            this.modbusButton.Text = "Modbus";
            this.modbusButton.UseVisualStyleBackColor = true;
            this.modbusButton.Click += new System.EventHandler(this.modbusButton_Click);
            // 
            // printerButton
            // 
            this.printerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.printerButton.Location = new System.Drawing.Point(214, 309);
            this.printerButton.Name = "printerButton";
            this.printerButton.Size = new System.Drawing.Size(300, 80);
            this.printerButton.TabIndex = 3;
            this.printerButton.Text = "Drukarka";
            this.printerButton.UseVisualStyleBackColor = true;
            this.printerButton.Click += new System.EventHandler(this.printerButton_Click);
            // 
            // res1Label
            // 
            this.res1Label.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.res1Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.res1Label.Location = new System.Drawing.Point(408, 199);
            this.res1Label.Name = "res1Label";
            this.res1Label.Size = new System.Drawing.Size(35, 13);
            this.res1Label.TabIndex = 4;
            // 
            // res2Label
            // 
            this.res2Label.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.res2Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.res2Label.Location = new System.Drawing.Point(367, 199);
            this.res2Label.Name = "res2Label";
            this.res2Label.Size = new System.Drawing.Size(35, 13);
            this.res2Label.TabIndex = 5;
            // 
            // res3Label
            // 
            this.res3Label.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.res3Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.res3Label.Location = new System.Drawing.Point(326, 199);
            this.res3Label.Name = "res3Label";
            this.res3Label.Size = new System.Drawing.Size(35, 13);
            this.res3Label.TabIndex = 6;
            // 
            // res4Label
            // 
            this.res4Label.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.res4Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.res4Label.Location = new System.Drawing.Point(285, 199);
            this.res4Label.Name = "res4Label";
            this.res4Label.Size = new System.Drawing.Size(35, 13);
            this.res4Label.TabIndex = 7;
            // 
            // infoLabel
            // 
            this.infoLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.infoLabel.Location = new System.Drawing.Point(0, 0);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(728, 50);
            this.infoLabel.TabIndex = 8;
            this.infoLabel.Text = "Proszę czekać. Trwa weryfikacja urządzeń";
            this.infoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CheckHardwareDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.ClientSize = new System.Drawing.Size(728, 502);
            this.Name = "CheckHardwareDialog";
            this.Text = "Status urządzeń";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CheckHardwareDialog_FormClosing);
            this.Load += new System.EventHandler(this.CheckHardwareDialog_Load);
            this.bottomPanel.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button printerButton;
        private System.Windows.Forms.Button modbusButton;
        private System.Windows.Forms.Button socketButton;
        private System.Windows.Forms.Button plcButton;
        private System.Windows.Forms.Label res4Label;
        private System.Windows.Forms.Label res3Label;
        private System.Windows.Forms.Label res2Label;
        private System.Windows.Forms.Label res1Label;
        private System.Windows.Forms.Label infoLabel;
    }
}
