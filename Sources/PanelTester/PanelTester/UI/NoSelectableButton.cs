﻿using System.Windows.Forms;

namespace PanelTester.UI
{
    internal class NoSelectableButton : Button
    {
        public NoSelectableButton()
        {
            SetStyle(ControlStyles.Selectable, false);
        }
    }
}