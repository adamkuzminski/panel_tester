﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace PanelTester.UI
{
    [Designer(typeof(ControlDesigner))]
    public class InheritedDataGridView : DataGridView { }
}