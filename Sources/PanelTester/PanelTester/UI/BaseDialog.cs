﻿using PanelTester.Common;
using System;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class BaseDialog : BaseForm
    {
        protected bool readOnly;

        public BaseDialog()
        {
            InitializeComponent();
        }

        public BaseDialog(bool readOnly = false)
        {
            this.readOnly = readOnly;
            InitializeComponent();
            if (readOnly)
            {
                mainPanel.BackColor = Consts.BackReadOnlyColor;
                cancelButton.Visible = false;
            }
            else if (IsKeyboardRequired())
            {
                Utils.ShowKeyboard();
            }
        }

        public void DisableControls()
        {
            DisableControls(mainPanel);
        }

        public void DisableControls(Control control)
        {
            if (control is ToolStrip)
            {
                foreach (ToolStripItem c in (control as ToolStrip).Items)
                    c.Enabled = false;
            }
            else
            {
                foreach (Control c in control.Controls)
                {
                    if (c is DataGridView)
                        (c as DataGridView).ReadOnly = true;
                    else if (c is TextBox)
                        (c as TextBox).ReadOnly = true;
                    else if (c is NumericUpDown)
                        (c as NumericUpDown).ReadOnly = true;
                    else
                        c.Enabled = false;

                    DisableControls(c);
                }
            }
        }

        public virtual bool SaveData(out String invalidField)
        {
            invalidField = "";
            return false;
        }

        protected virtual bool IsKeyboardRequired()
        {
            return false;
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        private void okButton_Click(object sender, System.EventArgs e)
        {
            if (readOnly)
            {
                DialogResult = DialogResult.Ignore;
            }
            else
            {
                String invalidField;
                if (SaveData(out invalidField))
                    DialogResult = DialogResult.OK;
                else if ((invalidField != null) && (invalidField.Length > 0))
                    MessageBox.Show("Niewłaściwa wartość w polu: " + invalidField + "\nUzupełnij dane", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}