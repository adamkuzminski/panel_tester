﻿namespace PanelTester.UI
{
    partial class TestCaseEditorDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestCaseEditorDialog));
            this.testCaseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.boardBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.socketPinBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.boardRegistryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.testCaseStepOutputBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.testCaseStepInputBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl = new System.Windows.Forms.TabControl();
            this.mainDataTabPage = new System.Windows.Forms.TabPage();
            this.productNameTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.resistanceComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.createDateTextBox = new System.Windows.Forms.TextBox();
            this.boardComboBox = new System.Windows.Forms.ComboBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.commentTextBox = new System.Windows.Forms.TextBox();
            this.activeCheckBox = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.authorTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.versionTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelDocNumberTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.modelNumberTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.socketTabPage = new System.Windows.Forms.TabPage();
            this.socketPinDataGridView = new System.Windows.Forms.DataGridView();
            this.numberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inUseDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.pinTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.captionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.socketTypeToolStrip = new System.Windows.Forms.ToolStrip();
            this.socketTypeToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.registryTabPage = new System.Windows.Forms.TabPage();
            this.boardRegistryDataGridView = new System.Windows.Forms.DataGridView();
            this.inUseDataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.symbolDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.captionDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boardRegistryToolStrip = new System.Windows.Forms.ToolStrip();
            this.boardRegistryTypeToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.refreshBoardToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.powerTabPage = new System.Windows.Forms.TabPage();
            this.powerDownButton = new System.Windows.Forms.Button();
            this.powerUpButton = new System.Windows.Forms.Button();
            this.powerRemoveButton = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.powerTargetListBox = new System.Windows.Forms.ListBox();
            this.powerAddButton = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.powerSourceListBox = new System.Windows.Forms.ListBox();
            this.stepsTabPage = new System.Windows.Forms.TabPage();
            this.stepSplitContainer = new System.Windows.Forms.SplitContainer();
            this.stepOutputDataGridView = new System.Windows.Forms.DataGridView();
            this.inUseDataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.labelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stepOutputPanel = new System.Windows.Forms.Panel();
            this.stepOutputValueNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.stepOutputValueLabel = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.stepOutputNameLabel = new System.Windows.Forms.Label();
            this.stepInputDataGridView = new System.Windows.Forms.DataGridView();
            this.labelDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.powerStepInputColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.stepInputPanel = new System.Windows.Forms.Panel();
            this.stepInputLogicValueCheckBox = new System.Windows.Forms.CheckBox();
            this.stepInputMaxValueNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.stepInputMinValueNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.stepInputImageButton = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.stepInputErrorTextBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.stepInputFatalErrorCheckBox = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.stepInputValueLabel = new System.Windows.Forms.Label();
            this.stepInputPowerModeLabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.stepInputNameLabel = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.stepToolStrip = new System.Windows.Forms.ToolStrip();
            this.addStepToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copyStepToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.deleteStepToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.prevStepToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.stepInfoToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.nextStepToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.stepDelayToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.stepErrorMessageToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.stepErrorImageToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.bottomPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testCaseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.socketPinBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardRegistryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testCaseStepOutputBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testCaseStepInputBindingSource)).BeginInit();
            this.tabControl.SuspendLayout();
            this.mainDataTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.socketTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.socketPinDataGridView)).BeginInit();
            this.socketTypeToolStrip.SuspendLayout();
            this.registryTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boardRegistryDataGridView)).BeginInit();
            this.boardRegistryToolStrip.SuspendLayout();
            this.powerTabPage.SuspendLayout();
            this.stepsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepSplitContainer)).BeginInit();
            this.stepSplitContainer.Panel1.SuspendLayout();
            this.stepSplitContainer.Panel2.SuspendLayout();
            this.stepSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepOutputDataGridView)).BeginInit();
            this.stepOutputPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepOutputValueNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepInputDataGridView)).BeginInit();
            this.stepInputPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepInputMaxValueNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepInputMinValueNumericUpDown)).BeginInit();
            this.stepToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(0, 728);
            this.bottomPanel.Size = new System.Drawing.Size(947, 61);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(823, 4);
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(703, 4);
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.tabControl);
            this.mainPanel.Size = new System.Drawing.Size(947, 728);
            // 
            // testCaseBindingSource
            // 
            this.testCaseBindingSource.DataSource = typeof(PanelTester.Data.TestCase);
            // 
            // boardBindingSource
            // 
            this.boardBindingSource.DataSource = typeof(PanelTester.Data.Board);
            // 
            // socketPinBindingSource
            // 
            this.socketPinBindingSource.DataSource = typeof(PanelTester.Data.SocketPin);
            // 
            // boardRegistryBindingSource
            // 
            this.boardRegistryBindingSource.DataSource = typeof(PanelTester.Data.BoardRegistry);
            // 
            // testCaseStepOutputBindingSource
            // 
            this.testCaseStepOutputBindingSource.DataSource = typeof(PanelTester.Data.TestCaseStepOutput);
            // 
            // testCaseStepInputBindingSource
            // 
            this.testCaseStepInputBindingSource.DataSource = typeof(PanelTester.Data.TestCaseStepInput);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.mainDataTabPage);
            this.tabControl.Controls.Add(this.socketTabPage);
            this.tabControl.Controls.Add(this.registryTabPage);
            this.tabControl.Controls.Add(this.powerTabPage);
            this.tabControl.Controls.Add(this.stepsTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(947, 728);
            this.tabControl.TabIndex = 1;
            this.tabControl.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl_Selecting);
            this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl_Selected);
            this.tabControl.Deselecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl_Deselecting);
            // 
            // mainDataTabPage
            // 
            this.mainDataTabPage.AutoScroll = true;
            this.mainDataTabPage.Controls.Add(this.productNameTextBox);
            this.mainDataTabPage.Controls.Add(this.label7);
            this.mainDataTabPage.Controls.Add(this.resistanceComboBox);
            this.mainDataTabPage.Controls.Add(this.label11);
            this.mainDataTabPage.Controls.Add(this.createDateTextBox);
            this.mainDataTabPage.Controls.Add(this.boardComboBox);
            this.mainDataTabPage.Controls.Add(this.pictureBox);
            this.mainDataTabPage.Controls.Add(this.label10);
            this.mainDataTabPage.Controls.Add(this.commentTextBox);
            this.mainDataTabPage.Controls.Add(this.activeCheckBox);
            this.mainDataTabPage.Controls.Add(this.label9);
            this.mainDataTabPage.Controls.Add(this.authorTextBox);
            this.mainDataTabPage.Controls.Add(this.label6);
            this.mainDataTabPage.Controls.Add(this.versionTextBox);
            this.mainDataTabPage.Controls.Add(this.label5);
            this.mainDataTabPage.Controls.Add(this.nameTextBox);
            this.mainDataTabPage.Controls.Add(this.label4);
            this.mainDataTabPage.Controls.Add(this.label3);
            this.mainDataTabPage.Controls.Add(this.panelDocNumberTextBox);
            this.mainDataTabPage.Controls.Add(this.label2);
            this.mainDataTabPage.Controls.Add(this.modelNumberTextBox);
            this.mainDataTabPage.Controls.Add(this.label1);
            this.mainDataTabPage.Location = new System.Drawing.Point(4, 29);
            this.mainDataTabPage.Name = "mainDataTabPage";
            this.mainDataTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.mainDataTabPage.Size = new System.Drawing.Size(939, 695);
            this.mainDataTabPage.TabIndex = 0;
            this.mainDataTabPage.Text = "Dane podstawowe";
            this.mainDataTabPage.UseVisualStyleBackColor = true;
            // 
            // productNameTextBox
            // 
            this.productNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testCaseBindingSource, "ProductName", true));
            this.productNameTextBox.Location = new System.Drawing.Point(230, 134);
            this.productNameTextBox.MaxLength = 100;
            this.productNameTextBox.Name = "productNameTextBox";
            this.productNameTextBox.Size = new System.Drawing.Size(200, 26);
            this.productNameTextBox.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(216, 26);
            this.label7.TabIndex = 26;
            this.label7.Text = "Nazwa produktu";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // resistanceComboBox
            // 
            this.resistanceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resistanceComboBox.FormattingEnabled = true;
            this.resistanceComboBox.Items.AddRange(new object[] {
            "100",
            "220",
            "470",
            "680",
            "1000",
            "2200",
            "4700",
            "8200"});
            this.resistanceComboBox.Location = new System.Drawing.Point(230, 200);
            this.resistanceComboBox.Name = "resistanceComboBox";
            this.resistanceComboBox.Size = new System.Drawing.Size(200, 28);
            this.resistanceComboBox.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(8, 348);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(216, 26);
            this.label11.TabIndex = 25;
            this.label11.Text = "Data utworzenia";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // createDateTextBox
            // 
            this.createDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testCaseBindingSource, "CreateTime", true));
            this.createDateTextBox.Location = new System.Drawing.Point(230, 348);
            this.createDateTextBox.Name = "createDateTextBox";
            this.createDateTextBox.ReadOnly = true;
            this.createDateTextBox.Size = new System.Drawing.Size(200, 26);
            this.createDateTextBox.TabIndex = 9;
            this.createDateTextBox.TabStop = false;
            // 
            // boardComboBox
            // 
            this.boardComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testCaseBindingSource, "BoardName", true));
            this.boardComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.testCaseBindingSource, "BoardName", true));
            this.boardComboBox.DataSource = this.boardBindingSource;
            this.boardComboBox.DisplayMember = "Name";
            this.boardComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.boardComboBox.FormattingEnabled = true;
            this.boardComboBox.Location = new System.Drawing.Point(230, 166);
            this.boardComboBox.Name = "boardComboBox";
            this.boardComboBox.Size = new System.Drawing.Size(200, 28);
            this.boardComboBox.TabIndex = 5;
            this.boardComboBox.ValueMember = "Name";
            this.boardComboBox.SelectedIndexChanged += new System.EventHandler(this.boardComboBox_SelectedIndexChanged);
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox.Location = new System.Drawing.Point(502, 6);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(200, 200);
            this.pictureBox.TabIndex = 22;
            this.pictureBox.TabStop = false;
            this.pictureBox.Visible = false;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(8, 232);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(216, 26);
            this.label10.TabIndex = 21;
            this.label10.Text = "Komentarz";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // commentTextBox
            // 
            this.commentTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testCaseBindingSource, "Comment", true));
            this.commentTextBox.Location = new System.Drawing.Point(230, 232);
            this.commentTextBox.Multiline = true;
            this.commentTextBox.Name = "commentTextBox";
            this.commentTextBox.Size = new System.Drawing.Size(200, 78);
            this.commentTextBox.TabIndex = 7;
            // 
            // activeCheckBox
            // 
            this.activeCheckBox.AutoSize = true;
            this.activeCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.testCaseBindingSource, "IsActive", true));
            this.activeCheckBox.Location = new System.Drawing.Point(230, 380);
            this.activeCheckBox.Name = "activeCheckBox";
            this.activeCheckBox.Size = new System.Drawing.Size(86, 24);
            this.activeCheckBox.TabIndex = 10;
            this.activeCheckBox.Text = "Aktywny";
            this.activeCheckBox.UseVisualStyleBackColor = true;
            this.activeCheckBox.CheckedChanged += new System.EventHandler(this.activeCheckBox_CheckedChanged);
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(8, 200);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(216, 26);
            this.label9.TabIndex = 18;
            this.label9.Text = "Wartość rezystacji wtyczek";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // authorTextBox
            // 
            this.authorTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testCaseBindingSource, "Author", true));
            this.authorTextBox.Location = new System.Drawing.Point(230, 316);
            this.authorTextBox.Name = "authorTextBox";
            this.authorTextBox.ReadOnly = true;
            this.authorTextBox.Size = new System.Drawing.Size(200, 26);
            this.authorTextBox.TabIndex = 8;
            this.authorTextBox.TabStop = false;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 316);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(216, 26);
            this.label6.TabIndex = 10;
            this.label6.Text = "Autor";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // versionTextBox
            // 
            this.versionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testCaseBindingSource, "Version", true));
            this.versionTextBox.Location = new System.Drawing.Point(230, 38);
            this.versionTextBox.Name = "versionTextBox";
            this.versionTextBox.ReadOnly = true;
            this.versionTextBox.Size = new System.Drawing.Size(100, 26);
            this.versionTextBox.TabIndex = 1;
            this.versionTextBox.TabStop = false;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(216, 26);
            this.label5.TabIndex = 8;
            this.label5.Text = "Wersja";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testCaseBindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(230, 6);
            this.nameTextBox.MaxLength = 100;
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(200, 26);
            this.nameTextBox.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(216, 26);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nazwa";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(216, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "Płyta główna";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelDocNumberTextBox
            // 
            this.panelDocNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testCaseBindingSource, "ModelDocNumber", true));
            this.panelDocNumberTextBox.Location = new System.Drawing.Point(230, 102);
            this.panelDocNumberTextBox.MaxLength = 100;
            this.panelDocNumberTextBox.Name = "panelDocNumberTextBox";
            this.panelDocNumberTextBox.Size = new System.Drawing.Size(200, 26);
            this.panelDocNumberTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(216, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Numer dokumentacji";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // modelNumberTextBox
            // 
            this.modelNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testCaseBindingSource, "Model", true));
            this.modelNumberTextBox.Location = new System.Drawing.Point(230, 70);
            this.modelNumberTextBox.Name = "modelNumberTextBox";
            this.modelNumberTextBox.ReadOnly = true;
            this.modelNumberTextBox.Size = new System.Drawing.Size(200, 26);
            this.modelNumberTextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numer modelu";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // socketTabPage
            // 
            this.socketTabPage.Controls.Add(this.socketPinDataGridView);
            this.socketTabPage.Controls.Add(this.socketTypeToolStrip);
            this.socketTabPage.Location = new System.Drawing.Point(4, 29);
            this.socketTabPage.Name = "socketTabPage";
            this.socketTabPage.Size = new System.Drawing.Size(939, 695);
            this.socketTabPage.TabIndex = 1;
            this.socketTabPage.Text = "Złącza";
            this.socketTabPage.UseVisualStyleBackColor = true;
            // 
            // socketPinDataGridView
            // 
            this.socketPinDataGridView.AllowUserToAddRows = false;
            this.socketPinDataGridView.AllowUserToDeleteRows = false;
            this.socketPinDataGridView.AllowUserToResizeColumns = false;
            this.socketPinDataGridView.AllowUserToResizeRows = false;
            this.socketPinDataGridView.AutoGenerateColumns = false;
            this.socketPinDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.socketPinDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numberDataGridViewTextBoxColumn,
            this.inUseDataGridViewCheckBoxColumn,
            this.pinTypeDataGridViewTextBoxColumn,
            this.captionDataGridViewTextBoxColumn});
            this.socketPinDataGridView.DataSource = this.socketPinBindingSource;
            this.socketPinDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.socketPinDataGridView.Location = new System.Drawing.Point(0, 25);
            this.socketPinDataGridView.MultiSelect = false;
            this.socketPinDataGridView.Name = "socketPinDataGridView";
            this.socketPinDataGridView.RowHeadersVisible = false;
            this.socketPinDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.socketPinDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.socketPinDataGridView.ShowCellErrors = false;
            this.socketPinDataGridView.ShowCellToolTips = false;
            this.socketPinDataGridView.ShowEditingIcon = false;
            this.socketPinDataGridView.ShowRowErrors = false;
            this.socketPinDataGridView.Size = new System.Drawing.Size(939, 670);
            this.socketPinDataGridView.TabIndex = 0;
            this.socketPinDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.socketPinDataGridView_CellFormatting);
            this.socketPinDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.socketPinDataGridView_CellValueChanged);
            this.socketPinDataGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.socketPinDataGridView_CurrentCellDirtyStateChanged);
            this.socketPinDataGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.socketPinDataGridView_RowsAdded);
            // 
            // numberDataGridViewTextBoxColumn
            // 
            this.numberDataGridViewTextBoxColumn.DataPropertyName = "Number";
            this.numberDataGridViewTextBoxColumn.HeaderText = "Numer";
            this.numberDataGridViewTextBoxColumn.Name = "numberDataGridViewTextBoxColumn";
            this.numberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // inUseDataGridViewCheckBoxColumn
            // 
            this.inUseDataGridViewCheckBoxColumn.DataPropertyName = "InUse";
            this.inUseDataGridViewCheckBoxColumn.HeaderText = "W użyciu";
            this.inUseDataGridViewCheckBoxColumn.Name = "inUseDataGridViewCheckBoxColumn";
            // 
            // pinTypeDataGridViewTextBoxColumn
            // 
            this.pinTypeDataGridViewTextBoxColumn.DataPropertyName = "PinType";
            this.pinTypeDataGridViewTextBoxColumn.HeaderText = "Typ pinu";
            this.pinTypeDataGridViewTextBoxColumn.Name = "pinTypeDataGridViewTextBoxColumn";
            this.pinTypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.pinTypeDataGridViewTextBoxColumn.Width = 200;
            // 
            // captionDataGridViewTextBoxColumn
            // 
            this.captionDataGridViewTextBoxColumn.DataPropertyName = "Caption";
            this.captionDataGridViewTextBoxColumn.HeaderText = "Etykieta";
            this.captionDataGridViewTextBoxColumn.Name = "captionDataGridViewTextBoxColumn";
            this.captionDataGridViewTextBoxColumn.Width = 300;
            // 
            // socketTypeToolStrip
            // 
            this.socketTypeToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.socketTypeToolStripComboBox});
            this.socketTypeToolStrip.Location = new System.Drawing.Point(0, 0);
            this.socketTypeToolStrip.Name = "socketTypeToolStrip";
            this.socketTypeToolStrip.Size = new System.Drawing.Size(939, 25);
            this.socketTypeToolStrip.TabIndex = 1;
            // 
            // socketTypeToolStripComboBox
            // 
            this.socketTypeToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.socketTypeToolStripComboBox.DropDownWidth = 200;
            this.socketTypeToolStripComboBox.Items.AddRange(new object[] {
            "Złącze 1 - AC OUT",
            "Złącze 2 - AC IN",
            "Złącze 3 - DC OUT",
            "Złącze 4 - DC IN"});
            this.socketTypeToolStripComboBox.Name = "socketTypeToolStripComboBox";
            this.socketTypeToolStripComboBox.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.socketTypeToolStripComboBox.Size = new System.Drawing.Size(200, 25);
            this.socketTypeToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.socketTypeToolStripComboBox_SelectedIndexChanged);
            // 
            // registryTabPage
            // 
            this.registryTabPage.Controls.Add(this.boardRegistryDataGridView);
            this.registryTabPage.Controls.Add(this.boardRegistryToolStrip);
            this.registryTabPage.Location = new System.Drawing.Point(4, 29);
            this.registryTabPage.Name = "registryTabPage";
            this.registryTabPage.Size = new System.Drawing.Size(720, 408);
            this.registryTabPage.TabIndex = 2;
            this.registryTabPage.Text = "Rejestry";
            this.registryTabPage.UseVisualStyleBackColor = true;
            // 
            // boardRegistryDataGridView
            // 
            this.boardRegistryDataGridView.AllowUserToAddRows = false;
            this.boardRegistryDataGridView.AllowUserToDeleteRows = false;
            this.boardRegistryDataGridView.AllowUserToResizeColumns = false;
            this.boardRegistryDataGridView.AllowUserToResizeRows = false;
            this.boardRegistryDataGridView.AutoGenerateColumns = false;
            this.boardRegistryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.boardRegistryDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.inUseDataGridViewCheckBoxColumn1,
            this.symbolDataGridViewTextBoxColumn,
            this.captionDataGridViewTextBoxColumn1});
            this.boardRegistryDataGridView.DataSource = this.boardRegistryBindingSource;
            this.boardRegistryDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.boardRegistryDataGridView.Location = new System.Drawing.Point(0, 25);
            this.boardRegistryDataGridView.MultiSelect = false;
            this.boardRegistryDataGridView.Name = "boardRegistryDataGridView";
            this.boardRegistryDataGridView.RowHeadersVisible = false;
            this.boardRegistryDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.boardRegistryDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.boardRegistryDataGridView.ShowCellErrors = false;
            this.boardRegistryDataGridView.ShowCellToolTips = false;
            this.boardRegistryDataGridView.ShowEditingIcon = false;
            this.boardRegistryDataGridView.ShowRowErrors = false;
            this.boardRegistryDataGridView.Size = new System.Drawing.Size(720, 383);
            this.boardRegistryDataGridView.TabIndex = 2;
            this.boardRegistryDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.boardRegistryDataGridView_CellFormatting);
            this.boardRegistryDataGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.boardRegistryDataGridView_CurrentCellDirtyStateChanged);
            // 
            // inUseDataGridViewCheckBoxColumn1
            // 
            this.inUseDataGridViewCheckBoxColumn1.DataPropertyName = "InUse";
            this.inUseDataGridViewCheckBoxColumn1.HeaderText = "W użyciu";
            this.inUseDataGridViewCheckBoxColumn1.Name = "inUseDataGridViewCheckBoxColumn1";
            // 
            // symbolDataGridViewTextBoxColumn
            // 
            this.symbolDataGridViewTextBoxColumn.DataPropertyName = "Symbol";
            this.symbolDataGridViewTextBoxColumn.HeaderText = "Symbol";
            this.symbolDataGridViewTextBoxColumn.Name = "symbolDataGridViewTextBoxColumn";
            this.symbolDataGridViewTextBoxColumn.ReadOnly = true;
            this.symbolDataGridViewTextBoxColumn.Width = 200;
            // 
            // captionDataGridViewTextBoxColumn1
            // 
            this.captionDataGridViewTextBoxColumn1.DataPropertyName = "Caption";
            this.captionDataGridViewTextBoxColumn1.HeaderText = "Etykieta";
            this.captionDataGridViewTextBoxColumn1.Name = "captionDataGridViewTextBoxColumn1";
            this.captionDataGridViewTextBoxColumn1.ReadOnly = true;
            this.captionDataGridViewTextBoxColumn1.Width = 300;
            // 
            // boardRegistryToolStrip
            // 
            this.boardRegistryToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.boardRegistryTypeToolStripComboBox,
            this.refreshBoardToolStripButton});
            this.boardRegistryToolStrip.Location = new System.Drawing.Point(0, 0);
            this.boardRegistryToolStrip.Name = "boardRegistryToolStrip";
            this.boardRegistryToolStrip.Size = new System.Drawing.Size(720, 25);
            this.boardRegistryToolStrip.TabIndex = 3;
            // 
            // boardRegistryTypeToolStripComboBox
            // 
            this.boardRegistryTypeToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.boardRegistryTypeToolStripComboBox.DropDownWidth = 200;
            this.boardRegistryTypeToolStripComboBox.Items.AddRange(new object[] {
            "AOut - wyjścia analogowe",
            "AIn - wejścia analogowe",
            "DOut - wyjścia cyfrowe",
            "DIn - wejścia cyfrowe"});
            this.boardRegistryTypeToolStripComboBox.Name = "boardRegistryTypeToolStripComboBox";
            this.boardRegistryTypeToolStripComboBox.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.boardRegistryTypeToolStripComboBox.Size = new System.Drawing.Size(200, 25);
            this.boardRegistryTypeToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.boardRegistryTypeToolStripComboBox_SelectedIndexChanged);
            // 
            // refreshBoardToolStripButton
            // 
            this.refreshBoardToolStripButton.Image = global::PanelTester.Properties.Resources.Repeat;
            this.refreshBoardToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshBoardToolStripButton.Name = "refreshBoardToolStripButton";
            this.refreshBoardToolStripButton.Size = new System.Drawing.Size(100, 22);
            this.refreshBoardToolStripButton.Text = "Odśwież płytę";
            this.refreshBoardToolStripButton.Click += new System.EventHandler(this.refreshBoardToolStripButton_Click);
            // 
            // powerTabPage
            // 
            this.powerTabPage.Controls.Add(this.powerDownButton);
            this.powerTabPage.Controls.Add(this.powerUpButton);
            this.powerTabPage.Controls.Add(this.powerRemoveButton);
            this.powerTabPage.Controls.Add(this.label14);
            this.powerTabPage.Controls.Add(this.powerTargetListBox);
            this.powerTabPage.Controls.Add(this.powerAddButton);
            this.powerTabPage.Controls.Add(this.label13);
            this.powerTabPage.Controls.Add(this.powerSourceListBox);
            this.powerTabPage.Location = new System.Drawing.Point(4, 29);
            this.powerTabPage.Name = "powerTabPage";
            this.powerTabPage.Size = new System.Drawing.Size(720, 408);
            this.powerTabPage.TabIndex = 3;
            this.powerTabPage.Text = "Zasilanie";
            this.powerTabPage.UseVisualStyleBackColor = true;
            // 
            // powerDownButton
            // 
            this.powerDownButton.Image = global::PanelTester.Properties.Resources.Down;
            this.powerDownButton.Location = new System.Drawing.Point(635, 399);
            this.powerDownButton.Name = "powerDownButton";
            this.powerDownButton.Size = new System.Drawing.Size(75, 43);
            this.powerDownButton.TabIndex = 7;
            this.powerDownButton.UseVisualStyleBackColor = true;
            this.powerDownButton.Click += new System.EventHandler(this.powerDownButton_Click);
            // 
            // powerUpButton
            // 
            this.powerUpButton.Image = global::PanelTester.Properties.Resources.Up;
            this.powerUpButton.Location = new System.Drawing.Point(554, 399);
            this.powerUpButton.Name = "powerUpButton";
            this.powerUpButton.Size = new System.Drawing.Size(75, 43);
            this.powerUpButton.TabIndex = 6;
            this.powerUpButton.UseVisualStyleBackColor = true;
            this.powerUpButton.Click += new System.EventHandler(this.powerUpButton_Click);
            // 
            // powerRemoveButton
            // 
            this.powerRemoveButton.Image = global::PanelTester.Properties.Resources.Left;
            this.powerRemoveButton.Location = new System.Drawing.Point(432, 216);
            this.powerRemoveButton.Name = "powerRemoveButton";
            this.powerRemoveButton.Size = new System.Drawing.Size(75, 43);
            this.powerRemoveButton.TabIndex = 5;
            this.powerRemoveButton.UseVisualStyleBackColor = true;
            this.powerRemoveButton.Click += new System.EventHandler(this.powerRemoveButton_Click);
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.LightBlue;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label14.Location = new System.Drawing.Point(513, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(236, 30);
            this.label14.TabIndex = 4;
            this.label14.Text = "Wybrane warianty zasilania";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // powerTargetListBox
            // 
            this.powerTargetListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.powerTargetListBox.FormattingEnabled = true;
            this.powerTargetListBox.ItemHeight = 20;
            this.powerTargetListBox.Location = new System.Drawing.Point(513, 49);
            this.powerTargetListBox.Name = "powerTargetListBox";
            this.powerTargetListBox.Size = new System.Drawing.Size(236, 344);
            this.powerTargetListBox.TabIndex = 3;
            this.powerTargetListBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.powerListBox_DrawItem);
            this.powerTargetListBox.SelectedIndexChanged += new System.EventHandler(this.powerTargetListBox_SelectedIndexChanged);
            // 
            // powerAddButton
            // 
            this.powerAddButton.Image = global::PanelTester.Properties.Resources.Right;
            this.powerAddButton.Location = new System.Drawing.Point(432, 167);
            this.powerAddButton.Name = "powerAddButton";
            this.powerAddButton.Size = new System.Drawing.Size(75, 43);
            this.powerAddButton.TabIndex = 2;
            this.powerAddButton.UseVisualStyleBackColor = true;
            this.powerAddButton.Click += new System.EventHandler(this.powerAddButton_Click);
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.LightBlue;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label13.Location = new System.Drawing.Point(190, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(236, 30);
            this.label13.TabIndex = 1;
            this.label13.Text = "Dostępne warianty";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // powerSourceListBox
            // 
            this.powerSourceListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.powerSourceListBox.FormattingEnabled = true;
            this.powerSourceListBox.ItemHeight = 20;
            this.powerSourceListBox.Location = new System.Drawing.Point(190, 49);
            this.powerSourceListBox.Name = "powerSourceListBox";
            this.powerSourceListBox.Size = new System.Drawing.Size(236, 344);
            this.powerSourceListBox.TabIndex = 0;
            this.powerSourceListBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.powerListBox_DrawItem);
            this.powerSourceListBox.SelectedIndexChanged += new System.EventHandler(this.powerSourceListBox_SelectedIndexChanged);
            // 
            // stepsTabPage
            // 
            this.stepsTabPage.Controls.Add(this.stepSplitContainer);
            this.stepsTabPage.Controls.Add(this.stepToolStrip);
            this.stepsTabPage.Location = new System.Drawing.Point(4, 29);
            this.stepsTabPage.Name = "stepsTabPage";
            this.stepsTabPage.Size = new System.Drawing.Size(720, 408);
            this.stepsTabPage.TabIndex = 5;
            this.stepsTabPage.Text = "Kroki";
            this.stepsTabPage.UseVisualStyleBackColor = true;
            // 
            // stepSplitContainer
            // 
            this.stepSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stepSplitContainer.Location = new System.Drawing.Point(0, 25);
            this.stepSplitContainer.Name = "stepSplitContainer";
            // 
            // stepSplitContainer.Panel1
            // 
            this.stepSplitContainer.Panel1.Controls.Add(this.stepOutputDataGridView);
            this.stepSplitContainer.Panel1.Controls.Add(this.stepOutputPanel);
            // 
            // stepSplitContainer.Panel2
            // 
            this.stepSplitContainer.Panel2.Controls.Add(this.stepInputDataGridView);
            this.stepSplitContainer.Panel2.Controls.Add(this.stepInputPanel);
            this.stepSplitContainer.Size = new System.Drawing.Size(720, 383);
            this.stepSplitContainer.SplitterDistance = 240;
            this.stepSplitContainer.TabIndex = 2;
            // 
            // stepOutputDataGridView
            // 
            this.stepOutputDataGridView.AllowUserToAddRows = false;
            this.stepOutputDataGridView.AllowUserToDeleteRows = false;
            this.stepOutputDataGridView.AllowUserToResizeColumns = false;
            this.stepOutputDataGridView.AllowUserToResizeRows = false;
            this.stepOutputDataGridView.AutoGenerateColumns = false;
            this.stepOutputDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.stepOutputDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.inUseDataGridViewCheckBoxColumn2,
            this.labelDataGridViewTextBoxColumn});
            this.stepOutputDataGridView.DataSource = this.testCaseStepOutputBindingSource;
            this.stepOutputDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stepOutputDataGridView.Location = new System.Drawing.Point(0, 0);
            this.stepOutputDataGridView.MultiSelect = false;
            this.stepOutputDataGridView.Name = "stepOutputDataGridView";
            this.stepOutputDataGridView.RowHeadersVisible = false;
            this.stepOutputDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.stepOutputDataGridView.ShowCellErrors = false;
            this.stepOutputDataGridView.ShowCellToolTips = false;
            this.stepOutputDataGridView.ShowEditingIcon = false;
            this.stepOutputDataGridView.ShowRowErrors = false;
            this.stepOutputDataGridView.Size = new System.Drawing.Size(240, 313);
            this.stepOutputDataGridView.TabIndex = 0;
            this.stepOutputDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.stepOutputDataGridView_CellFormatting);
            this.stepOutputDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.stepOutputDataGridView_CellValueChanged);
            this.stepOutputDataGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.stepOutputDataGridView_CurrentCellDirtyStateChanged);
            this.stepOutputDataGridView.SelectionChanged += new System.EventHandler(this.stepOutputDataGridView_SelectionChanged);
            // 
            // inUseDataGridViewCheckBoxColumn2
            // 
            this.inUseDataGridViewCheckBoxColumn2.DataPropertyName = "InUse";
            this.inUseDataGridViewCheckBoxColumn2.HeaderText = "W użyciu";
            this.inUseDataGridViewCheckBoxColumn2.Name = "inUseDataGridViewCheckBoxColumn2";
            // 
            // labelDataGridViewTextBoxColumn
            // 
            this.labelDataGridViewTextBoxColumn.DataPropertyName = "Label";
            this.labelDataGridViewTextBoxColumn.HeaderText = "Wyjście";
            this.labelDataGridViewTextBoxColumn.Name = "labelDataGridViewTextBoxColumn";
            this.labelDataGridViewTextBoxColumn.ReadOnly = true;
            this.labelDataGridViewTextBoxColumn.Width = 200;
            // 
            // stepOutputPanel
            // 
            this.stepOutputPanel.BackColor = System.Drawing.Color.LemonChiffon;
            this.stepOutputPanel.Controls.Add(this.stepOutputValueNumericUpDown);
            this.stepOutputPanel.Controls.Add(this.stepOutputValueLabel);
            this.stepOutputPanel.Controls.Add(this.label15);
            this.stepOutputPanel.Controls.Add(this.stepOutputNameLabel);
            this.stepOutputPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.stepOutputPanel.Location = new System.Drawing.Point(0, 313);
            this.stepOutputPanel.Name = "stepOutputPanel";
            this.stepOutputPanel.Size = new System.Drawing.Size(240, 70);
            this.stepOutputPanel.TabIndex = 1;
            this.stepOutputPanel.Visible = false;
            // 
            // stepOutputValueNumericUpDown
            // 
            this.stepOutputValueNumericUpDown.DecimalPlaces = 1;
            this.stepOutputValueNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.stepOutputValueNumericUpDown.Location = new System.Drawing.Point(80, 34);
            this.stepOutputValueNumericUpDown.Name = "stepOutputValueNumericUpDown";
            this.stepOutputValueNumericUpDown.Size = new System.Drawing.Size(120, 26);
            this.stepOutputValueNumericUpDown.TabIndex = 4;
            this.stepOutputValueNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // stepOutputValueLabel
            // 
            this.stepOutputValueLabel.AutoSize = true;
            this.stepOutputValueLabel.Location = new System.Drawing.Point(8, 36);
            this.stepOutputValueLabel.Name = "stepOutputValueLabel";
            this.stepOutputValueLabel.Size = new System.Drawing.Size(72, 20);
            this.stepOutputValueLabel.TabIndex = 2;
            this.stepOutputValueLabel.Text = "Wartość:";
            this.stepOutputValueLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 7);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 20);
            this.label15.TabIndex = 1;
            this.label15.Text = "Wyjście:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // stepOutputNameLabel
            // 
            this.stepOutputNameLabel.AutoSize = true;
            this.stepOutputNameLabel.Location = new System.Drawing.Point(76, 7);
            this.stepOutputNameLabel.Name = "stepOutputNameLabel";
            this.stepOutputNameLabel.Size = new System.Drawing.Size(102, 20);
            this.stepOutputNameLabel.TabIndex = 0;
            this.stepOutputNameLabel.Text = "Output name";
            // 
            // stepInputDataGridView
            // 
            this.stepInputDataGridView.AllowUserToAddRows = false;
            this.stepInputDataGridView.AllowUserToDeleteRows = false;
            this.stepInputDataGridView.AllowUserToResizeColumns = false;
            this.stepInputDataGridView.AllowUserToResizeRows = false;
            this.stepInputDataGridView.AutoGenerateColumns = false;
            this.stepInputDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.stepInputDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.labelDataGridViewTextBoxColumn1,
            this.powerStepInputColumn});
            this.stepInputDataGridView.DataSource = this.testCaseStepInputBindingSource;
            this.stepInputDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stepInputDataGridView.Location = new System.Drawing.Point(0, 0);
            this.stepInputDataGridView.MultiSelect = false;
            this.stepInputDataGridView.Name = "stepInputDataGridView";
            this.stepInputDataGridView.RowHeadersVisible = false;
            this.stepInputDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.stepInputDataGridView.ShowCellErrors = false;
            this.stepInputDataGridView.ShowCellToolTips = false;
            this.stepInputDataGridView.ShowEditingIcon = false;
            this.stepInputDataGridView.ShowRowErrors = false;
            this.stepInputDataGridView.Size = new System.Drawing.Size(476, 249);
            this.stepInputDataGridView.TabIndex = 1;
            this.stepInputDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.stepInputDataGridView_CellEnter);
            this.stepInputDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.stepInputDataGridView_CellFormatting);
            this.stepInputDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.stepInputDataGridView_CellValueChanged);
            this.stepInputDataGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.stepInputDataGridView_CurrentCellDirtyStateChanged);
            // 
            // labelDataGridViewTextBoxColumn1
            // 
            this.labelDataGridViewTextBoxColumn1.DataPropertyName = "Label";
            this.labelDataGridViewTextBoxColumn1.Frozen = true;
            this.labelDataGridViewTextBoxColumn1.HeaderText = "Wejście";
            this.labelDataGridViewTextBoxColumn1.Name = "labelDataGridViewTextBoxColumn1";
            this.labelDataGridViewTextBoxColumn1.ReadOnly = true;
            this.labelDataGridViewTextBoxColumn1.Width = 200;
            // 
            // powerStepInputColumn
            // 
            this.powerStepInputColumn.HeaderText = "Zasilanie";
            this.powerStepInputColumn.Name = "powerStepInputColumn";
            this.powerStepInputColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.powerStepInputColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.powerStepInputColumn.Width = 110;
            // 
            // stepInputPanel
            // 
            this.stepInputPanel.BackColor = System.Drawing.Color.LemonChiffon;
            this.stepInputPanel.Controls.Add(this.stepInputLogicValueCheckBox);
            this.stepInputPanel.Controls.Add(this.stepInputMaxValueNumericUpDown);
            this.stepInputPanel.Controls.Add(this.stepInputMinValueNumericUpDown);
            this.stepInputPanel.Controls.Add(this.stepInputImageButton);
            this.stepInputPanel.Controls.Add(this.label24);
            this.stepInputPanel.Controls.Add(this.stepInputErrorTextBox);
            this.stepInputPanel.Controls.Add(this.label23);
            this.stepInputPanel.Controls.Add(this.stepInputFatalErrorCheckBox);
            this.stepInputPanel.Controls.Add(this.label22);
            this.stepInputPanel.Controls.Add(this.stepInputValueLabel);
            this.stepInputPanel.Controls.Add(this.stepInputPowerModeLabel);
            this.stepInputPanel.Controls.Add(this.label18);
            this.stepInputPanel.Controls.Add(this.stepInputNameLabel);
            this.stepInputPanel.Controls.Add(this.label17);
            this.stepInputPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.stepInputPanel.Location = new System.Drawing.Point(0, 249);
            this.stepInputPanel.Name = "stepInputPanel";
            this.stepInputPanel.Size = new System.Drawing.Size(476, 134);
            this.stepInputPanel.TabIndex = 0;
            this.stepInputPanel.Visible = false;
            // 
            // stepInputLogicValueCheckBox
            // 
            this.stepInputLogicValueCheckBox.AutoSize = true;
            this.stepInputLogicValueCheckBox.Location = new System.Drawing.Point(87, 53);
            this.stepInputLogicValueCheckBox.Name = "stepInputLogicValueCheckBox";
            this.stepInputLogicValueCheckBox.Size = new System.Drawing.Size(58, 24);
            this.stepInputLogicValueCheckBox.TabIndex = 17;
            this.stepInputLogicValueCheckBox.Text = "1 / 0";
            this.stepInputLogicValueCheckBox.UseVisualStyleBackColor = true;
            // 
            // stepInputMaxValueNumericUpDown
            // 
            this.stepInputMaxValueNumericUpDown.DecimalPlaces = 1;
            this.stepInputMaxValueNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.stepInputMaxValueNumericUpDown.Location = new System.Drawing.Point(87, 67);
            this.stepInputMaxValueNumericUpDown.Name = "stepInputMaxValueNumericUpDown";
            this.stepInputMaxValueNumericUpDown.Size = new System.Drawing.Size(120, 26);
            this.stepInputMaxValueNumericUpDown.TabIndex = 1;
            this.stepInputMaxValueNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // stepInputMinValueNumericUpDown
            // 
            this.stepInputMinValueNumericUpDown.DecimalPlaces = 1;
            this.stepInputMinValueNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.stepInputMinValueNumericUpDown.Location = new System.Drawing.Point(87, 34);
            this.stepInputMinValueNumericUpDown.Name = "stepInputMinValueNumericUpDown";
            this.stepInputMinValueNumericUpDown.Size = new System.Drawing.Size(120, 26);
            this.stepInputMinValueNumericUpDown.TabIndex = 0;
            this.stepInputMinValueNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // stepInputImageButton
            // 
            this.stepInputImageButton.Location = new System.Drawing.Point(351, 98);
            this.stepInputImageButton.Name = "stepInputImageButton";
            this.stepInputImageButton.Size = new System.Drawing.Size(200, 26);
            this.stepInputImageButton.TabIndex = 4;
            this.stepInputImageButton.UseVisualStyleBackColor = true;
            this.stepInputImageButton.Click += new System.EventHandler(this.stepInputImageButton_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(281, 101);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 20);
            this.label24.TabIndex = 16;
            this.label24.Text = "Zdjęcie:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // stepInputErrorTextBox
            // 
            this.stepInputErrorTextBox.Location = new System.Drawing.Point(351, 66);
            this.stepInputErrorTextBox.Name = "stepInputErrorTextBox";
            this.stepInputErrorTextBox.Size = new System.Drawing.Size(200, 26);
            this.stepInputErrorTextBox.TabIndex = 3;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(257, 69);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(88, 20);
            this.label23.TabIndex = 14;
            this.label23.Text = "Komunikat:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // stepInputFatalErrorCheckBox
            // 
            this.stepInputFatalErrorCheckBox.AutoSize = true;
            this.stepInputFatalErrorCheckBox.Location = new System.Drawing.Point(351, 36);
            this.stepInputFatalErrorCheckBox.Name = "stepInputFatalErrorCheckBox";
            this.stepInputFatalErrorCheckBox.Size = new System.Drawing.Size(126, 24);
            this.stepInputFatalErrorCheckBox.TabIndex = 2;
            this.stepInputFatalErrorCheckBox.Text = "zatrzymaj test";
            this.stepInputFatalErrorCheckBox.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(228, 37);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(117, 20);
            this.label22.TabIndex = 12;
            this.label22.Text = "Obsługa błędu:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // stepInputValueLabel
            // 
            this.stepInputValueLabel.AutoSize = true;
            this.stepInputValueLabel.Location = new System.Drawing.Point(9, 54);
            this.stepInputValueLabel.Name = "stepInputValueLabel";
            this.stepInputValueLabel.Size = new System.Drawing.Size(72, 20);
            this.stepInputValueLabel.TabIndex = 8;
            this.stepInputValueLabel.Text = "Wartość:";
            this.stepInputValueLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // stepInputPowerModeLabel
            // 
            this.stepInputPowerModeLabel.AutoSize = true;
            this.stepInputPowerModeLabel.Location = new System.Drawing.Point(347, 7);
            this.stepInputPowerModeLabel.Name = "stepInputPowerModeLabel";
            this.stepInputPowerModeLabel.Size = new System.Drawing.Size(97, 20);
            this.stepInputPowerModeLabel.TabIndex = 5;
            this.stepInputPowerModeLabel.Text = "Power mode";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(269, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 20);
            this.label18.TabIndex = 4;
            this.label18.Text = "Zasilanie:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // stepInputNameLabel
            // 
            this.stepInputNameLabel.Location = new System.Drawing.Point(83, 7);
            this.stepInputNameLabel.Name = "stepInputNameLabel";
            this.stepInputNameLabel.Size = new System.Drawing.Size(180, 20);
            this.stepInputNameLabel.TabIndex = 3;
            this.stepInputNameLabel.Text = "Input name";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 7);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Wejście:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // stepToolStrip
            // 
            this.stepToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addStepToolStripButton,
            this.copyStepToolStripButton,
            this.deleteStepToolStripButton,
            this.toolStripSeparator1,
            this.prevStepToolStripButton,
            this.stepInfoToolStripLabel,
            this.nextStepToolStripButton,
            this.toolStripSeparator2,
            this.toolStripLabel1,
            this.stepDelayToolStripTextBox,
            this.toolStripLabel2,
            this.stepErrorMessageToolStripTextBox,
            this.stepErrorImageToolStripButton});
            this.stepToolStrip.Location = new System.Drawing.Point(0, 0);
            this.stepToolStrip.Name = "stepToolStrip";
            this.stepToolStrip.Size = new System.Drawing.Size(720, 25);
            this.stepToolStrip.TabIndex = 0;
            this.stepToolStrip.Text = "toolStrip1";
            // 
            // addStepToolStripButton
            // 
            this.addStepToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addStepToolStripButton.Image = global::PanelTester.Properties.Resources.Add;
            this.addStepToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addStepToolStripButton.Name = "addStepToolStripButton";
            this.addStepToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.addStepToolStripButton.Text = "Dodaj krok";
            this.addStepToolStripButton.Click += new System.EventHandler(this.addStepToolStripButton_Click);
            // 
            // copyStepToolStripButton
            // 
            this.copyStepToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyStepToolStripButton.Image = global::PanelTester.Properties.Resources.Copy;
            this.copyStepToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyStepToolStripButton.Name = "copyStepToolStripButton";
            this.copyStepToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.copyStepToolStripButton.Text = "Kopiuj krok";
            this.copyStepToolStripButton.Click += new System.EventHandler(this.copyStepToolStripButton_Click);
            // 
            // deleteStepToolStripButton
            // 
            this.deleteStepToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteStepToolStripButton.Image = global::PanelTester.Properties.Resources.Delete;
            this.deleteStepToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteStepToolStripButton.Name = "deleteStepToolStripButton";
            this.deleteStepToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.deleteStepToolStripButton.Text = "Usuń krok";
            this.deleteStepToolStripButton.Click += new System.EventHandler(this.deleteStepToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // prevStepToolStripButton
            // 
            this.prevStepToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.prevStepToolStripButton.Image = global::PanelTester.Properties.Resources.Left;
            this.prevStepToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.prevStepToolStripButton.Name = "prevStepToolStripButton";
            this.prevStepToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.prevStepToolStripButton.Text = "Poprzedni krok";
            this.prevStepToolStripButton.Click += new System.EventHandler(this.prevStepToolStripButton_Click);
            // 
            // stepInfoToolStripLabel
            // 
            this.stepInfoToolStripLabel.Name = "stepInfoToolStripLabel";
            this.stepInfoToolStripLabel.Size = new System.Drawing.Size(30, 22);
            this.stepInfoToolStripLabel.Text = "1 z 1";
            // 
            // nextStepToolStripButton
            // 
            this.nextStepToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.nextStepToolStripButton.Image = global::PanelTester.Properties.Resources.Right;
            this.nextStepToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.nextStepToolStripButton.Name = "nextStepToolStripButton";
            this.nextStepToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.nextStepToolStripButton.Text = "Następny krok";
            this.nextStepToolStripButton.Click += new System.EventHandler(this.nextStepToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(67, 22);
            this.toolStripLabel1.Text = "Opóźnienie";
            // 
            // stepDelayToolStripTextBox
            // 
            this.stepDelayToolStripTextBox.Name = "stepDelayToolStripTextBox";
            this.stepDelayToolStripTextBox.Size = new System.Drawing.Size(100, 25);
            this.stepDelayToolStripTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.stepDelayToolStripTextBox.TextChanged += new System.EventHandler(this.stepDelayToolStripTextBox_TextChanged);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(98, 22);
            this.toolStripLabel2.Text = "Komunikat błędu";
            // 
            // stepErrorMessageToolStripTextBox
            // 
            this.stepErrorMessageToolStripTextBox.Name = "stepErrorMessageToolStripTextBox";
            this.stepErrorMessageToolStripTextBox.Size = new System.Drawing.Size(100, 25);
            // 
            // stepErrorImageToolStripButton
            // 
            this.stepErrorImageToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stepErrorImageToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("stepErrorImageToolStripButton.Image")));
            this.stepErrorImageToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stepErrorImageToolStripButton.Name = "stepErrorImageToolStripButton";
            this.stepErrorImageToolStripButton.Size = new System.Drawing.Size(134, 22);
            this.stepErrorImageToolStripButton.Text = "Wybierz zdjęcie błędu...";
            this.stepErrorImageToolStripButton.Click += new System.EventHandler(this.stepErrorImageToolStripButton_Click);
            // 
            // TestCaseEditorDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.ClientSize = new System.Drawing.Size(947, 789);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "TestCaseEditorDialog";
            this.Text = "Edytor scenariusza testowego";
            this.Load += new System.EventHandler(this.TestCaseEditorDialog_Load);
            this.bottomPanel.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.testCaseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.socketPinBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardRegistryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testCaseStepOutputBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testCaseStepInputBindingSource)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.mainDataTabPage.ResumeLayout(false);
            this.mainDataTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.socketTabPage.ResumeLayout(false);
            this.socketTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.socketPinDataGridView)).EndInit();
            this.socketTypeToolStrip.ResumeLayout(false);
            this.socketTypeToolStrip.PerformLayout();
            this.registryTabPage.ResumeLayout(false);
            this.registryTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boardRegistryDataGridView)).EndInit();
            this.boardRegistryToolStrip.ResumeLayout(false);
            this.boardRegistryToolStrip.PerformLayout();
            this.powerTabPage.ResumeLayout(false);
            this.stepsTabPage.ResumeLayout(false);
            this.stepsTabPage.PerformLayout();
            this.stepSplitContainer.Panel1.ResumeLayout(false);
            this.stepSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stepSplitContainer)).EndInit();
            this.stepSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stepOutputDataGridView)).EndInit();
            this.stepOutputPanel.ResumeLayout(false);
            this.stepOutputPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepOutputValueNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepInputDataGridView)).EndInit();
            this.stepInputPanel.ResumeLayout(false);
            this.stepInputPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepInputMaxValueNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepInputMinValueNumericUpDown)).EndInit();
            this.stepToolStrip.ResumeLayout(false);
            this.stepToolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource boardBindingSource;
        private System.Windows.Forms.BindingSource testCaseBindingSource;
        private System.Windows.Forms.BindingSource socketPinBindingSource;
        private System.Windows.Forms.BindingSource boardRegistryBindingSource;
        private System.Windows.Forms.BindingSource testCaseStepOutputBindingSource;
        private System.Windows.Forms.BindingSource testCaseStepInputBindingSource;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage mainDataTabPage;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox createDateTextBox;
        private System.Windows.Forms.ComboBox boardComboBox;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox commentTextBox;
        private System.Windows.Forms.CheckBox activeCheckBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox authorTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox versionTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox panelDocNumberTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox modelNumberTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage socketTabPage;
        private System.Windows.Forms.DataGridView socketPinDataGridView;
        private System.Windows.Forms.ToolStrip socketTypeToolStrip;
        private System.Windows.Forms.ToolStripComboBox socketTypeToolStripComboBox;
        private System.Windows.Forms.TabPage registryTabPage;
        private System.Windows.Forms.DataGridView boardRegistryDataGridView;
        private System.Windows.Forms.DataGridViewCheckBoxColumn inUseDataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn symbolDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn captionDataGridViewTextBoxColumn1;
        private System.Windows.Forms.ToolStrip boardRegistryToolStrip;
        private System.Windows.Forms.ToolStripComboBox boardRegistryTypeToolStripComboBox;
        private System.Windows.Forms.TabPage powerTabPage;
        private System.Windows.Forms.Button powerDownButton;
        private System.Windows.Forms.Button powerUpButton;
        private System.Windows.Forms.Button powerRemoveButton;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ListBox powerTargetListBox;
        private System.Windows.Forms.Button powerAddButton;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ListBox powerSourceListBox;
        private System.Windows.Forms.TabPage stepsTabPage;
        private System.Windows.Forms.SplitContainer stepSplitContainer;
        private System.Windows.Forms.DataGridView stepOutputDataGridView;
        private System.Windows.Forms.DataGridViewCheckBoxColumn inUseDataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn labelDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel stepOutputPanel;
        private System.Windows.Forms.NumericUpDown stepOutputValueNumericUpDown;
        private System.Windows.Forms.Label stepOutputValueLabel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label stepOutputNameLabel;
        private System.Windows.Forms.DataGridView stepInputDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn labelDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn powerStepInputColumn;
        private System.Windows.Forms.Panel stepInputPanel;
        private System.Windows.Forms.NumericUpDown stepInputMaxValueNumericUpDown;
        private System.Windows.Forms.NumericUpDown stepInputMinValueNumericUpDown;
        private System.Windows.Forms.Button stepInputImageButton;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox stepInputErrorTextBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox stepInputFatalErrorCheckBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label stepInputValueLabel;
        private System.Windows.Forms.Label stepInputPowerModeLabel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label stepInputNameLabel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ToolStrip stepToolStrip;
        private System.Windows.Forms.ToolStripButton addStepToolStripButton;
        private System.Windows.Forms.ToolStripButton copyStepToolStripButton;
        private System.Windows.Forms.ToolStripButton deleteStepToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton prevStepToolStripButton;
        private System.Windows.Forms.ToolStripLabel stepInfoToolStripLabel;
        private System.Windows.Forms.ToolStripButton nextStepToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox stepDelayToolStripTextBox;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox stepErrorMessageToolStripTextBox;
        private System.Windows.Forms.ToolStripButton stepErrorImageToolStripButton;
        private System.Windows.Forms.ComboBox resistanceComboBox;
        private System.Windows.Forms.TextBox productNameTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox stepInputLogicValueCheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn inUseDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pinTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn captionDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripButton refreshBoardToolStripButton;
    }
}
