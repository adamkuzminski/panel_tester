﻿namespace PanelTester.UI
{
    partial class TestHistoryRegistryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitter = new System.Windows.Forms.Splitter();
            this.exportToolStripButton = new PanelTester.UI.InheritedToolStripButton();
            this.orderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.detailsDataGridView = new System.Windows.Forms.DataGridView();
            this.panelTestResultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statsToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.startDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.messageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTestResultBindingSource)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.DataSource = this.orderBindingSource;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridView.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.dataGridView.ShowCellErrors = false;
            this.dataGridView.ShowCellToolTips = false;
            this.dataGridView.ShowEditingIcon = false;
            this.dataGridView.ShowRowErrors = false;
            this.dataGridView.Size = new System.Drawing.Size(1176, 403);
            this.dataGridView.SelectionChanged += new System.EventHandler(this.dataGridView_SelectionChanged);
            this.dataGridView.Sorted += new System.EventHandler(this.dataGridView_Sorted);
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.Enabled = false;
            this.copyToolStripButton.Visible = false;
            // 
            // editToolStripButton
            // 
            this.editToolStripButton.Enabled = false;
            this.editToolStripButton.Visible = false;
            // 
            // deleteToolStripButton
            // 
            this.deleteToolStripButton.Enabled = false;
            this.deleteToolStripButton.Visible = false;
            // 
            // previewToolStripButton
            // 
            this.previewToolStripButton.Enabled = false;
            this.previewToolStripButton.Visible = false;
            // 
            // toolStrip
            // 
            this.toolStrip.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToolStripButton});
            // 
            // addToolStripButton
            // 
            this.addToolStripButton.Visible = false;
            // 
            // splitter
            // 
            this.splitter.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter.Location = new System.Drawing.Point(0, 461);
            this.splitter.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.splitter.Name = "splitter";
            this.splitter.Size = new System.Drawing.Size(1176, 5);
            this.splitter.TabIndex = 3;
            this.splitter.TabStop = false;
            // 
            // exportToolStripButton
            // 
            this.exportToolStripButton.AutoSize = false;
            this.exportToolStripButton.Image = global::PanelTester.Properties.Resources.Export;
            this.exportToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.exportToolStripButton.Name = "exportToolStripButton";
            this.exportToolStripButton.Size = new System.Drawing.Size(60, 55);
            this.exportToolStripButton.Text = "Eksportuj";
            this.exportToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.exportToolStripButton.Click += new System.EventHandler(this.exportToolStripButton_Click);
            // 
            // orderBindingSource
            // 
            this.orderBindingSource.DataSource = typeof(PanelTester.Data.Order);
            // 
            // detailsDataGridView
            // 
            this.detailsDataGridView.AllowUserToAddRows = false;
            this.detailsDataGridView.AllowUserToDeleteRows = false;
            this.detailsDataGridView.AllowUserToResizeRows = false;
            this.detailsDataGridView.AutoGenerateColumns = false;
            this.detailsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.detailsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detailsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.startDateDataGridViewTextBoxColumn,
            this.panelNumberDataGridViewTextBoxColumn,
            this.resultDataGridViewCheckBoxColumn,
            this.messageDataGridViewTextBoxColumn,
            this.errorsDataGridViewTextBoxColumn,
            this.tryDataGridViewTextBoxColumn,
            this.userIdDataGridViewTextBoxColumn,
            this.userNameDataGridViewTextBoxColumn,
            this.endDateDataGridViewTextBoxColumn});
            this.detailsDataGridView.DataSource = this.panelTestResultBindingSource;
            this.detailsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailsDataGridView.Location = new System.Drawing.Point(0, 466);
            this.detailsDataGridView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.detailsDataGridView.MultiSelect = false;
            this.detailsDataGridView.Name = "detailsDataGridView";
            this.detailsDataGridView.ReadOnly = true;
            this.detailsDataGridView.RowHeadersVisible = false;
            this.detailsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.detailsDataGridView.ShowCellErrors = false;
            this.detailsDataGridView.ShowCellToolTips = false;
            this.detailsDataGridView.ShowEditingIcon = false;
            this.detailsDataGridView.ShowRowErrors = false;
            this.detailsDataGridView.Size = new System.Drawing.Size(1176, 400);
            this.detailsDataGridView.TabIndex = 2;
            // 
            // panelTestResultBindingSource
            // 
            this.panelTestResultBindingSource.DataSource = typeof(PanelTester.Data.PanelTestResult);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statsToolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 866);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1176, 22);
            this.statusStrip.TabIndex = 4;
            this.statusStrip.Text = "statusStrip1";
            // 
            // statsToolStripStatusLabel
            // 
            this.statsToolStripStatusLabel.Name = "statsToolStripStatusLabel";
            this.statsToolStripStatusLabel.Size = new System.Drawing.Size(32, 17);
            this.statsToolStripStatusLabel.Text = "Stats";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "csv";
            this.saveFileDialog.Filter = "Pliki CSV|*.csv|Wszystkie pliki|*.*";
            this.saveFileDialog.Title = "Eksportuj wybrane pomiary do pliku CSV";
            // 
            // startDateDataGridViewTextBoxColumn
            // 
            this.startDateDataGridViewTextBoxColumn.DataPropertyName = "StartDate";
            this.startDateDataGridViewTextBoxColumn.HeaderText = "Data rozpoczęcia testu";
            this.startDateDataGridViewTextBoxColumn.Name = "startDateDataGridViewTextBoxColumn";
            this.startDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.startDateDataGridViewTextBoxColumn.Width = 180;
            // 
            // panelNumberDataGridViewTextBoxColumn
            // 
            this.panelNumberDataGridViewTextBoxColumn.DataPropertyName = "PanelNumber";
            this.panelNumberDataGridViewTextBoxColumn.HeaderText = "Numer panelu";
            this.panelNumberDataGridViewTextBoxColumn.Name = "panelNumberDataGridViewTextBoxColumn";
            this.panelNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.panelNumberDataGridViewTextBoxColumn.Width = 122;
            // 
            // resultDataGridViewCheckBoxColumn
            // 
            this.resultDataGridViewCheckBoxColumn.DataPropertyName = "Result";
            this.resultDataGridViewCheckBoxColumn.HeaderText = "Wynik testu";
            this.resultDataGridViewCheckBoxColumn.Name = "resultDataGridViewCheckBoxColumn";
            this.resultDataGridViewCheckBoxColumn.ReadOnly = true;
            this.resultDataGridViewCheckBoxColumn.Width = 87;
            // 
            // messageDataGridViewTextBoxColumn
            // 
            this.messageDataGridViewTextBoxColumn.DataPropertyName = "Message";
            this.messageDataGridViewTextBoxColumn.HeaderText = "Komunikat";
            this.messageDataGridViewTextBoxColumn.Name = "messageDataGridViewTextBoxColumn";
            this.messageDataGridViewTextBoxColumn.ReadOnly = true;
            this.messageDataGridViewTextBoxColumn.Width = 109;
            // 
            // errorsDataGridViewTextBoxColumn
            // 
            this.errorsDataGridViewTextBoxColumn.DataPropertyName = "Errors";
            this.errorsDataGridViewTextBoxColumn.HeaderText = "Błędy";
            this.errorsDataGridViewTextBoxColumn.Name = "errorsDataGridViewTextBoxColumn";
            this.errorsDataGridViewTextBoxColumn.ReadOnly = true;
            this.errorsDataGridViewTextBoxColumn.Width = 74;
            // 
            // tryDataGridViewTextBoxColumn
            // 
            this.tryDataGridViewTextBoxColumn.DataPropertyName = "Try";
            this.tryDataGridViewTextBoxColumn.HeaderText = "Próba";
            this.tryDataGridViewTextBoxColumn.Name = "tryDataGridViewTextBoxColumn";
            this.tryDataGridViewTextBoxColumn.ReadOnly = true;
            this.tryDataGridViewTextBoxColumn.Width = 76;
            // 
            // userIdDataGridViewTextBoxColumn
            // 
            this.userIdDataGridViewTextBoxColumn.DataPropertyName = "UserId";
            this.userIdDataGridViewTextBoxColumn.HeaderText = "Identyfikator testera";
            this.userIdDataGridViewTextBoxColumn.Name = "userIdDataGridViewTextBoxColumn";
            this.userIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.userIdDataGridViewTextBoxColumn.Width = 160;
            // 
            // userNameDataGridViewTextBoxColumn
            // 
            this.userNameDataGridViewTextBoxColumn.DataPropertyName = "UserName";
            this.userNameDataGridViewTextBoxColumn.HeaderText = "Nazwa testera";
            this.userNameDataGridViewTextBoxColumn.Name = "userNameDataGridViewTextBoxColumn";
            this.userNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.userNameDataGridViewTextBoxColumn.Width = 124;
            // 
            // endDateDataGridViewTextBoxColumn
            // 
            this.endDateDataGridViewTextBoxColumn.DataPropertyName = "EndDate";
            this.endDateDataGridViewTextBoxColumn.HeaderText = "Data zakończenia testu";
            this.endDateDataGridViewTextBoxColumn.Name = "endDateDataGridViewTextBoxColumn";
            this.endDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.endDateDataGridViewTextBoxColumn.Width = 184;
            // 
            // TestHistoryRegistryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.ClientSize = new System.Drawing.Size(1176, 888);
            this.Controls.Add(this.detailsDataGridView);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.splitter);
            this.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
            this.Name = "TestHistoryRegistryForm";
            this.Text = "Historia pomiarów";
            this.Load += new System.EventHandler(this.TestHistoryRegistryForm_Load);
            this.Controls.SetChildIndex(this.toolStrip, 0);
            this.Controls.SetChildIndex(this.dataGridView, 0);
            this.Controls.SetChildIndex(this.splitter, 0);
            this.Controls.SetChildIndex(this.statusStrip, 0);
            this.Controls.SetChildIndex(this.detailsDataGridView, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.toolStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.orderBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTestResultBindingSource)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Splitter splitter;
        private InheritedToolStripButton exportToolStripButton;
        private System.Windows.Forms.BindingSource orderBindingSource;
        private System.Windows.Forms.DataGridView detailsDataGridView;
        private System.Windows.Forms.BindingSource panelTestResultBindingSource;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel statsToolStripStatusLabel;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn errorMessageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastStepValuesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn panelNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn resultDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn messageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn errorsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDateDataGridViewTextBoxColumn;
    }
}
