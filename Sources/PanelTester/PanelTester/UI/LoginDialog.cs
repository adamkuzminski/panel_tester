﻿using PanelTester.Data;
using System;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class LoginDialog : BaseDialog
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public LoginDialog()
        {
            InitializeComponent();
        }

        public override bool SaveData(out String invalidField)
        {
            invalidField = "";

            string userId = textBoxUserId.Text.Trim();
            if (userId.Length == 0)
            {
                invalidField = labelUserId.Text;
                return false;
            }

            string password = textBoxPassword.Text.Trim();
            if (password.Length == 0)
            {
                invalidField = labelPassword.Text;
                return false;
            }

            Database database = Database.GetInstance();
            foreach (User user in database.UserList)
            {
                if ((user.Id == userId) && (user.Password == password))
                {
                    AppEngine.GetInstance().User = user;
                    return true;
                }
            }

            MessageBox.Show("Nie znaleziono użytkownika o podanym identyfikatorze lub haśle", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            return false;
        }

        protected override bool IsKeyboardRequired()
        {
            return true;
        }

        private void textBoxPassword_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.Enter)
                okButton.PerformClick();
        }

        private void textBoxUserId_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.Enter)
                textBoxPassword.Focus();
        }
    }
}