﻿using PanelTester.Common;
using PanelTester.Data;
using System;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class BoardRegistryForm : PanelTester.UI.BaseRegistryForm
    {
        public BoardRegistryForm()
        {
            InitializeComponent();
        }

        protected override void AddRecord()
        {
            AddBoard(new Board());
        }

        protected override void CopyRecord()
        {
            if (dataGridView.SelectedRows.Count == 1)
            {
                AddBoard((dataGridView.SelectedRows[0].DataBoundItem as Board).Copy());
            }
        }

        protected override void DeleteRecord()
        {
            if (dataGridView.SelectedRows.Count > 0)
            {
                Database database = Database.GetInstance();
                database.BoardList.Remove(dataGridView.SelectedRows[0].DataBoundItem as Board);
                database.SaveData(database.BoardList);
                RefreshGrid();
            }
        }

        protected override void EditRecord()
        {
            if (dataGridView.SelectedRows.Count == 1)
            {
                Board board = dataGridView.SelectedRows[0].DataBoundItem as Board;
                BoardEditorDialog dlg = new BoardEditorDialog(board, false);
                Database database = Database.GetInstance();
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    database.SaveData(database.BoardList);
                }
                else
                {
                    database.LoadData(ref database.BoardList);
                    RefreshGrid();
                }
            }
        }

        protected override void PreviewRecord()
        {
            if (dataGridView.SelectedRows.Count == 1)
            {
                BoardEditorDialog dlg = new BoardEditorDialog(dataGridView.SelectedRows[0].DataBoundItem as Board, true);
                dlg.ShowDialog();
            }
        }

        protected override void RefreshGrid()
        {
            RefreshGrid(new BindingSource(new SortableBindingList<Board>(Database.GetInstance().BoardList), null));            
        }

        private bool AddBoard(Board board)
        {
            BoardEditorDialog dlg = new BoardEditorDialog(board, false);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Database database = Database.GetInstance();
                database.BoardList.Add(board);
                database.SaveData(database.BoardList);
                RefreshGrid();
                return true;
            }

            return false;
        }

        private void BoardRegistryForm_Load(object sender, EventArgs e)
        {
        }
    }
}