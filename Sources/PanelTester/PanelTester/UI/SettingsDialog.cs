﻿using com.citizen.sdk.LabelPrint;
using PanelTester.Common;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace PanelTester.UI
{
    public partial class SettingsDialog : PanelTester.UI.BaseDialog
    {
        private PrinterEngine printer;

        public SettingsDialog()
        {
            InitializeComponent();
        }

        public override bool SaveData(out String invalidField)
        {
            invalidField = "";

            Properties.Settings config = Properties.Settings.Default;

            string dbPath = dbTextBox.Text.Trim();
            if (dbPath.Length == 0)
            {
                invalidField = "Katalog z danymi";
                return false;
            }
            if (!Directory.Exists(dbPath))
            {
                MessageBox.Show("Katalog z danymi nie istnieje!\nSprawdź, czy ścieżka do danych jest poprawna", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            config.DatabasePath = dbPath;

            String printerDeviceName = printerTextBox.Text;
            if ((printerDeviceName == null) || (printerDeviceName.Length == 0))
            {
                config.PrinterDeviceName = "";
                MessageBox.Show("Brak drukarki uniemożliwi przeprowadzanie testów", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                config.PrinterDeviceName = printerDeviceName;

            config.PrinterDarkness = (int)printerDarknessNumericUpDown.Value;
            config.BarcodeHorzOffset = (int)barcodeHorzOffsetNumericUpDown.Value;
            config.BarcodeThickBarWidth = (int)thickBarNumericUpDown.Value;
            config.BarcodeNarrowBarWidth = (int)narrowBarNumericUpDown.Value;

            config.PLCAddress = plcAddressTextBox.Text;
            config.PLCNodeId = plcNodeIdTextBox.Text;

            config.ShowVirtualKeyboard = keyboardCheckBox.Checked;

            config.Save();

            return true;
        }

        protected override bool IsKeyboardRequired()
        {
            return true;
        }

        private void dbPathButton_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.SelectedPath = dbTextBox.Text;
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                dbTextBox.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void plcTestButton_Click(object sender, EventArgs e)
        {
            Properties.Settings config = Properties.Settings.Default;
            try
            {
                config.PLCAddress = plcAddressTextBox.Text;
                config.PLCNodeId = plcNodeIdTextBox.Text;

                PlcEngine plcEngine = new PlcEngine();
                if (plcEngine.Connect())
                    MessageBox.Show("Nawiązano połączenie ze sterownikiem PLC", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Nie można połączyć się ze sterownikiem PLC", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nie można połączyć się ze sterownikiem PLC.\n\n" + ex.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                config.Reload();
            }
        }

        private void printerComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            if (e.Index >= 0)
            {
                CitizenPrinterInfo printerInfo = (CitizenPrinterInfo)(sender as ComboBox).Items[e.Index];
                e.Graphics.DrawString(String.Format("[{0}] {1}", printerInfo.deviceName, printerInfo.printerModel), e.Font, new SolidBrush(e.ForeColor), e.Bounds);
            }
            e.DrawFocusRectangle();
        }

        private void refreshPrinterButton_Click(object sender, EventArgs e)
        {
            SearchPrinter();
        }

        private void SearchPrinter()
        {
            string printerDeviceName = Properties.Settings.Default.PrinterDeviceName;

            printerComboBox.Items.Clear();

            int result;
            CitizenPrinterInfo[] list = printer.SearchPrinter(out result);
            if (result == LabelConst.CLS_SUCCESS)
            {
                printerComboBox.Items.AddRange(list);
                if ((printerDeviceName != null) && (printerDeviceName.Length > 0))
                {
                    foreach (CitizenPrinterInfo printerInfo in printerComboBox.Items)
                    {
                        if (printerInfo.deviceName.Equals(printerDeviceName))
                        {
                            printerComboBox.SelectedItem = printerInfo;
                            break;
                        }
                    }
                }
            }
            else
                MessageBox.Show("Wystąpił problem z pobraniem listy drukarek", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void selectPrinterButton_Click(object sender, EventArgs e)
        {
            CitizenPrinterInfo printerInfo = printerComboBox.SelectedItem as CitizenPrinterInfo;
            if (printerInfo == null)
                printerTextBox.Text = "";
            else
                printerTextBox.Text = printerInfo.deviceName;
        }

        private void SettingsDialog_Load(object sender, EventArgs e)
        {
            printer = new PrinterEngine();
            SearchPrinter();

            Properties.Settings config = Properties.Settings.Default;

            dbTextBox.Text = config.DatabasePath;
            printerTextBox.Text = config.PrinterDeviceName;
            printerDarknessNumericUpDown.Value = config.PrinterDarkness;
            barcodeHorzOffsetNumericUpDown.Value = config.BarcodeHorzOffset;
            thickBarNumericUpDown.Value = config.BarcodeThickBarWidth;
            narrowBarNumericUpDown.Value = config.BarcodeNarrowBarWidth;

            plcAddressTextBox.Text = config.PLCAddress;
            plcNodeIdTextBox.Text = config.PLCNodeId;

            keyboardCheckBox.Checked = config.ShowVirtualKeyboard;
        }

        private void testPrintButton_Click(object sender, EventArgs e)
        {
            CitizenPrinterInfo printerInfo = printerComboBox.SelectedItem as CitizenPrinterInfo;
            if (printerInfo == null)
            {
                MessageBox.Show("Brak drukarki", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                Properties.Settings config = Properties.Settings.Default;

                config.PrinterDarkness = (int)printerDarknessNumericUpDown.Value;
                config.BarcodeHorzOffset = (int)barcodeHorzOffsetNumericUpDown.Value;
                config.BarcodeThickBarWidth = (int)thickBarNumericUpDown.Value;
                config.BarcodeNarrowBarWidth = (int)narrowBarNumericUpDown.Value;

                printer.PrintLabel("61424401", "2", "SR LW 310 Generation 2010", "1047");

                config.Reload();
            }
        }
    }
}